<?php
/**
 * Created by Administrator
 * User: longli
 * VX: isa1589518286
 * Date: 2020/08/21
 * Time: 19:23
 * @link http://www.lmterp.cn
 */

namespace app\common\service\import;


use app\common\library\Tools;
use app\common\model\Account;
use app\common\model\AccountPlatform;
use app\common\model\ProductPlatformSku;
use app\common\model\ProductStore;
use app\common\service\product\ProductService;

/**
 * 导入平台sku和内部sku生成映射关系
 * Class PlatformSku
 * @package app\common\service\import
 */
class PlatformSku extends BaseImport
{

    /**
     * 交换键值后读入头信息
     * @var array
     */
    protected $hsku = [];

    /**
     * 原读入头信息
     * @var array
     */
    protected $esku = [];

    /**
     * 过虑字段
     * @var string[]
     */
    protected $excludeField = ['sku.create_time'];

    public function init()
    {
        $this->esku = (new \app\common\service\export\PlatformSku())->getHeader();
        $this->hsku = array_flip($this->esku);
    }

    protected function write()
    {
        return ProductPlatformSku::limit(500)->insertAll($this->buildData()) > 0;
    }

    /**
     * 构建导入数据
     * @return array
     * @date 2020/09/16
     * @author longli
     */
    protected function buildData()
    {
        $ret = [];
        foreach($this->getData() as $item)
        {
            foreach($this->excludeField as $k) unset($item[$k]);
            $this->transform($item);
            foreach($item['skus'] as $sku)
            {
                foreach($item['platform_sku'] as $s)
                {
                    $temp = ['platform_sku' => $s] + $sku + $item;
                    $ret[] = ProductPlatformSku::getFilterField($temp);
                }
            }
        }
        return $ret;
    }

    /**
     * 平台sku
     * @var array
     */
    protected $pskus = [];

    protected function validate(&$row = [], $key = 0)
    {
        $row = Tools::replaceArrayKey($this->hsku, $row);
        $error = [];
        $skus = replaceStr($row['sku.sku']);
        $pskus = replaceStr($row['sku.platform_sku']);
        if(Tools::contains($skus, ',') && Tools::contains($pskus, ',')) $error[] = "sku【{$skus}】和平台sku【{$pskus}】不能同时设置多对多关系";
        foreach(explode(',', $skus) as $sku)
        {
            if(!ProductStore::hasSku($sku)) $error[] = "SKU【{$sku}】不存在";
        }
        foreach(explode(',', $pskus) as $psku)
        {
            if(in_array($psku, $this->pskus))
            {
                $error[] = "平台SKU【{$psku}】已重复";
                continue;
            }
            $this->pskus[] = $psku;
            if(ProductPlatformSku::hasPlatformSku($psku)) $error[] = "平台SKU【{$psku}】已存在";
        }
        if(!Account::hasAccount($row['account.username'])) $error[] = "店铺账号【{$row['account.username']}】不存在";
        //if(!AccountPlatform::hasName($row['platform.name'])) $error[] = "平台【{$row['platform.name']}】不存在";
        if(!empty($error)) return join(", ", $error);
        return true;
    }

    /**
     * 转换数据
     * @param array $row
     * @date 2021/02/17
     * @author longli
     */
    protected function transform(& $row = [])
    {
        if(!empty($row['sku.sku']))
        {
            $temp = [];
            foreach(ProductStore::whereIn('sku', replaceStr($row['sku.sku']))->select() as $store)
            {
                $temp[] = [
                   'store_id' => $store->store_id,
                   'product_id' => $store->product_id,
                   'sku' => $store->sku,
                ];
            }
            $row['sku.skus'] = $temp;
        }
        if(!empty($row['account.username'])) $row['sku.account_id'] = Account::getByName($row['account.username'])->account_id;
        $row['sku.platform_sku'] = explode(',', replaceStr($row['sku.platform_sku']));
        $row = ProductService::getInstance()->parseRequestData($row)['sku'];

    }
}