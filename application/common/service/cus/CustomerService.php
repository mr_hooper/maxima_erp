<?php
/**
 * Created by Administrator
 * User: longli
 * VX: isa1589518286
 * Date: 2020/12/10
 * Time: 21:03
 * @link http://www.lmterp.cn
 */


namespace app\common\service\cus;


use app\common\library\Tools;
use app\common\model\CsBlacklist;
use app\common\model\Orders;
use app\common\service\BaseService;

class CustomerService extends BaseService
{
    /**
     * 添加黑名单
     * @param array $data 黑名单信息
     * @date 2020/12/10
     * @author longli
     * @return CsBlacklist|bool
     */
    public function addBlack($data = [])
    {
        $bdata = CsBlacklist::getFilterField($data, '');
        if(empty($bdata)) return false;
        $bdata = Tools::trim($bdata);
        $black = null;
        if(isset($bdata['black_id'])) $black = CsBlacklist::get($bdata['black_id']);
        if(!empty($black))
        {
            $black->save($bdata);
        }
        else
        {
            $black = CsBlacklist::create($bdata)->refresh();
        }
        return $black;
    }

    /**
     * 订单添加黑名单
     * @param int|string|string[] $orderIds 订单号
     * @param string $reason 加入黑名单原因
     * @date 2020/12/11
     * @author longli
     * @return void
     */
    public function addBlackByOrder($orderIds, $reason = '')
    {
        foreach(Orders::whereIn("order_id", $orderIds)->select() as $order)
        {
            if(CsBlacklist::inBlackBox($order)) continue;
            $data = $order->toArray();
            $data['reason'] = $reason;
            $this->addBlack($data);
        }
    }
}