<?php
/**
 * Created by Administrator
 * User: longli
 * VX: isa1589518286
 * Date: 2020/12/08
 * Time: 21:03
 * @link http://www.lmterp.cn
 */

namespace app\common\service\cus;


use app\common\library\Tools;
use app\common\model\CsEmailCategory;
use app\common\model\CsEmailTemplate;
use app\common\service\BaseService;

class EmailService extends BaseService
{
    /**
     * 添加邮件模板
     * @param array $data 分类信息
     * @date 2020/12/08
     * @author longli
     * @return CsEmailTemplate|bool
     */
    public function addEmailTemplate($data = [])
    {
        $tdata = CsEmailTemplate::getFilterField($data, '');
        if(empty($tdata)) return false;
        $tdata = Tools::trim($tdata);
        $template = null;
        if(isset($tdata['temp_id'])) $template = CsEmailTemplate::get($tdata['temp_id']);
        if(!empty($template))
        {
            $template->save($tdata);
        }
        else
        {
            $template = CsEmailTemplate::create($tdata)->refresh();
        }
        return $template;
    }

    /**
     * 添加邮件分类
     * @param array $data 分类信息
     * @date 2020/12/08
     * @author longli
     * @return CsEmailCategory|bool
     */
    public function addCategory($data = [])
    {
        $cDate = CsEmailCategory::getFilterField($data, '');
        if(empty($cDate)) return false;
        $cDate = Tools::trim($cDate);
        $cate = null;
        if(isset($cDate['cate_id'])) $cate = CsEmailCategory::get($cDate['cate_id']);
        if(!empty($cate))
        {
            $cate->save($cDate);
        }
        else
        {
            $cate = CsEmailCategory::create($cDate)->refresh();
        }
        return $cate;
    }
}