<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2020/04/21
 * Time: 15:18
 * @link http://www.lmterp.cn
 */

namespace app\common\service;

use app\common\library\Tools;
use app\common\model\Account;
use think\facade\Cache;

/**
 * 服务基类
 * Class BaseService
 * @package app\common\service
 */
abstract class BaseService
{

    protected static $instance = null;

    /**
     * 通过 ID 获取平台账号信息
     * @param int $accountId 账号 ID
     * @return Account
     * @date 2020/02/25
     * @author longli
     */
    public function getAccountById($accountId)
    {
        return Account::get($accountId);
    }

    /**
     * 获取附件的绝对路径
     * @param string $path
     * @return string
     * @date 2020/12/19
     * @author longli
     */
    public function getAttachRootPath($path = '')
    {
        if(empty($path) || !Tools::startWith($path, '/uploads')) return $path;
        $rootPath = \Env::get('root_path') . "./public" . $path;
        return $rootPath;
    }

    /**
     * 获取单例对象
     * @date 2020/08/12
     * @author longli
     * @return static
     */
    public static function getInstance()
    {
        if(!(static::$instance instanceof static)) static::$instance = new static();
        return self::$instance;
    }

    /**
     * 获取指定路径下的所有 Service 服务
     * @param string $path 路径
     * @param string|string[] $filterClass 需要排除的类
     * @return array
     * @date 2020/09/24
     * @author longli
     */
    public static function getAllService($path, $filterClass = [])
    {
        if(!is_array($filterClass)) $filterClass = [$filterClass];
        $classes = array_map(function($item)
        {
            list($name) = explode('.', substr(strrchr($item, '/'), 1));
            return $name;
        }, Tools::getFiles($path));
        if(empty($filterClass)) return $classes;
        return array_filter($classes, function($item) use ($filterClass)
        {
            return !in_array($item, $filterClass);
        });
    }

    /**
     * 隐藏系统绝对路径
     * @param string $rootPath 系统中绝对路径
     * @return string 返回相对于项目路径
     * @date 2020/12/24
     * @author longli
     */
    public static function hiddenRootPath($rootPath = '')
    {
        $path = str_replace(\Env::get('root_path'), '', $rootPath);
        $start = 0;
        if(Tools::startWith($path, './public'))$start = 8;
        else if(Tools::startWith($path, '.')) $start = 1;
        else if(Tools::startWith($path,"public/")) $start = 6;
        return substr($path, $start);
    }

    /**
     * 拼接文档绝对路径
     * @param string $path 路径
     * @return string 返回文件在系统中绝对路径
     * @date 2020/12/24
     * @author longli
     */
    public static function getRootPath($path = '')
    {
        if(empty($path)) return $path;
        if(Tools::startWith($path, 'http')) return $path;
        if(Tools::startWith($path, '.')) $path = "/$path";
        return \Env::get("root_path") . "public{$path}";
    }

    /**
     * 生成单号
     * @param string $prefix 前缀
     * @return string
     * @date 2021/06/05
     * @author longli
     */
    public static function generateSn($prefix = '')
    {
        if(!empty($prefix) && !Tools::endWith($prefix, '-')) $prefix .= '-';
        return $prefix . date('YmdHis') . '-' . mt_rand(1000, 9999);
    }
}