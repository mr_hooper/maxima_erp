<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2020/12/01
 * Time: 21:09
 * @link http://www.lmterp.cn
 */

namespace app\common\service\system;

use app\common\library\Tools;
use app\common\model\JobFlowModule;
use app\common\model\JobFlowSettings;
use app\common\service\BaseService;

/**
 * 审批流服务类
 * Class JobFlowService
 * @package app\common\service\system
 */
class JobFlowService extends BaseService
{
    /**
     * 添加审批模块
     * @param array $data 审批模块数据
     * @return JobFlowModule|false
     * @date 2021/04/02
     * @author longli
     */
    public function addFlow($data = [])
    {
        $fData = JobFlowModule::getFilterField($data, '');
        if(empty($fData)) return false;
        $fData = Tools::trim($fData);
        $flow = null;
        if(isset($fData['flow_id'])) $flow = JobFlowModule::get($fData['flow_id']);
        if(empty($flow) && isset($fData['module'])) $flow = JobFlowModule::get(['module' => $fData['module']]);
        if(!empty($flow))
        {
            $flow->save($fData);
        }
        else
        {
            $flow = JobFlowModule::create($fData)->refresh();
        }
        return $flow;
    }

    /**
     * 添加审批流
     * @param array $data 审批游信息
     * @return JobFlowSettings|false
     * @date 2021/04/02
     * @author longli
     */
    public function addSettings($data = [])
    {
        $sData = JobFlowSettings::getFilterField($data, '');
        if(empty($sData)) return false;
        $sData = Tools::trim($sData);
        $settings = null;
        if(isset($sData['settings_id'])) $settings = JobFlowSettings::get($sData['settings_id']);
        if(!empty($settings))
        {
            $settings->save($sData);
        }
        else
        {
            $settings = JobFlowSettings::create($sData)->refresh();
        }
        return $settings;
    }
}