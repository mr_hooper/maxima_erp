<?php
/**
 * Created by Administrator
 * User: longli
 * VX: isa1589518286
 * Date: 2020/08/17
 * Time: 21:03
 * @link http://www.lmterp.cn
 */

namespace app\common\service\report;

use app\common\library\Tools;
use app\common\model\ReportOrderDay;
use app\common\service\BaseService;

class ReportService extends BaseService
{
    /**
     * 添加统计订单
     * @param array $data 统计信息
     * @return ReportOrderDay|false
     * @date 2020/09/27
     * @author longli
     */
    public function addStatOrder($data = [])
    {
        $rData = ReportOrderDay::getFilterField($data, '');
        if(empty($rData)) return false;
        $rData = Tools::trim($rData);
        $report = null;
        if(isset($rData['day_id'])) $report = ReportOrderDay::get($rData['day_id']);
        if(empty($report) && isset($rData['ref_key']) && isset($rData['ref_value']) && isset($rData['stat_date']))
            $report = ReportOrderDay::get(['ref_key' => $rData['ref_key'], 'ref_value' => $rData['ref_value'], 'stat_date' => $rData['stat_date']]);
        if(!empty($report))
        {
            $report->save($rData);
        }
        else
        {
            $report = ReportOrderDay::create($rData)->refresh();
        }
        return $report;
    }
}