<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2021/01/14
 * Time: 17:10
 * @link http://www.lmterp.cn
 */

namespace app\common\behavior\finance;

use app\common\behavior\BaseBehavior;
use app\common\library\Tools;
use app\common\model\FinanceCash;
use app\common\model\JobFlowModule;
use app\common\model\Producer;
use app\common\model\PurchaseExchange;
use app\common\model\PurchaseReturn;

/**
 * 收款回调
 * Class Cash
 * @package app\common\behavior\finance
 */
class Cash extends BaseBehavior
{
    /**
     * 付款数据模型
     * @var FinanceCash
     */
    protected $cash;

    /**
     * 收款单入口
     * @param FinanceCash $cash 收款单
     * @return bool|void
     * @date 2021/02/05
     * @author longli
     */
    public function run(FinanceCash $cash)
    {
        // 验证审批状态和付款状态
        if(!JobFlowModule::isCheckFinish($cash) || $cash->real_money <= 0) return;
        $method = Tools::toCamelCase($cash->getData('ref_type'));
        if(!method_exists($this, $method)) return;
        $this->cash = $cash;
        return $this->$method();
    }

    /**
     * 采购退货
     * @return bool|void
     * @date 2021/02/05
     * @author longli
     */
    protected function purchaseReturn()
    {
        $ren = PurchaseReturn::get(['return_sn' => $this->cash->ref_sn]);
        if(empty($ren)) return;
        $ren->save(['is_pay' => PurchaseReturn::IS_YES]);
        Producer::get($ren->producer_id)->addPrice(-$this->cash->real_money);
        return true;
    }

    /**
     * 采购换货
     * @return bool|void
     * @date 2021/02/08
     * @author longli
     */
    protected function purchaseExchange()
    {
        $exchange = PurchaseExchange::get(['exchange_sn' => $this->cash->ref_sn]);
        if(empty($exchange)) return;
        $exchange->save(['is_pay' => PurchaseExchange::IS_YES]);
        Producer::get($exchange->producer_id)->addPrice(-$this->cash->real_money);
        return true;
    }
}