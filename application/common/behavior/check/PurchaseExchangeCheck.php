<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2021/01/05
 * Time: 18:23
 * @link http://www.lmterp.cn
 */

namespace app\common\behavior\check;

use app\common\model\JobFlowModule;
use app\common\model\Purchase;
use app\common\model\PurchaseExchange;
use app\common\service\product\StockService;
use think\Db;

/**
 * 采购换货审批回调
 * Class PurchaseExchangeCheck
 * @package app\common\behavior\check
 */
class PurchaseExchangeCheck extends BaseCheck
{

    /**
     * @inheritDoc
     */
    protected function execute()
    {
        $jobFlow = $this->getJobApprove();
        if($jobFlow->id_type != 'purchase_exchange') return;
        $exchange = PurchaseExchange::get($jobFlow->ref_id);
        // 更新审批状态
        $this->updateCheckStatus($exchange);
        // 审批完成占用库存
        if(JobFlowModule::isCheckFinish($exchange)) $this->lockStock($exchange);
    }

    /**
     * 占用库存
     * @param PurchaseExchange $exchange 采购换货单
     * @return bool
     * @date 2021/01/05
     * @author longli
     */
    protected function lockStock($exchange)
    {
        Db::startTrans();
        if(in_array($exchange->purchase->getData('order_status'), [Purchase::ORDER_STATUS_IN_PART, Purchase::ORDER_STATUS_IN_SUCC])
            && StockService::purchaseExchangeLock($exchange) !== true)
        {
            Db::rollback();
            return false;
        }
        $exchange->status = PurchaseExchange::STATUS_EXCHANGE_WAIT_OUT; // 待出库
        $exchange->save();
        Db::commit();
        return true;
    }
}