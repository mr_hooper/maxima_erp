<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2021/01/02
 * Time: 20:57
 * @link http://www.lmterp.cn
 */

namespace app\common\behavior\check;

use app\common\model\JobFlowModule;
use app\common\model\WarehouseAllot;
use app\common\service\product\StockService;

/**
 * 调拨出库审批回调
 * Class WarehouseAllotCheck
 * @package app\common\behavior\check
 */
class WarehouseAllotCheck extends BaseCheck
{

    /**
     * @inheritDoc
     */
    protected function execute()
    {
        $jobFlow = $this->getJobApprove();
        if($jobFlow->id_type != 'warehouse_allot') return;
        $allot = WarehouseAllot::get($jobFlow->ref_id);
        // 更新审批状态
        $this->updateCheckStatus($allot);
        // 审批完成占用库存
        if(JobFlowModule::isCheckFinish($allot)) StockService::allotLock($allot->allot_id);
    }
}