<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2021/01/02
 * Time: 18:23
 * @link http://www.lmterp.cn
 */

namespace app\common\behavior\check;

use app\common\model\Purchase;

/**
 * 采购审批回调
 * Class PurchaseCheck
 * @package app\common\behavior\check
 */
class PurchaseCheck extends BaseCheck
{

    /**
     * @inheritDoc
     */
    protected function execute()
    {
        $jobFlow = $this->getJobApprove();
        if($jobFlow->id_type != 'purchase') return;
        $purchase = Purchase::get($jobFlow->ref_id);
        // 更新审批状态
        $this->updateCheckStatus($purchase);
    }
}