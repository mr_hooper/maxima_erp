<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2021/01/16
 * Time: 16:20
 * @link http://www.lmterp.cn
 */

namespace app\common\behavior\check;

/**
 * 财务付款审批回调
 * Class FinancePaymentCheck
 * @package app\common\behavior\check
 */
class FinancePaymentCheck extends BaseCheck
{

    /**
     * @inheritDoc
     */
    protected function execute()
    {
        $jobFlow = $this->getJobApprove();
        if($jobFlow->id_type != 'finance_payment') return;
        $payment = \app\common\model\FinancePayment::get($jobFlow->ref_id);
        // 更新审批状态
        $this->updateCheckStatus($payment);
    }
}