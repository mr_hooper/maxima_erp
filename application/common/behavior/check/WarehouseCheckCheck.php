<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2021/02/10
 * Time: 11:25
 * @link http://www.lmterp.cn
 */

namespace app\common\behavior\check;

use app\common\model\WarehouseCheck;

/**
 * 盘点单审批回调
 * Class WarehouseCheckCheck
 * @package app\common\behavior\check
 */
class WarehouseCheckCheck extends BaseCheck
{

    protected function execute()
    {
        $jobFlow = $this->getJobApprove();
        if($jobFlow->id_type != 'warehouse_check') return;
        $check = WarehouseCheck::get($jobFlow->ref_id);
        // 更新审批状态
        $this->updateCheckStatus($check);
    }
}