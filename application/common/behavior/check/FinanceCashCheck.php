<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2021/01/16
 * Time: 16:20
 * @link http://www.lmterp.cn
 */

namespace app\common\behavior\check;

/**
 * 财务收款审批回调
 * Class FinancePaymentCheck
 * @package app\common\behavior\check
 */
class FinanceCashCheck extends BaseCheck
{

    /**
     * @inheritDoc
     */
    protected function execute()
    {
        $jobFlow = $this->getJobApprove();
        if($jobFlow->id_type != 'finance_cash') return;
        $cash = \app\common\model\FinanceCash::get($jobFlow->ref_id);
        // 更新审批状态
        $this->updateCheckStatus($cash);
    }
}