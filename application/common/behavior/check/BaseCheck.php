<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2021/01/02
 * Time: 18:22
 * @link http://www.lmterp.cn
 */

namespace app\common\behavior\check;

use app\common\behavior\BaseBehavior;
use app\common\model\BaseModel;
use app\common\model\JobApprove;
use app\common\model\JobFlowModule;

/**
 * 审批回调
 * Class BaseCheck
 * @package app\common\behavior\check
 */
abstract class BaseCheck extends BaseBehavior
{
    /**
     * 当前审批节点
     * @var JobApprove
     */
    protected $jobApprove;

    /**
     * 执行回调
     * @return bool
     * @date 2021/01/02
     * @author longli
     */
    public function run(JobApprove $approve)
    {
        $this->jobApprove = $approve;
        return $this->execute();
    }

    /**
     * 执行审批后续处理
     * @return bool
     * @date 2021/01/14
     * @author longli
     */
    abstract protected function execute();

    /**
     * 更新审批状态
     * @param BaseModel $obj 审批业务单模型
     * @return bool
     * @date 2021/01/08
     * @author longli
     */
    public function updateCheckStatus(BaseModel $obj)
    {
        if(empty($obj) || !isset($obj->check_status)) return false;
        // 更新审批状态
        $obj->check_status = JobFlowModule::CHECK_ING;
        if($this->getJobApprove()->is_end == JobApprove::IS_YES)
        {
            $obj->check_status = $this->getJobApprove()->getData('status') == JobApprove::JOB_STATUS_PASS
                ? JobFlowModule::CHECK_SUCC
                : JobFlowModule::CHECK_REJECT;
        }
        $obj->save();
        return true;
    }

    /**
     * @return JobApprove
     */
    public function getJobApprove()
    {
        return $this->jobApprove;
    }
}