<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2021/03/11
 * Time: 14:13
 * @link http://www.lmterp.cn
 */

namespace app\common\behavior\order;

use app\common\behavior\BaseBehavior;
use app\common\model\Orders;

/**
 * 选择渠道
 * Class ChoiceChannel
 * @package app\common\behavior\order
 */
class ChoiceChannel extends BaseBehavior
{
    public function run(Orders $order)
    {
        if(empty($order) || !empty($order->channel_id)) return;
        \think\Console::call('order_channel', ["--ids={$order->order_id}"]);
    }
}