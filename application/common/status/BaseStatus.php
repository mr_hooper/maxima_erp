<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2020/09/01
 * Time: 14:45
 * @link http://www.lmterp.cn
 */

namespace app\common\status;


class BaseStatus
{
    /**
     * 成功
     */
    const CODE_NORMAL = 1;

    /**
     * 失败
     */
    const CODE_FAULT = 0;

    /**
     * 业务类型采购
     * @var string
     */
    const REF_TYPE_PURCHASE = 'purchase';

    /**
     * 业务类型采购退货
     * @var string
     */
    const REF_TYPE_PURCHASE_RETURN = 'purchase_return';

    /**
     * 业务类型采购换货
     * @var string
     */
    const REF_TYPE_PURCHASE_EXCHANGE = 'purchase_exchange';

    /**
     * 业务类型订单出库
     * @var string
     */
    const REF_TYPE_ORDER_OUT = 'order_out';

    /**
     * 业务类型订单退货入库
     * @var string
     */
    const REF_TYPE_ORDER_RETURN = 'order_return';


    /**
     * 业务类型订单占用库存
     * @var string
     */
    const REF_TYPE_ORDER_LOCK = 'order_lock';

    /**
     * 业务类型订单释放库存
     * @var string
     */
    const REF_TYPE_ORDER_UNLOCK = 'order_unlock';

    /**
     * 业务类型调拨
     * @var string
     */
    const REF_TYPE_WMS_ALLOT = 'wms_allot';

    /**
     * 业务类型出库
     * @var string
     */
    const REF_TYPE_WMS_OUT = 'wms_out';

    /**
     * 业务类型入库
     */
    const REF_TYPE_WMS_IN = 'wms_in';

    /**
     * 业务类型仓库盘点
     * @var string
     */
    const REF_TYPE_WMS_CHECK = 'wms_check';

    /**
     * 业务类型商品
     * @var string
     */
    const REF_TYPE_PRODUCT = 'product';

    /**
     * 财务报销
     * @var string
     */
    const REF_TYPE_EXPEND = 'finance_expend';

    /**
     * 业务类型
     * @var array
     */
    public static $REF_TYPE = [
        self::REF_TYPE_PURCHASE          => '采购',
        self::REF_TYPE_PURCHASE_EXCHANGE => '采购换货',
        self::REF_TYPE_PURCHASE_RETURN   => '采购退货',
        self::REF_TYPE_WMS_OUT           => '出库',
        self::REF_TYPE_WMS_IN            => '入库',
        self::REF_TYPE_ORDER_OUT         => '订单出库',
        self::REF_TYPE_ORDER_LOCK        => '订单占用库存',
        self::REF_TYPE_ORDER_RETURN      => '订单退货',
        self::REF_TYPE_ORDER_UNLOCK      => '订单释放库存',
        self::REF_TYPE_WMS_ALLOT         => '库存调拨',
        self::REF_TYPE_WMS_CHECK         => '库存盘点',
        self::REF_TYPE_PRODUCT           => '录用商品',
        self::REF_TYPE_EXPEND            => '报销',
    ];
}