<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2020/04/14
 * Time: 11:34
 * @link http://www.lmterp.cn
 */

namespace app\common\model;

use think\model\relation\BelongsTo;

/**
 * 订单详情模型
 * Class OrderInfo
 * @package app\common\model
 */
class OrdersDetail extends BaseModel
{
    protected $pk = 'detail_id';

    protected $autoWriteTimestamp = 'datetime';

    // 设置 json 字段
    protected $json = ['ext_json'];

    protected $insert = ['create_by', 'update_by'];

    protected $update = ['update_by'];

    /**
     * 未捆绑商品
     * @var int
     */
    const BUNDLE_N = 0;

    /**
     * 有捆绑商品
     * @var int
     */
    const BUNDLE_Y = 1;

    public static $BUNDLE_STATUS = [
        self::BUNDLE_N => '未捆绑',
        self::BUNDLE_Y => '有捆绑',
    ];

    /**
     * 未取消
     * @var int
     */
    const CANCEL_N = 0;

    /**
     * 已取消
     * @var int
     */
    const CANCEL_Y = 1;

    public static $CANCEL_STATUS = [
        self::BUNDLE_N => '未取消',
        self::BUNDLE_Y => '已取消',
    ];

    protected function getIsBundleAttr($value)
    {
        return isset(self::$BUNDLE_STATUS[$value])
            ? self::$BUNDLE_STATUS[$value]
            : $value;
    }

    protected function getIsCancelAttr($value)
    {
        return isset(self::$CANCEL_STATUS[$value])
            ? self::$CANCEL_STATUS[$value]
            : $value;
    }

    protected function setIsBundleAttr($value)
    {
        $temp = array_flip(self::$BUNDLE_STATUS);
        return isset($temp[$value])
            ? $temp[$value]
            : $value;
    }

    protected function setIsCancelAttr($value)
    {
        $temp = array_flip(self::$CANCEL_STATUS);
        return isset($temp[$value])
            ? $temp[$value]
            : $value;
    }

    /**
     * 主订单
     * @return BelongsTo
     * @date 2020/04/14
     * @author longli
     */
    public function order()
    {
        return $this->belongsTo(Orders::class, 'order_id', 'order_id');
    }

    /**
     * 关联商品变体
     * @return BelongsTo
     * @date 2021/03/03
     * @author longli
     */
    public function store()
    {
        return $this->belongsTo(ProductStore::class, 'store_id', 'store_id');
    }
}