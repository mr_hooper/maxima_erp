<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2020/08/15
 * Time: 21:00
 * @link http://www.lmterp.cn
 */

namespace app\common\model;


class ProductPurchase extends BaseModel
{
    protected $pk = 'pp_id';

    protected $autoWriteTimestamp = 'datetime';

    // 设置 json 字段
    protected $json = ['ext_json'];

    protected $insert = ['create_by', 'update_by'];

    protected $update = ['update_by'];

    public static $STATUS = [
        self::IS_YES => '启用',
        self::IS_NO => '关闭'
    ];

    protected function getIsDefaultAttr($value)
    {
        return isset(static::$IS_STATUS[$value]) ? static::$IS_STATUS[$value] : $value;
    }

    protected function getTypeAttr($value)
    {
        return isset(Producer::$LINE_TYPE[$value]) ? Producer::$LINE_TYPE[$value] : $value;
    }

    protected function getStatusAttr($value)
    {
        return isset(self::$STATUS[$value]) ? self::$STATUS[$value] : $value;
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function producer()
    {
        return $this->belongsTo(Producer::class);
    }

    /**
     * 判断 url 是否已存在
     * @param string $url url
     * @return bool
     * @date 2020/11/04
     * @author longli
     */
    public static function hasUrl($url)
    {
        return !!static::where("url", $url)->count();
    }
}