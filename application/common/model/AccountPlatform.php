<?php
/**
 * Created by Administrator
 * User: longli
 * VX: isa1589518286
 * Date: 2020/08/18
 * Time: 16:02
 * @link http://www.lmterp.cn
 */

namespace app\common\model;

/**
 * 平台模型
 * Class AccountPlatform
 * @package app\common\model
 */
class AccountPlatform extends BaseModel
{
    protected $pk = 'platform_id';
    protected $autoWriteTimestamp = 'datetime';

    public function account()
    {
        return $this->hasMany(Account::class, 'platform_id', 'platform_id');
    }

    /**
     * 获取所有平台
     * @date 2020/09/15
     * @author longli
     */
    public static function getAll()
    {
        return self::order("sort desc, name")->where(['status' => self::IS_YES])->select();
    }

    /**
     * 平台名称是否存在
     * @param string $name 平台名称
     * @return bool
     * @date 2020/09/16
     * @author longli
     */
    public static function hasName($name)
    {
        return !!static::where("name", $name)->count();
    }
}