<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2020/08/25
 * Time: 15:24
 * @link http://www.lmterp.cn
 */

namespace app\common\model;

use PDOStatement;
use think\Collection;

class Channel extends BaseModel
{
    protected $pk = 'channel_id';

    protected $autoWriteTimestamp = 'datetime';

    // 设置 json 字段
    protected $json = ['ext_json'];

    protected $insert = ['create_by', 'update_by'];

    protected $update = ['update_by'];

    /**
     * 邮政
     */
    const CHANNEL_TYPE_Y = 10;

    /**
     * 专线
     */
    const CHANNEL_TYPE_Z = 20;

    /**
     * 快递
     */
    const CHANNEL_TYPE_K = 30;

    /**
     * 其它
     */
    const CHANNEL_TYPE_Q = 40;

    public static $CHANNEL_TYPE = [
        self::CHANNEL_TYPE_Y => '邮政',
        self::CHANNEL_TYPE_Z => '专线',
        self::CHANNEL_TYPE_K => '快递',
        self::CHANNEL_TYPE_Q => '其它',
    ];

    public static $IS_TRACK = [
        self::IS_YES => '可追踪',
        self::IS_NO => '不可追踪',
    ];

    /**
     * 预备追踪号
     */
    const TRACK_TYPE_Y = 1;

    /**
     * API获取追踪号
     */
    const TRACK_TYPE_A = 2;

    public static $TRACK_TYPE = [
        self::TRACK_TYPE_A => 'API获取追踪号',
        self::TRACK_TYPE_Y => '预备追踪号',
    ];

    /**
     * 状态开启
     */
    const STATUS_Y = 1;

    /**
     * 状态关闭
     */
    const STATUS_N = 0;

    /**
     * 测试
     */
    const STATUS_T = 2;

    public static $CHANNEL_STATUS = [
        self::STATUS_Y => '开启',
        self::STATUS_N => '关闭',
        self::STATUS_T => '测试',
    ];

    /**
     * 自制
     */
    const LABEL_TYPE_S = 1;

    /**
     * api 下载
     */
    const LABEL_TYPE_D = 2;

    /**
     * 面单制作方式
     * @var array
     */
    public static $LABEL_TYPE = [
        self::LABEL_TYPE_D => 'API下载',
        self::LABEL_TYPE_S => '自制',
    ];

    protected function getLabelTypeAttr($value)
    {
        return isset(self::$LABEL_TYPE[$value]) ? self::$LABEL_TYPE[$value] : $value;
    }

    protected function getTrackTypeAttr($value)
    {
        return isset(self::$TRACK_TYPE[$value]) ? self::$TRACK_TYPE[$value] : $value;
    }

    protected function getChannelTypeAttr($value)
    {
        return isset(self::$CHANNEL_TYPE[$value]) ? self::$CHANNEL_TYPE[$value] : $value;
    }

    protected function getIsTrackAttr($value)
    {
        return isset(self::$IS_TRACK[$value]) ? self::$IS_TRACK[$value] : $value;
    }

    public function carrier()
    {
        return $this->belongsTo(ChannelCarrier::class, 'carrier_id', 'carrier_id');
    }

    /**
     * 通过承运商ID获取渠道
     * @param int|int[] $carrierId 承运商 id
     * @return array|PDOStatement|string|Collection
     * @date 2020/09/04
     * @author longli
     */
    public static function getByCarrierId($carrierId)
    {
        return Channel::where([
            ['carrier_id', 'in', $carrierId],
            ['status', '=', Channel::STATUS_Y]
        ])->select();
    }

    /**
     * 获取所有可用渠道
     * @return Channel[]
     * @date 2020/12/04
     * @author longli
     */
    public static function getAll()
    {
        return static::where(['status'=>self::STATUS_Y])->order("sort desc")->select();
    }

    /**
     * 获取面单打印尺寸
     * @return string
     * @date 2021/07/20
     * @author longli
     */
    public function getPrintSize()
    {
        return "{$this->label_width}x{$this->label_height}";
    }
}
