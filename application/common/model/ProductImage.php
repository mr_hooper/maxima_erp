<?php

namespace app\common\model;


class ProductImage extends BaseModel
{
    protected $pk = 'image_id';

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
