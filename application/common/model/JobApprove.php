<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2020/11/14
 * Time: 17:10
 * @link http://www.lmterp.cn
 */

namespace app\common\model;

class JobApprove extends BaseModel
{
    protected $pk = 'approve_id';

    protected $insert = ['create_by'];

    /**
     * 等待审批
     * @var int
     */
    const JOB_STATUS_WAIT = 0;

    /**
     * 审批通过
     * @var int
     */
    const JOB_STATUS_PASS = 10;

    /**
     * 驳回
     * @var int
     */
    const JOB_STATUS_REJECT = 20;

    public static $JOB_STATUS = [
        self::JOB_STATUS_WAIT => '等待',
        self::JOB_STATUS_PASS => '通过',
        self::JOB_STATUS_REJECT => '驳回',
    ];

    protected function getStatusAttr($value)
    {
        return isset(self::$JOB_STATUS[$value])
            ? self::$JOB_STATUS[$value]
            : $value;
    }
}
