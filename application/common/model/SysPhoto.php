<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2020/11/28
 * Time: 13:43
 * @link http://www.lmterp.cn
 */

namespace app\common\model;

class SysPhoto extends BaseModel
{
    protected $pk = 'photo_id';

    protected $insert = ['create_by'];

}
