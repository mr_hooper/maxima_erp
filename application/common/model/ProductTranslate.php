<?php

namespace app\common\model;


class ProductTranslate extends BaseModel
{
    protected $pk = 'translate_id';

    protected $autoWriteTimestamp = 'datetime';

    // 设置 json 字段
    protected $json = ['ext_json'];

    protected $insert = ['create_by', 'update_by'];

    protected $update = ['update_by'];

}
