<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2020/09/16
 * Time: 14:32
 * @link http://www.lmterp.cn
 */

namespace app\common\model;

use PHPUnit\phpDocumentor\Reflection\Types\Null_;

class WarehouseStock extends BaseModel
{
    protected $pk = 'stock_id';

    protected $autoWriteTimestamp = 'datetime';

    protected $insert = ['create_by', 'update_by'];

    protected $update = ['update_by'];

    /**
     * 关联变体
     * @return \think\model\relation\BelongsTo
     * @date 2020/09/16
     * @author longli
     */
    public function store()
    {
        return $this->belongsTo(ProductStore::class, 'store_id', 'store_id');
    }

    /**
     * 关联仓库
     * @return \think\model\relation\BelongsTo
     * @date 2021/02/09
     * @author longli
     */
    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class);
    }

    /**
     * 关联库位
     * @return \think\model\relation\BelongsTo
     * @date 2021/02/09
     * @author longli
     */
    public function location()
    {
        return $this->belongsTo(WarehouseLocation::class, 'location_id', 'location_id');
    }

    /**
     * 当前仓库是否有指定的 SKU
     * @param int|Warehouse $warehouse 仓库id, 或者仓库
     * @param string $sku 商品 SKU
     * @return bool
     * @date 2020/12/30
     * @author longli
     */
    public static function hasSku($warehouse, $sku)
    {
        $warehouse = Warehouse::getObj($warehouse);
        return !!static::where([
            'warehouse_id' => $warehouse->warehouse_id,
            'sku' => trim($sku)
        ])->count();
    }

    /**
     * 检查是否有足够的库存
     * @param int|Warehouse $warehouse 仓库id, 或者仓库
     * @param string $sku 商品 SKU
     * @param int $qty 数量
     * @return bool
     * @date 2020/12/31
     * @author longli
     */
    public static function isEnough($warehouse, $sku, $qty)
    {
        if($warehouse instanceof Warehouse) $warehouse = $warehouse->warehouse_id;
        return !!static::where([
            ['warehouse_id', '=', $warehouse],
            ['sku', '=', trim($sku)],
            ['stock', '>=', intval($qty)],
            ['status', '=', static::IS_YES],
        ])->count();
    }

    /**
     * 批量检查是否有足够的库存
     * @param int|Warehouse $warehouse 仓库id, 或者仓库
     * @param array $data SKU 信息
     * @example [
     *      [
     *         'sku' => 'A001',
     *         'qty' => 5
     *      ]
     * ]
     * @return bool
     * @date 2021/03/10
     * @author longli
     */
    public static function isEnoughBatch($warehouse, $data = [])
    {
        if(empty($warehouse) || empty($data)) return false;
        foreach($data as $item)
        {
            if(!static::isEnough($warehouse, $item['sku'], $item['qty']))
                return false;
        }
        return true;
    }

    /**
     * 获取指定仓库的 SKU 信息
     * @param int|Warehouse $warehouse 仓库id, 或者仓库
     * @param string $sku 商品 SKU
     * @return WarehouseStock|null 返回商品在仓库仓库的信息
     * @date 2020/12/30
     * @author longli
     */
    public static function getWarehouseBySku($warehouse, $sku)
    {
        $stock = static::getWarehouseBySkus($warehouse, $sku);
        return $stock->isEmpty() ? null : $stock[0];
    }

    /**
     * 获取指定仓库的多个 SKU 信息
     * @param int|Warehouse $warehouse 仓库id, 或者仓库
     * @param string $sku 商品 SKU
     * @return WarehouseStock[]
     * @date 2021/04/26
     * @author longli
     */
    public static function getWarehouseBySkus($warehouse, $sku)
    {
        if($warehouse instanceof Warehouse) $warehouse = $warehouse->warehouse_id;
        $stock = static::where([
            'warehouse_id' => $warehouse,
            'status' => self::IS_YES,
        ]);
        return $stock->whereIn('sku', $sku)->select();
    }
}
