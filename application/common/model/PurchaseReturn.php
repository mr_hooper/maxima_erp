<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2020/11/14
 * Time: 17:10
 * @link http://www.lmterp.cn
 */
namespace app\common\model;

class PurchaseReturn extends BaseModel
{
    protected $pk = 'ren_id';

    protected $autoWriteTimestamp = 'datetime';

    protected $insert = ['create_by', 'update_by'];

    protected $update = ['update_by'];

    /**
     * 退钱退货
     * @var int
     */
    const RETURN_TYPE_PAM = 0;

    /**
     * 退钱不退货
     * @var int
     */
    const RETURN_TYPE_M = 10;

    /**
     * 缺货退钱
     * @var int
     */
    const RETURN_TYPE_NPM = 20;

    public static $RETURN_TYPE = [
        self::RETURN_TYPE_PAM => '退钱退货',
        self::RETURN_TYPE_M => '退钱不退货',
        self::RETURN_TYPE_NPM => '缺货退钱',
    ];

    protected function getTypeAttr($value)
    {
        return isset(self::$RETURN_TYPE[$value]) ? self::$RETURN_TYPE[$value] : $value;
    }

    /**
     * 待处理
     * @var int
     */
    const STATUS_RETURN_WAIT = 0;

    /**
     * 占用库存
     * @var int
     */
    const STATUS_RETURN_LOCK_STOCK = 10;

    /**
     * 待出库
     * @var int
     */
    const STATUS_RETURN_WAIT_OUT = 20;

    /**
     * 已出库
     * @var int
     */
    const STATUS_RETURN_OUT = 30;

    /**
     * 已签收
     * @var int
     */
    const STATUS_RETURN_RECV = 40;

    /**
     * 取消
     * @var int
     */
    const STATUS_RETURN_CANCEL = 50;

    /**
     * 无需发货
     * @var int
     */
    const STATUS_RETURN_NOT_OUT = 60;

    public static $STATUS_RETURN = [
        self::STATUS_RETURN_WAIT => '待处理',
        self::STATUS_RETURN_LOCK_STOCK => '占用库存',
        self::STATUS_RETURN_WAIT_OUT => '待出库',
        self::STATUS_RETURN_OUT => '已出库',
        self::STATUS_RETURN_RECV => '已签收',
        self::STATUS_RETURN_CANCEL => '取消',
        self::STATUS_RETURN_NOT_OUT => '无需发货',
    ];

    protected function getStatusAttr($value)
    {
        return isset(self::$STATUS_RETURN[$value]) ? self::$STATUS_RETURN[$value] : $value;
    }

    protected function getPayTypeAttr($value)
    {
        return isset(PurchaseTrack::$PAY_TYPE[$value]) ? PurchaseTrack::$PAY_TYPE[$value] : $value;
    }

    /**
     * 关联采购单
     * @return \think\model\relation\BelongsTo
     * @date 2021/01/04
     * @author longli
     */
    public function purchase()
    {
        return $this->belongsTo(Purchase::class);
    }

    /**
     * 关联退货详情
     * @return \think\model\relation\HasMany
     * @date 2020/12/29
     * @author longli
     */
    public function info()
    {
        return $this->hasMany(PurchaseTerpInfo::class, 'ref_id', 'ren_id')
            ->where("id_type", static::getTable());
    }

    /**
     * 关联仓库
     * @return \think\model\relation\BelongsTo
     * @date 2020/11/07
     * @author longli
     */
    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class);
    }

    /**
     * 获取换货单
     * @param int|string $sn 换货单id，换货单号
     * @return static
     * @date 2021/01/11
     * @author longli
     */
    public static function getBySn($sn)
    {
        return static::where("ren_id", $sn)
            ->whereOr("return_sn", trim($sn))
            ->find();
    }
}
