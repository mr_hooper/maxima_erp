<?php

namespace app\common\model;


class PlatformPublish extends BaseModel
{
    protected $pk = 'publish_id';

    protected $autoWriteTimestamp = 'datetime';

    protected $insert = ['create_by', 'update_by'];

    protected $update = ['update_by'];
}
