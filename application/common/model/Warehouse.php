<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2020/09/10
 * Time: 19:25
 * @link http://www.lmterp.cn
 */

namespace app\common\model;

use PDOStatement;
use think\Collection;

class Warehouse extends BaseModel
{
    protected $pk = 'warehouse_id';

    protected $autoWriteTimestamp = 'datetime';

    protected $insert = ['create_by', 'update_by'];

    protected $update = ['update_by'];

    /**
     * 仓库名称是否存在
     * @param string $name 分类名
     * @return bool
     * @date 2020/09/11
     * @author longli
     */
    public static function hasByName($name)
    {
        return !!static::where(['name' => $name])->count();
    }

    /**
     * 仓库代码是否存在
     * @param string $code 分类名
     * @return bool
     * @date 2020/09/14
     * @author longli
     */
    public static function hasByCode($code)
    {
        return !!static::where(['code' => $code])->count();
    }

    /**
     * 通过名称获取仓库
     * @param string $name 分类名
     * @return static
     * @date 2020/09/11
     * @author longli
     */
    public static function getByName($name)
    {
        return static::get(['name' => trim($name)]);
    }

    /**
     * 获取所有仓库
     * @return array|PDOStatement|string|Collection
     * @date 2020/09/12
     * @author longli
     */
    public static function getAll()
    {
        return static::where(['status' => self::IS_YES])->select();
    }

    /**
     * 获取正常仓库
     * @return Warehouse[]
     * @date 2021/03/22
     * @author longli
     */
    public static function getNotVirtual()
    {
        return static::where([
            ['status', 'eq', static::IS_YES],
            ['is_virtual', 'eq', static::IS_NO],
        ])->select();
    }

    /**
     * 设置默认仓库
     * @param int|self $warehouse 仓库id，仓库
     * @param bool $force 强制设置
     * @return bool
     * @date 2021/01/22
     * @author longli
     */
    public static function setDefault($warehouse, $force = false)
    {
        $warehouse = self::getObj($warehouse);
        if(empty($warehouse)) return false;
        if(!$force && $warehouse->is_default == self::IS_YES) return true;
        static::where("warehouse_id", ">", 0)
            ->update(['is_default' => self::IS_NO]);
        static::where("warehouse_id", $warehouse->warehouse_id)->update(['is_default' => self::IS_YES]);
        return true;
    }

    /**
     * 获取默认仓库
     * @return Warehouse
     * @date 2021/03/24
     * @author longli
     */
    public static function getDefault()
    {
        return static::where([
            'is_default' => self::IS_YES,
            'status'     => self::IS_YES,
        ])->find();
    }

    /**
     * 仓库信息
     * @param int|int[] $ids 仓库 id
     * @param array $data 分类信息
     * @return bool
     * @date 2020/09/14
     * @author longli
     */
    public static function editByWarehouseId($ids, $data)
    {
        try
        {
            foreach(static::where("warehouse_id", "in", $ids)->select() as $warehouse)
            {
                $warehouse->save($data);
            }
            return true;
        }catch(\Exception $e)
        {
            return false;
        }
    }
}
