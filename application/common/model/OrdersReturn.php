<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2021/03/04
 * Time: 14:41
 * @link http://www.lmterp.cn
 */

namespace app\common\model;

/**
 * 订单退货模型
 * Class OrdersReturn
 * @package app\common\model
 */
class OrdersReturn extends BaseModel
{
    protected $pk = 'rid';

    protected $insert = ['create_by', 'update_by'];

    protected $update = ['update_by'];

    protected $json = ['return_info'];

    protected function getTrackStatusAttr($value)
    {
        return isset(ChannelOrders::$TRACK_STATUS[$value]) ? ChannelOrders::$TRACK_STATUS[$value] : $value;
    }

    /**
     * 主订单
     * @return \think\model\relation\BelongsTo
     * @date 2021/03/04
     * @author longli
     */
    public function order()
    {
        return $this->belongsTo(Orders::class, 'order_id', 'order_id');
    }

    /**
     * 是否全部退货
     * @return bool
     * @date 2021/04/04
     * @author longli
     */
    public function isReturnAll()
    {
        foreach($this->order->detail as $detail)
        {
            foreach($this->return_info as $info)
            {
                if($info->sku == $detail->sku)
                {
                    if($detail->qty != $info->qty) return false;
                    break;
                }
            }
        }
        return true;
    }

    /**
     * 关联渠道
     * @return \think\model\relation\BelongsTo
     * @date 2021/03/04
     * @author longli
     */
    public function channel()
    {
        return $this->belongsTo(Channel::class);
    }

    /**
     * 关联币种
     * @return \think\model\relation\BelongsTo
     * @date 2021/03/04
     * @author longli
     */
    public function currcode()
    {
        return $this->belongsTo(SysCountries::class, 'currency', 'currency_code');
    }

    /**
     * 关联退货仓库
     * @return \think\model\relation\BelongsTo
     * @date 2021/03/05
     * @author longli
     */
    public function win()
    {
        return $this->belongsTo(Warehouse::class, 'in_id', 'warehouse_id');
    }

    /**
     * 关联发货仓库
     * @return \think\model\relation\BelongsTo
     * @date 2021/03/05
     * @author longli
     */
    public function wout()
    {
        return $this->belongsTo(Warehouse::class, 'out_id', 'warehouse_id');
    }
}
