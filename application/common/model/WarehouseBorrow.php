<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2021/01/04
 * Time: 15:38
 * @link http://www.lmterp.cn
 */

namespace app\common\model;

class WarehouseBorrow extends BaseModel
{
    protected $pk = 'borrow_id';

    protected $autoWriteTimestamp = 'datetime';

    protected $insert = ['create_by', 'update_by'];

    protected $update = ['update_by'];

    /**
     * 待出库
     * @var int
     */
    const STATUS_WAIT = 0;

    /**
     * 已出库
     * @var int
     */
    const STATUS_OUT = 10;

    /**
     * 部分退还
     * @var int
     */
    const STATUS_RETURN_PART = 20;

    /**
     * 全部退还
     * @var int
     */
    const STATUS_RETURN_ALL = 30;

    /**
     * 待上架
     * @var int
     */
    const STATUS_WAIT_UP = 40;

    /**
     * 上架成功
     * @var int
     */
    const STATUS_WAIT_UP_SUCC = 50;

    /**
     * 已取消
     * @var int
     */
    const STATUS_CANCEL = 60;

    public static $BORROW_STATUS = [
        self::STATUS_WAIT => '待出库',
        self::STATUS_OUT => '已出库',
        self::STATUS_RETURN_PART => '部分退还',
        self::STATUS_RETURN_ALL => '全部退还',
        self::STATUS_WAIT_UP => '待上架',
        self::STATUS_WAIT_UP_SUCC => '上架完成',
        self::STATUS_CANCEL => '已取消',
    ];

    protected function getStatusAttr($value)
    {
        return isset(self::$BORROW_STATUS[$value])
            ? self::$BORROW_STATUS[$value]
            : $value;
    }

    /**
     * 关联借用详情
     * @return \think\model\relation\HasMany
     * @date 2021/01/04
     * @author longli
     */
    public function info()
    {
        return $this->hasMany(WarehouseTerpInfo::class, 'ref_id', 'borrow_id')
            ->where("id_type", static::getTable());
    }

    /**
     * 关联退还仓库
     * @return \think\model\relation\BelongsTo
     * @date 2021/02/18
     * @author longli
     */
    public function win()
    {
        return $this->belongsTo(Warehouse::class, 'in_id', 'warehouse_id');
    }

    /**
     * 关联借出仓库
     * @return \think\model\relation\BelongsTo
     * @date 2021/02/18
     * @author longli
     */
    public function wout()
    {
        return $this->belongsTo(Warehouse::class, 'out_id', 'warehouse_id');
    }

    /**
     * 关联用户
     * @return \think\model\relation\BelongsTo
     * @date 2021/02/18
     * @author longli
     */
    public function user()
    {
        return $this->belongsTo(Admin::class, 'user_id', 'id');
    }

    /**
     * 通过借用单获取
     * @param string $borrowSn 借用单号
     * @return static
     * @date 2021/01/04
     * @author longli
     */
    public static function getBySn($borrowSn)
    {
        $borrowSn = trim($borrowSn);
        return static::where("borrow_id", $borrowSn)
            ->whereOr("borrow_sn", $borrowSn)
            ->find();
    }

    /**
     * 借用单是否全部退还
     * @param string $borrowSn 借用单号
     * @return bool
     * @date 2021/01/04
     * @author longli
     */
    public static function isReturnAll($borrowSn)
    {
        foreach(self::getBySn($borrowSn)->info as $info)
        {
            if($info->in_qty < $info->qty) return false;
        }
        return true;
    }
}
