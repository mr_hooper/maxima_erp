<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2020/12/10
 * Time: 16:52
 * @link http://www.lmterp.cn
 */

namespace app\common\model;

class CsBlacklist extends BaseModel
{
    protected $pk = 'black_id';

    protected $autoWriteTimestamp = 'datetime';

    protected $updateTime = false;

    protected $insert = ['create_by', 'black_date'];

    protected function setBlackDateAttr()
    {
        return date('Y-m-d');
    }

    /**
     * 关联国家
     * @return \think\model\relation\BelongsTo
     * @date 2020/12/10
     * @author longli
     */
    public function country()
    {
        return $this->belongsTo(SysCountries::class, 'buyer_country_code', 'code_two');
    }

    /**
     * 检查订单是否在黑名单中
     * @param Orders $order 订单
     * @date 2020/12/10
     * @author longli
     * @return bool
     */
    public static function inBlackBox($order)
    {
        if(empty($order)) return false;

        // 验证邮箱
        if(!empty($order->buyer_email)
            && static::where("buyer_email", $order->buyer_email)
                ->count()
        ) return true;

        // 验证平台/账号
        if(!empty($order->buyer_account)
            && !empty($order->platform_name)
            && static::where([
                'platform_name' => $order->platform_name,
                "buyer_account" => $order->buyer_account
            ])->count()
        ) return true;

        // 验证国家/电话
        if(!empty($order->buyer_phone)
            && static::where([
                'buyer_country_code' => $order->buyer_country_code,
                'buyer_phone' => $order->buyer_phone,
            ])->count()
        ) return true;

        // 验证国家/手机
        if(!empty($order->buyer_mobile)
            && static::where([
                'buyer_country_code' => $order->buyer_country_code,
                'buyer_mobile' => $order->buyer_mobile,
            ])->count()
        ) return true;

        // 验证国家/省/城市/收件人
        if(!empty($order->consignee)
            && static::where([
                'buyer_country_code' => $order->buyer_country_code,
                'buyer_province' => $order->buyer_province,
                'buyer_city' => $order->buyer_city,
                'consignee' => $order->consignee,
            ])->count()) return true;

        //  验证国家/省/城市/名/姓
        if(!empty($order->buyer_first_name)
            && !empty($order->buyer_last_name)
            && static::where([
                'buyer_country_code' => $order->buyer_country_code,
                'buyer_province' => $order->buyer_province,
                'buyer_city' => $order->buyer_city,
                'buyer_first_name' => $order->buyer_first_name,
                'buyer_last_name' => $order->buyer_last_name,
            ])->count()) return true;

        return false;
    }
}
