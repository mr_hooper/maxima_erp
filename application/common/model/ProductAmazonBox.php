<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2021/04/19
 * Time: 13:41
 * @link http://www.lmterp.cn
 */

namespace app\common\model;

class ProductAmazonBox extends BaseModel
{
    protected $pk = 'box_id';

    /**
     * 关联账号
     * @return \think\model\relation\BelongsTo
     * @date 2021/04/19
     * @author longli
     */
    public function account()
    {
        return $this->belongsTo(Account::class);
    }
}
