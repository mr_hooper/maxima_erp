<?php

namespace app\common\model;


class WarehouseStoreStatus extends BaseModel
{
    protected $pk = 'sid';

    protected $autoWriteTimestamp = 'datetime';

    protected $insert = ['create_by', 'update_by'];

    protected $update = ['update_by'];
}
