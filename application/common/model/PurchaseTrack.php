<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2020/12/19
 * Time: 13:58
 * @link http://www.lmterp.cn
 */
namespace app\common\model;

use app\common\library\Tools;

class PurchaseTrack extends BaseModel
{
    protected $pk = 'track_id';

    protected $autoWriteTimestamp = 'datetime';

    protected $insert = ['create_by', 'update_by'];

    protected $update = ['update_by'];

    /**
     * 自付
     * @var int
     */
    const PAY_TYPE_M = 1;

    /**
     * 包邮
     * @var int
     */
    const PAY_TYPE_T = 0;

    /**
     * 垫付
     * @var int
     */
    const PAY_TYPE_D = 2;

    public static $PAY_TYPE = [
        self::PAY_TYPE_M => '自付',
        self::PAY_TYPE_T => '包邮',
        self::PAY_TYPE_D => '垫付',
    ];

    protected function getTrackStatusAttr($value)
    {
        return isset(ChannelExpress::$TRACK_STATUS[$value]) ? ChannelExpress::$TRACK_STATUS[$value] : $value;
    }
    
    /**
     * 关联国内快递
     * @return \think\model\relation\BelongsTo
     * @date 2021/03/16
     * @author longli
     */
    public function express()
    {
        return $this->belongsTo(ChannelExpress::class, 'express_id', 'express_id');
    }

    /**
     * 关联采购单
     * @return \think\model\relation\BelongsTo
     * @date 2020/11/07
     * @author longli
     */
    public function purchase()
    {
        return $this->belongsTo(Purchase::class);
    }

    /**
     * 关联追踪号详情
     * @return \think\model\relation\HasMany
     * @date 2020/12/25
     * @author longli
     */
    public function info()
    {
        return $this->hasMany(PurchaseTerpInfo::class, 'ref_id', 'track_id')
            ->where("id_type", static::getTable());
    }

    /**
     * 检查是否包含追踪号
     * @param string $trackNum 追踪号
     * @param int $purId 采购ID
     * @return bool
     * @date 2020/12/23
     * @author longli
     */
    public static function hasTrackNum($trackNum, $purId = 0)
    {
        $model = static::where("track_num", trim($trackNum));
        if(!empty($purId)) $model->where("purchase_id", $purId);
        return !!$model->count();
    }

    /**
     * 检查追踪号是否已收过货
     * @param string $trackNum 追踪号
     * @param int $purId 采购ID
     * @return bool
     * @date 2020/12/25
     * @author longli
     */
    public static function isRecv($trackNum, $purId = 0)
    {
        $model = static::where("track_num", trim($trackNum))
            ->where("is_recv", self::IS_YES);
        if(!empty($purId)) $model->where("purchase_id", $purId);
        return !!$model->count();
    }

    /**
     * 根据追踪号获取
     * @param string $trackNum 追踪号
     * @return PurchaseTrack
     * @date 2021/01/01
     * @author longli
     */
    public static function getByTrackNum($trackNum)
    {
        return static::get(['track_num' => trim($trackNum)]);
    }

    /**
     * 更新采购单追踪号为收货状态
     * @param string $trackNum 追踪号
     * @param int $purId 采购ID
     * @return bool
     * @date 2020/12/28
     * @author longli
     */
    public static function recv($trackNum, $purId = 0)
    {
        $trackNum = trim($trackNum);
        $model = static::where("track_num", $trackNum);
        if(!empty($purId)) $model->where("purchase_id", $purId);
        if(!($track = $model->find())) return false;
        $track->is_recv = self::IS_YES;
        $track->recv_date = Tools::now();
        $user = session("lmterp");
        if(!empty($user)) $track->recv_user_id = $user->id;
        $track->save();
        return true;
    }
}
