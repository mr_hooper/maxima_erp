<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2020/11/14
 * Time: 17:10
 * @link http://www.lmterp.cn
 */

namespace app\common\model;

class JobFlowModule extends BaseModel
{
    protected $pk = 'flow_id';

    protected $autoWriteTimestamp = 'datetime';

    protected $insert = ['create_by', 'update_by'];

    protected $update = ['update_by'];

    /**
     * 未审批
     * @var int
     */
    const CHECK_WAIT = 0;

    /**
     * 审批中
     * @var int
     */
    const CHECK_ING = 10;

    /**
     * 审批驳回
     * @var int
     */
    const CHECK_REJECT = 20;

    /**
     * 不需要审批
     * @var int
     */
    const CHECK_NO_NEED = 30;

    /**
     * 审批完成
     * @var int
     */
    const CHECK_SUCC = 40;

    public static $CHECK_STATUS = [
        self::CHECK_WAIT => '未审批',
        self::CHECK_ING => '审批中',
        self::CHECK_REJECT => '审批驳回',
        self::CHECK_NO_NEED => '不需要审批',
        self::CHECK_SUCC => '审批完成',
    ];

    /**
     * 关联审批流详情
     * @return \think\model\relation\HasMany
     * @date 2021/04/02
     * @author longli
     */
    public function settings()
    {
        return $this->hasMany(JobFlowSettings::class, 'flow_id', 'flow_id');
    }

    /**
     * 判断对象是否审批完成
     * @param BaseModel $obj 需要检查的对象
     * @return bool
     * @date 2021/01/07
     * @author longli
     */
    public static function isCheckFinish(BaseModel $obj)
    {
        return self::isCheck($obj, [self::CHECK_SUCC, self::CHECK_NO_NEED]);
    }

    /**
     * 判断对象是否审批中
     * @param BaseModel $obj
     * @return bool
     * @date 2021/01/09
     * @author longli
     */
    public static function isCheckIng(BaseModel $obj)
    {
        return self::isCheck($obj, [self::CHECK_WAIT, self::CHECK_ING]);
    }

    /**
     * 判断对象是否等待审批
     * @param BaseModel $obj
     * @return bool
     * @date 2021/01/09
     * @author longli
     */
    public static function isCheckWait(BaseModel $obj)
    {
        return self::isCheck($obj, self::CHECK_WAIT);
    }

    /**
     * 判断对象是否审批失败
     * @param BaseModel $obj
     * @return bool
     * @date 2021/01/09
     * @author longli
     */
    public static function isCheckReject(BaseModel $obj)
    {
        return self::isCheck($obj, self::CHECK_REJECT);
    }

    /**
     * 判断对象是否审批完成
     * @param BaseModel $obj 需要检查的对象
     * @param int|int[] $status 指定的审批状态
     * @return bool
     * @date 2021/01/07
     * @author longli
     */
    public static function isCheck(BaseModel $obj, $status)
    {
        if(empty($obj) || !isset($obj->check_status)) return false;
        if(!is_array($status)) $status = [$status];
        return in_array($obj->getData('check_status'), $status);
    }

    /**
     * 获取所有模块
     * @return JobFlowModule[]
     * @date 2021/04/02
     * @author longli
     */
    public static function getAll()
    {
        return static::order('sort desc, flow_id desc')->select();
    }
}
