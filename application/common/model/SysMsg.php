<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2020/12/02
 * Time: 12:11
 * @link http://www.lmterp.cn
 */

namespace app\common\model;

class SysMsg extends BaseModel
{
    protected $pk = 'msg_id';

    /**
     * 获取当前用户消息
     * @param Admin $user 用户
     * @return SysMsg[]
     * @date 2021/01/26
     * @author longli
     */
    public static function getMsg(Admin $user)
    {
        $msgs = [];
        foreach(static::where([
            'is_read' => self::IS_NO,
            'to_id' => $user->id,
        ])->order("msg_id desc")->select() as $msg)
        {
            $msg->is_read = self::IS_YES;
            $msg->read_time = \app\common\library\Tools::now();
            $msg->save();
            $msgs[] = $msg;
        }
        return $msgs;
    }
}
