<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2020/11/14
 * Time: 17:10
 * @link http://www.lmterp.cn
 */
namespace app\common\model;


class PurchaseInfo extends BaseModel
{
    protected $pk = 'info_id';

    protected $autoWriteTimestamp = 'datetime';

    protected $insert = ['create_by', 'update_by'];

    protected $update = ['update_by'];

    /**
     * 待入库
     * @var int
     */
    const STORE_WAIT = 0;

    /**
     * 质检中
     * @var int
     */
    const STORE_CHE_ING = 10;

    /**
     * 异常
     * @var int
     */
    const STORE_EXCEPTION = 20;

    /**
     * 退货
     * @var int
     */
    const STORE_RETURN = 30;

    /**
     * 换货
     * @var int
     */
    const STORE_EXCHANGE = 40;

    /**
     * 补发
     * @var int
     */
    const STORE_REISSUE = 50;

    /**
     * 入库中
     * @var int
     */
    const STORE_IN_ING = 60;

    /**
     * 待上架
     * @var int
     */
    const STORE_WAIT_UP = 70;

    /**
     * 部分入库
     * @var int
     */
    const STORE_PART_UP = 80;

    /**
     * 全部入库
     * @var int
     */
    const STORE_SUCC = 90;

    public static $STORE_STATUS = [
        self::STORE_WAIT        => '待入库',
        self::STORE_CHE_ING     => '质检中',
        self::STORE_EXCEPTION   => '异常',
        self::STORE_RETURN      => '退货',
        self::STORE_EXCHANGE    => '换货',
        self::STORE_REISSUE     => '补发',
        self::STORE_IN_ING      => '入库中',
        self::STORE_WAIT_UP     => '待上架',
        self::STORE_PART_UP     => '部分入库',
        self::STORE_SUCC        => '全部入库',
    ];

    protected function getStoreStatusAttr($value)
    {
        return isset(self::$STORE_STATUS[$value])
            ? self::$STORE_STATUS[$value]
            : $value;
    }

    /**
     * 关联采购单
     * @return \think\model\relation\BelongsTo
     * @date 2020/11/07
     * @author longli
     */
    public function purchase()
    {
        return $this->belongsTo(Purchase::class);
    }

    /**
     * 关联商品变体
     * @return \think\model\relation\BelongsTo
     * @date 2020/11/07
     * @author longli
     */
    public function store()
    {
        return $this->belongsTo(ProductStore::class, 'store_id', 'store_id');
    }

    /**
     * 获取采购单指定 sku 单价
     * @param int|Purchase $purchase 采购id, 或者采购单
     * @param string $sku sku
     * @return float|int
     * @date 2020/12/29
     * @author longli
     */
    public static function getSkuPrice($purchase, $sku)
    {
        $price = 0;
        if(!is_numeric($purchase)) $purchase = $purchase->purchase_id;
        if($t = static::where([
            "purchase_id" => $purchase,
            'sku' => trim($sku)
        ])->value('price')) return $t;
        return $price;
    }

    /**
     * 获取采购单指定的 sku
     * @param string $sku sku
     * @param int|Purchase $purchase 采购id, 或者采购单
     * @return PurchaseInfo
     * @date 2021/01/06
     * @author longli
     */
    public static function getBySku($sku, $purchase)
    {
        if(!is_numeric($purchase)) $purchase = $purchase->purchase_id;
        return static::where([
            "sku" => trim($sku),
            "purchase_id" =>  $purchase
        ])->find();
    }

    /**
     * 计算采购单详情单个商品未入库数量
     * @return int
     * @date 2021/01/06
     * @author longli
     */
    public function getNotStoreQty()
    {
        $qty = $this->qty - $this->store_qty - $this->stock_qty - $this->return_qty - $this->exce_qty;
        return $qty > 0 ? $qty : 0;
    }

    /**
     * 判断采购单详情单个商品是否一个都未收到
     * @param PurchaseInfo $info 采购单详情
     * @return bool
     * @date 2021/01/06
     * @author longli
     */
    public function isNotRecv()
    {
        return $this->qty == $this->n_store_qty;
    }
}
