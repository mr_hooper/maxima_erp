<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2021/03/04
 * Time: 14:41
 * @link http://www.lmterp.cn
 */

namespace app\common\model;

class ChannelExpress extends BaseModel
{
    protected $pk = 'express_id';

    protected $insert = ['create_by', 'update_by'];

    protected $update = ['update_by'];

    /**
     * 运输中
     * @var int
     */
    const TRACK_STATUS_ING = 0;

    /**
     * 揽收
     * @var int
     */
    const TRACK_STATUS_ERECV = 1;

    /**
     * 疑难
     * @var int
     */
    const TRACK_STATUS_KNOTTY = 2;

    /**
     * 签收
     * @var int
     */
    const TRACK_STATUS_SIGN_RECV = 3;

    /**
     * 退签
     * @var int
     */
    const TRACK_STATUS_SIGN_RETURN = 4;

    /**
     * 派送
     * @var int
     */
    const TRACK_STATUS_DELIVERY = 5;

    /**
     * 退回
     * @var int
     */
    const TRACK_STATUS_RETURN = 6;

    /**
     * 转投
     * @var int
     */
    const TRACK_STATUS_TURN_SEND = 7;

    /**
     * 待清关
     * @var int
     */
    const TRACK_STATUS_CUSTOMS_WAIT = 10;

    /**
     * 清关中
     * @var int
     */
    const TRACK_STATUS_CUSTOMS_ING = 11;

    /**
     * 已清关
     */
    const TRACK_STATUS_CUSTOMS_SUCC = 12;

    /**
     * 清关异常
     */
    const TRACK_STATUS_CUSTOMS_EX = 13;

    /**
     * 拒签
     * @var int
     */
    const TRACK_STATUS_REJECT = 14;

    /**
     * 无状态
     * @var int
     */
    const TRACK_STATUS_NONE = 200;

    /**
     * 直接使用快递100状态码
     * @var string[]
     */
    public static $TRACK_STATUS = [
         self::TRACK_STATUS_ING => '运输中',
         self::TRACK_STATUS_ERECV => '揽收',
         self::TRACK_STATUS_KNOTTY => '疑难',
         self::TRACK_STATUS_SIGN_RECV => '签收',
         self::TRACK_STATUS_SIGN_RETURN => '退签',
         self::TRACK_STATUS_DELIVERY => '派件',
         self::TRACK_STATUS_RETURN => '退回',
         self::TRACK_STATUS_TURN_SEND => '转投',
         self::TRACK_STATUS_CUSTOMS_WAIT => '待清关',
         self::TRACK_STATUS_CUSTOMS_ING => '清关中',
         self::TRACK_STATUS_CUSTOMS_SUCC => '已清关',
         self::TRACK_STATUS_CUSTOMS_EX => '清关异常',
         self::TRACK_STATUS_REJECT => '拒签',
        self::TRACK_STATUS_NONE => '无状态',
    ];

    /**
     * 获取所有快递
     * @return ChannelExpress[]
     * @date 2021/03/04
     * @author longli
     */
    public static function getAll()
    {
        return static::where("status", static::IS_YES)
            ->order("sort desc")->select();
    }

    /**
     * 快递名称是否存在
     * @param string $name 快递名称
     * @return bool
     * @date 2021/03/06
     * @author longli
     */
    public static function hasName($name)
    {
        return !!static::where('logistics_name', trim($name))
            ->count();
    }

    /**
     * 通过id 获取快递名称
     * @param int $expressId 快递id
     * @return string
     * @date 2021/03/17
     * @author longli
     */
    public static function getNameById($expressId)
    {
        $express = static::get($expressId);
        return !empty($express) ? $express->logistics_name : '';
    }

}
