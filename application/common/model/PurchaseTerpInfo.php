<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2020/11/14
 * Time: 17:10
 * @link http://www.lmterp.cn
 */

namespace app\common\model;

/**
 * 采购商品退换物流...详情
 * Class PurchaseTerpInfo
 * @package app\common\model
 */
class PurchaseTerpInfo extends BaseModel
{
    protected $pk = 'info_id';

    protected $autoWriteTimestamp = 'datetime';

    protected $insert = ['create_by', 'update_by'];

    protected $update = ['update_by'];

    /**
     * 关联追踪号
     * @return \think\model\relation\BelongsTo
     * @date 2020/12/25
     * @author longli
     */
    public function track()
    {
        if($this->id_type != PurchaseTrack::getTable()) return null;
        return $this->belongsTo(PurchaseTrack::class, 'ref_id', 'track_id');
    }

    /**
     * 关联采购异常
     * @return \think\model\relation\BelongsTo
     * @date 2020/12/25
     * @author longli
     */
    public function exce()
    {
        if($this->id_type != PurchaseException::getTable()) return null;
        return $this->belongsTo(PurchaseException::class, 'ref_id', 'eid');
    }
}
