<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2020/09/08
 * Time: 13:52
 * @link http://www.lmterp.cn
 */

namespace app\common\model;

use think\model\relation\BelongsTo;

class ProductKeyword extends BaseModel
{
    protected $pk = 'word_id';

    protected $autoWriteTimestamp = 'datetime';

    protected $insert = ['create_by', 'update_by'];

    protected $update = ['update_by'];

    /**
     * 普通
     */
    const TYPE_ORD = 0;
    /**
     * 敏感词
     */
    const TYPE_SEN = 10;
    /**
     * 热词
     */
    const TYPE_HOT = 20;

    public static $WORD_TYPE = [
        self::TYPE_ORD => '普通',
        self::TYPE_SEN => '敏感词',
        self::TYPE_HOT => '热词',
    ];

    protected function getTypeAttr($value)
    {
        return isset(self::$WORD_TYPE[$value]) ? self::$WORD_TYPE[$value] : $value;
    }

    /**
     * 关联关键词分类
     * @return \think\model\relation\BelongsTo
     * @date 2021/07/20
     * @author longli
     */
    public function category()
    {
        return $this->belongsTo(ProductKeywordCategory::class, 'cate_id', 'cate_id');
    }

    /**
     * 用户
     * @return BelongsTo
     * @date 2020/09/11
     * @author longli
     */
    public function admin()
    {
        return $this->belongsTo(Admin::class, "create_by", "id");
    }

    /**
     * 是否包含关键词
     * @param string $word 关键词
     * @param int $cateId 分类
     * @return bool
     * @date 2021/07/21
     * @author longli
     */
    public static function hasWord($word, $cateId = 0)
    {
        $model = static::where("word", trim($word));
        if(!empty($cateId)) $model->where("cate_id", $cateId);
        return !!$model->count();
    }
}
