<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2020/02/25
 * Time: 15:24
 * @link http://www.lmterp.cn
 */

namespace app\common\model;

/**
 * 账号配置模型
 * Class AccountSettings
 * @package app\common\model
 */
class AccountSettings extends BaseModel
{
    protected $pk = 'settings_id';

    protected $autoWriteTimestamp = 'datetime';

    /**
     * 未同步
     */
    const STATUS_NONE = 0;

    /**
     * 同步中
     */
    const STATUS_ING = 5;

    /**
     * 同步成功
     */
    const STATUS_SUC = 10;

    /**
     * 同步失败
     */
    const STATUS_ERR = 15;

    public static $STATUS = [
        self::STATUS_NONE => '未同步',
        self::STATUS_ING => '同步中',
        self::STATUS_SUC => '同步成功',
        self::STATUS_ERR => '同步失败',
    ];

    /**
     * 自动同步
     */
    const AUTO_SYNC_Y = 1;

    /**
     * 手动同步
     */
    const AUTO_SYNC_N = 0;

    public static $AUTO_SYNC = [
        self::AUTO_SYNC_N => '手动同步',
        self::AUTO_SYNC_Y => '自动同步',
    ];

    protected function getStatusAttr($value)
    {
        return isset(self::$STATUS[$value]) ? self::$STATUS[$value] : $value;
    }

    protected function getIsAutoSyncAttr($value)
    {
        return isset(self::$AUTO_SYNC[$value]) ? self::$AUTO_SYNC[$value] : $value;
    }

    /**
     * 关联账号
     * @return \think\model\relation\BelongsTo
     * @date 2020/09/17
     * @author longli
     */
    public function account()
    {
        return $this->belongsTo(Account::class);
    }
}