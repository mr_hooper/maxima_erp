<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2020/09/27
 * Time: 22:22
 * @link http://www.lmterp.cn
 */

namespace app\common\model;

/**
 * 统计订单
 * Class ReportOrderDay
 * @package app\common\model
 */
class ReportOrderDay extends BaseModel
{
    protected $pk = 'day_id';

    protected $autoWriteTimestamp = 'datetime';
}
