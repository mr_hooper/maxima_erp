<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2020/12/15
 * Time: 15:24
 * @link http://www.lmterp.cn
 */
namespace app\common\model;


class PurchaseLog extends BaseModel
{
    protected $pk = 'log_id';

    protected $insert = ['create_by'];

    /**
     * 添加采购操作日志
     * @param int $purchaseId 采购id
     * @param string $action 动作
     * @param array|BaseModel $beforeData 操作前数据
     * @param array|BaseModel $afterData 操作后数据
     * @param string $remark 备注信息
     * @return PurchaseLog
     * @date 2020/12/15
     * @author longli
     */
    public static function addLog($purchaseId, $action, $beforeData = [], $afterData = [], $remark = '')
    {
        if($beforeData instanceof BaseModel) $beforeData = $beforeData->toArray();
        if($afterData instanceof BaseModel) $afterData = $afterData->toArray();
       $data = \app\common\library\Tools::trim([
           'purchase_id' => $purchaseId,
           'action' => $action,
           'before_data' => json_encode($beforeData),
           'after_data' => json_encode($afterData),
           'remark' => $remark,
       ]);
       return static::create($data);
    }
}
