<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2020/12/19
 * Time: 13:58
 * @link http://www.lmterp.cn
 */
namespace app\common\model;


class PurchaseException extends BaseModel
{
    protected $pk = 'eid';

    protected $autoWriteTimestamp = 'datetime';

    protected $insert = ['create_by', 'update_by'];

    protected $update = ['update_by'];

    /**
     * 缺少数量
     * @var int
     */
    const EXE_TYPE_LACK = 10;

    /**
     * 损坏
     * @var int
     */
    const EXE_TYPE_FAILURE = 20;

    /**
     * 次品
     * @var int
     */
    const EXE_TYPE_QUALITY = 30;

    /**
     * 来货不符
     * @var int
     */
    const EXE_TYPE_ERR = 40;

    /**
     * 临近过期
     * @var int
     */
    const EXE_TYPE_PAST = 50;

    public static $EXCEPTION_TYPE = [
        self::EXE_TYPE_LACK => '缺少数量',
        self::EXE_TYPE_FAILURE => '损坏',
        self::EXE_TYPE_QUALITY => '次品',
        self::EXE_TYPE_ERR => '来货不符',
        self::EXE_TYPE_PAST => '临近过期',
    ];

    protected function getExceptionTypeAttr($value)
    {
        return isset(self::$EXCEPTION_TYPE[$value]) ? self::$EXCEPTION_TYPE[$value] : $value;
    }

    /**
     * 未处理
     * @var int
     */
    const POSE_RES_WAIT = 0;

    /**
     * 退钱不退货
     * @var int
     */
    const POSE_RES_RET_M = 10;

    /**
     * 退钱退货
     * @var int
     */
    const POSE_RES_RET_PAM = 20;


    /**
     * 补发
     * @var int
     */
    const POSE_RES_REISSUE = 30;

    /**
     * 换货
     * @var int
     */
    const POSE_RES_EXCHG = 40;

    /**
     * 无异常
     * @var int
     */
    const POSE_RES_NE = 50;

    /**
     * 确认异常
     * @var int
     */
    const POSE_RES_AFF_EXE = 60;

    public static $DISPOSE_RESULT = [
        self::POSE_RES_WAIT     => '未处理',
        self::POSE_RES_RET_M    => '退钱不退货',
        self::POSE_RES_RET_PAM  => '退钱退货',
        self::POSE_RES_REISSUE  => '补发',
        self::POSE_RES_EXCHG    => '换货',
        self::POSE_RES_NE       => '无异常',
        self::POSE_RES_AFF_EXE  => '确认异常',
    ];

    protected function getDisposeResultAttr($value)
    {
        return isset(self::$DISPOSE_RESULT[$value]) ? self::$DISPOSE_RESULT[$value] : $value;
    }

    /**
     * 关联采购异常详情
     * @return \think\model\relation\HasOne
     * @date 2020/12/29
     * @author longli
     */
    public function info()
    {
        return $this->hasOne(PurchaseTerpInfo::class, 'ref_id', 'eid')
            ->where("id_type", static::getTable());
    }
}
