<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2020/08/25
 * Time: 15:24
 * @link http://www.lmterp.cn
 */

namespace app\common\model;

class ChannelOrders extends BaseModel
{
    protected $pk = 'ch_id';

    protected $autoWriteTimestamp = 'datetime';

    protected $insert = ['create_by', 'update_by'];

    protected $update = ['update_by'];

    public static $CANCEL = [
        self::IS_YES => '取消',
        self::IS_NO => '正常',
    ];

    /**
     * 待揽收
     * @var int
     */
    const TRACK_STATUS_WAIT = 0;

    /**
     * 已揽收
     * @var int
     */
    const TRACK_STATUS_OUT = 10;

    /**
     * 无物流信息
     * @var int
     */
    const TRACK_STATUS_NONE = 20;

    /**
     * 运输中
     * @var int
     */
    const TRACK_STATUS_SHIPPING = 30;

    /**
     * 派送中
     * @var int
     */
    const TRACK_STATUS_PICKUP = 40;

    /**
     * 已签收
     * @var int
     */
    const TRACK_STATUS_RECV = 50;

    /**
     * 运输超时
     * @var int
     */
    const TRACK_STATUS_TIMEOUT = 60;

    /**
     * 派送失败
     * @var int
     */
    const TRACK_STATUS_UNRECV = 70;

    /**
     * 物流异常
     * @var int
     */
    const TRACK_STATUS_EXCEPTION = 80;

    /**
     * 无状态
     * @var int
     */
    const TRACK_STATUS_NOMAPPING = 200;

    public static $TRACK_STATUS = [
        self::TRACK_STATUS_WAIT      => '待揽收',
        self::TRACK_STATUS_OUT       => '已揽收',
        self::TRACK_STATUS_NONE      => '无物流信息',
        self::TRACK_STATUS_SHIPPING  => '运输中',
        self::TRACK_STATUS_PICKUP    => '派送中',
        self::TRACK_STATUS_RECV      => '已签收',
        self::TRACK_STATUS_TIMEOUT   => '运输超时',
        self::TRACK_STATUS_UNRECV    => '派送失败',
        self::TRACK_STATUS_EXCEPTION => '物流异常',
        self::TRACK_STATUS_NOMAPPING => '无状态',
    ];

    /**
     * 映射订单发货状态
     * @var array
     */
    public static $MAPPING_ORDER_SEND = [
        self::TRACK_STATUS_RECV       => Orders::SEND_RECEIVE,
        self::TRACK_STATUS_PICKUP     => Orders::SEND_PICKUP,
        self::TRACK_STATUS_TIMEOUT    => Orders::SEND_TIMEOUT,
        self::TRACK_STATUS_UNRECV     => Orders::SEND_UNRECV,
        self::TRACK_STATUS_EXCEPTION  => Orders::SEND_EXCEPTION,
    ];

    protected function getTrackStatusAttr($value)
    {
        return isset(self::$TRACK_STATUS[$value]) ? self::$TRACK_STATUS[$value] : $value;
    }

    /**
     * 关联订单
     * @return \think\db\Query|\think\model\relation\BelongsTo
     * @date 2020/09/18
     * @author longli
     */
    public function order()
    {
        return $this->belongsTo(Orders::class, 'order_id', 'order_id');
    }

    /**
     * 关联渠道
     * @return \think\model\relation\BelongsTo
     * @date 2020/09/18
     * @author longli
     */
    public function channel()
    {
        return $this->belongsTo(Channel::class);
    }

    public function country()
    {
        return $this->belongsTo(SysCountries::class, 'country_code', 'code_two');
    }

    /**
     * 是否包含订单
     * @param int|Orders $order 订单号或者订单id
     * @param string $trackNum 追踪号
     * @return bool
     * @date 2021/02/24
     * @author longli
     */
    public static function hasOrder($order, $trackNum = '')
    {
        $orderId = is_numeric($order) ? $order : $order->order_id;
        $model = static::where('order_id', $orderId);
        if(!empty($trackNum)) $model->where('track_num', trim($trackNum));
        return !!$model->count();
    }
}
