<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2020/12/27
 * Time: 14:14
 * @link http://www.lmterp.cn
 */

namespace app\common\model;

use app\common\status\BaseStatus;

class FinanceCash extends BaseModel
{
    protected $pk = 'cash_id';

    protected $autoWriteTimestamp = 'datetime';

    protected $insert = ['create_by', 'update_by'];

    protected $update = ['update_by'];

    /**
     * 银行转账
     * @var int
     */
    const CASH_TYPE_BANK = 1;

    /**
     * 支付宝
     * @var int
     */
    const CASH_TYPE_ALIPAY = 2;

    /**
     * 现金
     * @var int
     */
    const CASH_TYPE_CASH = 3;

    public static $CASH_TYPE = [
        self::CASH_TYPE_BANK => '银行转账',
        self::CASH_TYPE_ALIPAY => '支付宝',
        self::CASH_TYPE_CASH => '现金',
    ];

    protected function getCashTypeAttr($value)
    {
        return isset(self::$CASH_TYPE[$value]) ? self::$CASH_TYPE[$value] : $value;
    }

    protected function getInvoiceTypeAttr($value)
    {
        return isset(FinanceInvoiceSetting::$INVOICE_TYPE[$value]) ? FinanceInvoiceSetting::$INVOICE_TYPE[$value] : $value;
    }


    /**
     * 待收款
     * @var int
     */
    const CASH_STATUS_WAIT = 0;

    /**
     * 部分收款
     * @var int
     */
    const CASH_STATUS_PART = 10;

    /**
     * 全部收款
     * @var int
     */
    const CASH_STATUS_ALL = 20;

    /**
     * 对方拒付
     * @var int
     */
    const CASH_STATUS_REJECT = 30;

    /**
     * 部分退款
     * @var int
     */
    const CASH_STATUS_RETURN_PART = 40;

    /**
     * 全部退款
     * @var int
     */
    const CASH_STATUS_RETURN_ALL = 50;

    /**
     * 取消
     * @var int
     */
    const CASH_STATUS_CANCEL = 60;

    public static $CASH_STATUS = [
        self::CASH_STATUS_WAIT => '待收款',
        self::CASH_STATUS_PART => '部分收款',
        self::CASH_STATUS_ALL => '全部收款',
        self::CASH_STATUS_REJECT => '对方拒付',
        self::CASH_STATUS_RETURN_PART => '部分退款',
        self::CASH_STATUS_RETURN_ALL => '全部退款',
        self::CASH_STATUS_CANCEL => '取消',
    ];

    protected function getCashStatusAttr($value)
    {
        return isset(self::$CASH_STATUS[$value]) ? self::$CASH_STATUS[$value] : $value;
    }

    protected function getRefTypeAttr($value)
    {
        return isset(BaseStatus::$REF_TYPE[$value]) ? BaseStatus::$REF_TYPE[$value] : $value;
    }

    /**
     * 关联后台用户
     * @return \think\model\relation\BelongsTo
     * @date 2021/02/08
     * @author longli
     */
    public function cashUser()
    {
        return $this->belongsTo(Admin::class, "cash_user_id", "id");
    }

    /**
     * 关联附件
     * @return \think\model\relation\HasMany
     * @date 2021/02/08
     * @author longli
     */
    public function attachment()
    {
        return $this->hasMany(SysAttachment::class, 'ref_id', 'cash_id')
                ->where("id_type", self::getTable());
    }

    /**
     * 关联发票配置
     * @return \think\model\relation\BelongsTo
     * @date 2021/01/15
     * @author longli
     */
    public function invoice()
    {
        if($this->is_invoice == self::IS_NO || empty($this->invoice_id)) return null;
        return $this->belongsTo(FinanceInvoiceSetting::class, 'invoice_id', 'invoice_id');
    }

    /**
     * 收款单是否存在
     * @param string $orderSn 业务单号
     * @param string $idType 业务主表
     * @return bool
     * @date 2020/12/27
     * @author longli
     */
    public static function hasOrder($orderSn, $idType = '')
    {
        $model = static::where("ref_sn", trim($orderSn));
        if(!empty($idType)) $model->where("ref_id_type", $idType);
        return !!$model->count();
    }

    /**
     * 收款单应收金额是否存在
     * @param string $orderSn 业务单号
     * @param float $money 金额
     * @param string $idType 业务主表
     * @return bool
     * @date 2020/12/27
     * @author longli
     */
    public static function hasOrderMoney($orderSn, $money, $idType = '')
    {
        $model = static::where([
            "ref_sn" => trim($orderSn),
            "cash_money" => $money
        ]);
        if(!empty($idType)) $model->where("ref_id_type", $idType);
        return !!$model->count();
    }

    /**
     * 获取收款订单总金额
     * @param string $orderSn 订单号
     * @param string $idType 订单号类型
     * @param array $where 自定义条件
     * @return float
     * @date 2020/12/20
     * @author longli
     */
    public static function getTotalMoneyBySn($orderSn, $idType = '', $where = [])
    {
        $model = static::where([
            ["ref_sn", "eq", $orderSn],
            ["cash_status", "in", [self::CASH_STATUS_PART, self::CASH_STATUS_ALL]]
        ]);
        if(!empty($idType)) $model->where("ref_id_type", $idType);
        if(!empty($where)) $model->where($where);
        return floatval($model->sum('real_money') + $model->sum('service_charge'));
    }
}
