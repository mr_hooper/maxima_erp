<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2020/11/14
 * Time: 17:10
 * @link http://www.lmterp.cn
 */

namespace app\common\model;

class WarehouseArea extends BaseModel
{
    protected $pk = 'area_id';

    protected $insert = ['create_by'];

    /**
     * 关联仓库
     * @return \think\model\relation\BelongsTo
     * @date 2021/02/18
     * @author longli
     */
    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class);
    }

    /**
     * 关联货架
     * @return \think\model\relation\HasMany
     * @date 2021/02/20
     * @author longli
     */
    public function shelf()
    {
        return $this->hasMany(WarehouseShelf::class, 'area_id', 'area_id');
    }

    /**
     * 仓库区域是名称是否存在
     * @param string $name 区域名称
     * @param int $warehouseId 仓库id
     * @return bool
     * @date 2021/02/20
     * @author longli
     */
    public static function hasName($name, $warehouseId = 0)
    {
        $area = static::where('name', trim($name));
        if(!empty($warehouseId)) $area->where('warehouse_id', $warehouseId);
        return !!$area->count();
    }

    /**
     * 获取仓库可用区域
     * @param int $wid
     * @return WarehouseArea[]
     * @date 2021/02/20
     * @author longli
     */
    public static function getArea($wid = 0)
    {
        $area = static::where('status', static::IS_YES);
        if(!empty($wid)) $area->where('warehouse_id', $wid);
        return $area->select();
    }
}