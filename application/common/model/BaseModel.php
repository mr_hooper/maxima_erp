<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2020/02/25
 * Time: 15:24
 * @link http://www.lmterp.cn
 */

namespace app\common\model;

use app\common\library\Tools;
use think\Model;

abstract class BaseModel extends Model
{
    /**
     * 是
     * @var int
     */
    const IS_YES = 1;

    /**
     * 否
     * @var int
     */
    const IS_NO = 0;

    public static $IS_STATUS = [
        self::IS_NO => '否',
        self::IS_YES => '是',
    ];

    protected function initialize()
    {
        parent::initialize();
    }

    /**
     * 自动生成创建者id
     * @return int
     * @date 2021/02/10
     * @author longli
     */
    protected function setCreateByAttr()
    {
        if(Tools::isCli()) return 0;
        $admin = session('lmterp');
        return $admin ? $admin->id : 0;
    }

    /**
     * 自动生成更新者id
     * @return int
     * @date 2021/02/10
     * @author longli
     */
    protected function setUpdateByAttr()
    {
        if(Tools::isCli()) return 0;
        $admin = session('lmterp');
        return $admin ? $admin->id : 0;
    }

    /**
     * 获取审批状态
     * @param int $value
     * @return string
     * @date 2021/02/10
     * @author longli
     */
    protected function getCheckStatusAttr($value)
    {
        return isset(JobFlowModule::$CHECK_STATUS[$value]) ? JobFlowModule::$CHECK_STATUS[$value] : $value;
    }

    /**
     * 初始化监听事件
     * @date 2020/07/13
     * @author longli
     */
    protected static function init()
    {
        static::beforeDelete(function ($data)
        {
            $tableName = static::getTable();
            if(!($data instanceof static) || $tableName == SysTrash::getTable()) return;
            // 处理数据到回收站
            $in = config('include_table');
            $out = config('exclude_table');
            if (($in == '*' || in_array($tableName, $in)) && !($out == '*' || in_array($tableName, $out)))
            {
                $pk = $data->getPk();
                $data = $data->getData();
                SysTrash::create([
                    'tablename' => $tableName,
                    'id'        => $data[$pk],
                    'data'      => serialize($data),
                ]);
            }
        });
    }

    /**
     * 过虑掉多余不需要插入的字段
     * @param array $data 需要过虑插入的数据
     * @param string|int 需要过虑掉的值
     * @date 2020/07/13
     * @author longli
     * @return array
     */
    public static function getFilterField(array $data, $filter = null)
    {
        $t = array_flip(static::getTableFields());
        if(!is_array($filter)) $filter = [$filter];
        return array_filter(array_intersect_key($data, $t), function($item)use($filter)
        {
            return $item !== null && !in_array($item, $filter, true);
        });
    }

    /**
     * 获取所有表名
     * @return array
     * @date 2020/12/01
     * @author longli
     */
    public static function getTables()
    {
        $table = [];
        foreach(\think\Db::query("show tables") as $tableName)
        {
            $name = current($tableName);
            $table[] = trim($name);
        }
        return $table;
    }

    /**
     * 转换id，为当前模型对象
     * @param int|static $obj 模型ID, 或者当前对象
     * @return null|static
     * @date 2020/12/30
     * @author longli
     */
    public static function getObj($obj)
    {
        if(!($obj instanceof static)) $obj = static::get($obj);
        return $obj;
    }

    /**
     *  刷新模型数据
     * @return static
     * @date 2020/12/15
     * @author longli
     */
    public function refresh()
    {
        $pk = $this->getPk();
        $this->data(static::get($this->{$pk})->getData());
        return $this;
    }
}