<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2020/09/08
 * Time: 13:52
 * @link http://www.lmterp.cn
 */

namespace app\common\model;

class AccountPlatformWarn extends BaseModel
{
    protected $pk = 'warn_id';

    protected $autoWriteTimestamp = 'datetime';

    protected $insert = ['create_by', 'update_by'];

    protected $update = ['update_by'];
}
