<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2020/12/06
 * Time: 14:29
 * @link http://www.lmterp.cn
 */

namespace app\common\model;

use app\common\library\Tools;

class CsEmailCategory extends BaseModel
{
    protected $pk = 'cate_id';

    protected $autoWriteTimestamp = 'datetime';

    protected $insert = ['create_by', 'update_by'];

    protected $update = ['update_by'];

    public function son()
    {
        return $this->hasMany(CsEmailCategory::class, 'parent_id', 'cate_id');
    }

    public function parent()
    {
        return $this->belongsTo(CsEmailCategory::class, 'parent_id', 'cate_id');
    }

    /**
     * 获取所有邮件分类
     * @return CsEmailCategory[]
     * @date 2020/12/09
     * @author longli
     */
    public static function getAll()
    {
        return static::where("status", self::IS_YES)
            ->order("sort desc")->select();
    }

    /**
     * 获取所有邮件分类，并生成树形结构数据
     * @return array
     * @date 2020/12/09
     * @author longli
     */
    public static function getAllByTree()
    {
        $cates = static::getAll()->toArray();
        $cates = Tools::generateTree($cates, 'cate_id', 'parent_id');
        $cates = Tools::levelArray($cates);
        return $cates;
    }

    /**
     * 关联邮件模板
     * @return \think\model\relation\HasMany
     * @date 2020/12/09
     * @author longli
     */
    public function template()
    {
        return $this->hasMany(CsEmailTemplate::class, 'cate_id', 'cate_id');
    }
}
