<?php
/**
 * Created by Administrator
 * User: longli
 * VX: isa1589518286
 * Date: 2020/08/12
 * Time: 18:17
 * @link http://www.lmterp.cn
 */

namespace app\common\model;


class ProductPlatformSku extends BaseModel
{
    protected $pk = 'pid';

    protected $autoWriteTimestamp = 'datetime';

    protected $updateTime = false;

    // 设置 json 字段
    protected $json = ['ext_json'];

    protected $insert = ['create_by'];


    public static $STATUS = [
        self::IS_YES => '启用',
        self::IS_NO => '关闭'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function store()
    {
        return $this->belongsTo(ProductStore::class, 'store_id', 'store_id');
    }

    public function account()
    {
        return $this->belongsTo(Account::class);
    }

    /**
     * 平台 sku 是否存在
     * @param string $psku 平台 sku
     * @return bool
     * @date 2020/09/15
     * @author longli
     */
    public static function hasPlatformSku($psku)
    {
        return !!static::where(["platform_sku" => $psku])->count();
    }

    /**
     * 账号对应的变体是否存在
     * @param int $accountId 账号id
     * @param int $storeId 变体id
     * @return bool
     * @date 2020/09/15
     * @author longli
     */
    public static function hasByStoreAccount($accountId, $storeId)
    {
        return !!static::where(["store_id" => $storeId, "account_id" => $accountId])->count();
    }
}