<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2020/11/14
 * Time: 17:10
 * @link http://www.lmterp.cn
 */

namespace app\common\model;

class JobFlowSettings extends BaseModel
{
    protected $pk = 'settings_id';

    protected $autoWriteTimestamp = 'datetime';

    protected $insert = ['create_by', 'update_by'];

    protected $update = ['update_by'];

    /**
     * 状态审批模块
     * @return \think\model\relation\BelongsTo
     * @date 2020/12/01
     * @author longli
     */
    public function flow()
    {
        return $this->belongsTo(JobFlowModule::class, 'flow_id', 'flow_id');
    }
}
