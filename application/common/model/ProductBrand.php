<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2020/09/08
 * Time: 13:52
 * @link http://www.lmterp.cn
 */

namespace app\common\model;

class ProductBrand extends BaseModel
{
    protected $pk = 'brand_id';

    public function products()
    {
        return $this->hasMany(ProductBrand::class, 'brand_id', 'brand_id');

    }

    /**
     * 检查品牌是否存在
     * @param string $brand 品牌中文名或英文名
     * @return bool
     * @date 2020/09/11
     * @author longli
     */
    public static function hasBrand($brand)
    {
        return !!static::whereOr('name_ch', $brand)
                ->whereOr('name_en', $brand)
                ->count();
    }

    /**
     * 获取名称获取品牌
     * @param string $brand 品牌名
     * @return bool
     * @date 2020/09/11
     * @author longli
     */
    public static function getByName($brand)
    {
        return static::whereOr('name_ch', $brand)
            ->whereOr('name_en', $brand)
            ->find();
    }

    /**
     * 获取所有品牌
     * @return array|\PDOStatement|string|\think\Collection
     * @date 2020/09/15
     * @author longli
     */
    public static function getAll()
    {
        return static::where(['status' => self::IS_YES])->order("sort desc, brand_id desc")->select();
    }

    /**
     * 更新品牌信息
     * @param int|int[] $ids 规格 id
     * @param array $data 规格信息
     * @return bool
     * @date 2020/09/15
     * @author longli
     */
    public static function editByBrandId($ids, $data)
    {
        try
        {
            foreach(static::where("brand_id", "in", $ids)->select() as $spec)
            {
                $spec->save($data);
            }
            return true;
        }catch(\Exception $e)
        {
            return false;
        }
    }
}
