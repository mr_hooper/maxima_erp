<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2020/09/20
 * Time: 17:17
 * @link http://www.lmterp.cn
 */

namespace app\common\model;

class SysExchange extends BaseModel
{
    protected $pk = 'exchange_id';

    public $autoWriteTimestamp = 'datetime';

    protected $createTime = false;
}
