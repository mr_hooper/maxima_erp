<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2021/01/16
 * Time: 20:38
 * @link http://www.lmterp.cn
 */

namespace app\common\model;

class ProductPlatformBarcode extends BaseModel
{
    protected $pk = 'barcode_id';

    /**
     * 关联账号
     * @return \think\model\relation\BelongsTo
     * @date 2021/01/16
     * @author longli
     */
    public function account()
    {
        return $this->belongsTo(Account::class);
    }
}
