<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2020/11/14
 * Time: 17:10
 * @link http://www.lmterp.cn
 */

namespace app\common\model;

class WarehouseTerpInfo extends BaseModel
{
    protected $pk = 'info_id';

    protected $insert = ['create_by'];

    /**
     * 关联调拨单
     * @return \think\model\relation\BelongsTo
     * @date 2021/01/01
     * @author longli
     */
    public function allot()
    {
        if($this->id_type != WarehouseAllot::getTable()) return null;
        return $this->belongsTo(WarehouseAllot::class, 'ref_id', 'allot_id');
    }

    /**
     * 关联借用单
     * @return \think\model\relation\BelongsTo
     * @date 2021/01/04
     * @author longli
     */
    public function borrow()
    {
        if($this->id_type != WarehouseBorrow::getTable()) return null;
        return $this->belongsTo(WarehouseBorrow::class, 'ref_id', 'borrow_id');
    }

    /**
     * 关联商品变体
     * @return \think\model\relation\BelongsTo
     * @date 2021/03/01
     * @author longli
     */
    public function store()
    {
        return $this->belongsTo(ProductStore::class, 'store_id', 'store_id');
    }

    /**
     * 关联仓库
     * @return \think\model\relation\BelongsTo
     * @date 2021/06/18
     * @author longli
     */
    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class, 'warehouse_id', 'warehouse_id');
    }

    /**
     * 关联盘点单
     * @return \think\model\relation\BelongsTo
     * @date 2021/06/18
     * @author longli
     */
    public function check()
    {
        return $this->belongsTo(WarehouseCheck::class, 'ref_id', 'check_id');
    }

    /**
     * 关联管理员
     * @return \think\model\relation\BelongsTo
     * @date 2021/06/19
     * @author longli
     */
    public function admin()
    {
        return $this->belongsTo(Admin::class, "create_by", "id");
    }
}
