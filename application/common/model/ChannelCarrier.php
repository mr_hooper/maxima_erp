<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2020/08/25
 * Time: 15:24
 * @link http://www.lmterp.cn
 */

namespace app\common\model;

class ChannelCarrier extends BaseModel
{
    protected $pk = 'carrier_id';

    protected $autoWriteTimestamp = 'datetime';

    // 设置 json 字段
    protected $json = ['ext_json', 'token'];

    protected $insert = ['create_by', 'update_by'];

    protected $update = ['update_by'];

    protected function getPayTypeAttr($value)
    {
        return isset(FinancePayment::$PAY_TYPE[$value]) ? FinancePayment::$PAY_TYPE[$value] : $value;
    }

    public function channels()
    {
        return $this->hasMany(Channel::class, 'carrier_id', 'carrier_id');
    }

    /**
     * 获取所有承运商
     * @return array|\PDOStatement|string|\think\Collection
     * @date 2020/09/15
     * @author longli
     */
    public static function getAll()
    {
        return static::where(['status' => self::IS_YES])->order("sort desc, carrier_name")->select();
    }
}
