<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2020/12/02
 * Time: 14:31
 * @link http://www.lmterp.cn
 */

namespace app\common\model;

class SysAttachment extends BaseModel
{
    protected $pk = 'attach_id';

    protected $insert = ['create_by'];

    /**
     * 合同
     * @var int
     */
    const FILE_TYPE_CONTRACT = 0;

    /**
     * 发票
     * @var int
     */
    const FILE_TYPE_INVOICE = 10;

    /**
     * 其它
     * @var int
     */
    const FILE_TYPE_OTHER = 20;

    public static $FILE_TYPE = [
        self::FILE_TYPE_INVOICE => '发票',
        self::FILE_TYPE_CONTRACT => '合同',
        self::FILE_TYPE_OTHER => '其它',
    ];

    protected function getTypeAttr($value)
    {
        return isset(self::$FILE_TYPE[$value]) ? self::$FILE_TYPE[$value] : $value;
    }

    /**
     * 附件是否有上传过
     * @param string $code 文件 md5 码
     * @return bool
     * @date 2020/12/18
     * @author longli
     */
    public static function hasFile($code)
    {
        if(is_file($code)) $code = md5_file($code);
        return !!static::where('md5_code', $code)->count();
    }
}
