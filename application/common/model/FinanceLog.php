<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2020/12/27
 * Time: 14:40
 * @link http://www.lmterp.cn
 */

namespace app\common\model;

class FinanceLog extends BaseModel
{
    protected $pk = 'log_id';

    protected $insert = ['create_by'];
}
