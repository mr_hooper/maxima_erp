<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2020/11/14
 * Time: 17:10
 * @link http://www.lmterp.cn
 */

namespace app\common\model;

class WarehouseCheck extends BaseModel
{
    protected $pk = 'check_id';

    protected $autoWriteTimestamp = 'datetime';

    protected $insert = ['create_by', 'update_by'];

    protected $update = ['update_by'];

    /**
     * 关联仓库
     * @return \think\model\relation\BelongsTo
     * @date 2021/02/10
     * @author longli
     */
    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class);
    }

    /**
     * 关联盘点详情
     * @return \think\model\relation\HasMany
     * @date 2021/01/01
     * @author longli
     */
    public function info()
    {
        return $this->hasMany(WarehouseTerpInfo::class, 'ref_id', 'check_id')
            ->where("id_type", static::getTable());
    }

    /**
     * 通过id或者单号获取盘点单
     * @param string|int $sn id, 或者单号
     * @return WarehouseCheck
     * @date 2021/01/04
     * @author longli
     */
    public static function getBySn($sn)
    {
        return static::where("check_id", $sn)
            ->whereOr("check_sn", trim($sn))
            ->find();
    }
}
