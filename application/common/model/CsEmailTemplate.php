<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2020/12/06
 * Time: 14:29
 * @link http://www.lmterp.cn
 */

namespace app\common\model;

class CsEmailTemplate extends BaseModel
{
    protected $pk = 'temp_id';

    protected $autoWriteTimestamp = 'datetime';

    // 设置 json 字段
    protected $json = ['variable'];

    protected $insert = ['create_by', 'update_by'];

    protected $update = ['update_by'];

    /**
     * 关联邮件分类
     * @return \think\model\relation\BelongsTo
     * @date 2020/12/09
     * @author longli
     */
    public function category()
    {
        return $this->belongsTo(CsEmailCategory::class, 'cate_id', 'cate_id');
    }

    /**
     * 关联国家
     * @return \think\model\relation\BelongsTo
     * @date 2020/12/09
     * @author longli
     */
    public function country()
    {
        return $this->belongsTo(SysCountries::class, 'lang', 'code_two');
    }
}
