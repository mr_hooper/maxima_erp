<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2021/01/29
 * Time: 15:20
 * @link http://www.lmterp.cn
 */

namespace app\command;

use think\console\Command;
use think\console\Input;
use think\console\Output;

/**
 * 控制台处理基类
 * Class BaseCommand
 * @package app\command
 */
abstract class BaseCommand extends Command
{

    /**
     * chunk 默认执行条数
     * @var int
     */
    protected $defChunk = 1000;

    protected function initialize(Input $input, Output $output)
    {
        \app\common\library\Config::config();
    }

    protected function execute(Input $input, Output $output)
    {
        $this->handler();
    }

    /**
     * 处理程序
     * @return mixed
     * @date 2021/01/29
     * @author longli
     */
    abstract protected function handler();
}