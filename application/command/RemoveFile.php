<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2020/12/11
 * Time: 15:20
 * @link http://www.lmterp.cn
 */
namespace app\command;

use app\common\library\Tools;
use app\common\model\Admin;
use app\common\model\OrdersDetail;
use app\common\model\ProductBrand;
use app\common\model\ProductImage;
use app\common\model\SysPhoto;
use think\facade\Log;

/**
 * 删除无业务逻辑的文件
 * Class RemoveFile
 * @package app\command
 */
class RemoveFile extends BaseCommand
{
    protected function configure()
    {
        // 指令配置
        $this->setName('removefile');
            //->addOption("path", null, Option::VALUE_REQUIRED, "扫描路径");
    }

    protected function handler()
    {
        $this->scanImageDir(); // 扫描图片文件夹
    }

    /**
     * 扫描图片文件夹
     * @date 2020/12/08
     * @author longli
     */
    private function scanImageDir()
    {
        $reDir = "/uploads/image";
        $path = $this->input->hasOption("path")
                ? $this->input->getOption("path")
                : \Env::get("root_path") . "public{$reDir}";
        $fp = opendir($path);
        $count = 0;
        while($dateDir = readdir($fp))
        {
            $rTempDir = "$path/$dateDir";
            if(in_array($dateDir, ['.', '..']) || !is_dir($rTempDir)) continue;
            $tpd = opendir($rTempDir);
            while($file = readdir($tpd))
            {
                if(in_array($file, ['.', '..'])) continue;
                $imageUrl = "$reDir/$dateDir/$file";
                $suffix = strtolower(Tools::getFileSuffix($file));
                if(!in_array($suffix, ['jpg', 'jpeg', 'gif', 'png', 'bmp'])) continue;
                if(!$this->hasImage($imageUrl))
                {
                    if($this->moveTo("$path/$dateDir/$file", "images")) $count++;
                    //@unlink("$path/$dateDir/$file");
                }
            }
            closedir($tpd);
        }
        closedir($fp);
        Log::log("info","删除图片，共删除了【{$count}】张图片");
    }

    /**
     * 图片是否存在数据库中
     * @param string $imgPath 图片路径
     * @date 2020/12/08
     * @author longli
     * @return bool
     */
    private function hasImage($imgPath)
    {
        return ProductImage::where("image_url", $imgPath)->count()
               || SysPhoto::where("image_url", $imgPath)->count()
               || OrdersDetail::where("image_url", $imgPath)->count()
               || Admin::where("face", $imgPath)->count()
               || ProductBrand::where("logo_path", $imgPath)->count();
    }

    /**
     * 移动并备份文件到其它目录
     * @param string $file 源文件路径
     * @param string $type 文件类型
     * @param string $toPath 指定保存路径
     * @return bool
     * @date 2020/12/27
     * @author longli
     */
    private function moveTo($file, $type, $toPath = '')
    {
        if(empty($type)) $type = 'other';
        $src = str_replace(\Env::get("root_path"), '', dirname($file));
        if(empty($toPath)) $toPath = \Env::get("root_path") . "data/backup/{$type}/";
        $toPath .= date('Ymd') . "/$src/";
        if(!is_dir($toPath)) mkdir($toPath, 0777, true);
        $toPath .= basename($file);
        return rename($file, $toPath);
    }
}
