<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2021/04/05
 * Time: 12:38
 * @link http://www.lmterp.cn
 */

namespace app\command;

use app\common\library\Tools;
use app\common\model\FinancePayment;
use app\common\model\Orders;
use app\common\model\Producer;
use app\common\model\ProductPurchase;
use app\common\model\ProductStore;
use app\common\model\Purchase;
use app\common\model\WarehouseStock;
use app\common\service\purchase\PurchaseService;
use think\console\input\Option;
use think\facade\Log;

/**
 * 处理采购单
 * Class Purchase
 * @package app\command
 */
class PurchaseCommand extends BaseCommand
{
    protected function configure()
    {
        $this->setName('purchase')
            ->addOption("action", null, Option::VALUE_REQUIRED, "默认生成采购单，可选: check/generate/generateByOrder")
            ->addOption("ids", null, Option::VALUE_REQUIRED, "订单号 id，多个使用英文逗号隔开");
    }

    protected function handler()
    {
        $action = $this->input->hasOption('action')
            ? $this->input->getOption('action')
            : 'generate';
        $action = 'auto' . ucfirst($action);
        if(!method_exists($this, $action)) return;
        $this->$action();
    }

    /**
     * 自动核销采购单
     * @date 2021/04/07
     * @author longli
     */
    private function autoCheck()
    {
        Log::info("开始核销采购单");
        Purchase::with(['info', 'pren'])->where([
            ['order_status', 'in', [Purchase::ORDER_STATUS_FCHE_ING, Purchase::ORDER_STATUS_IN_PART, Purchase::ORDER_STATUS_IN_SUCC]],
            ['pay_status', 'in', [FinancePayment::PAY_STATUS_ALL, FinancePayment::PAY_STATUS_RET_ALL, FinancePayment::PAY_STATUS_RET_PART]],
        ])->chunk($this->defChunk, function($purchases)
        {
            $service = PurchaseService::getInstance();
            foreach($purchases as $purchase)
            {
                ($msg = $service->verification($purchase)) === true
                    ? Log::info("自动核销采购单【{$purchase->purchase_sn}】")
                    : Log::info($msg);
            }
        });
        Log::info("完成核销采购单");
    }

    /**
     * 生成采购单
     * @date 2021/04/07
     * @author longli
     */
    private function autoGenerate()
    {
        Log::info("开始生成采购单");
        // @todo 生成采购单
        Log::info("完成生成采购单");
    }

    /**
     * 通过缺货订单 id 自动生成采购单
     * @date 2021/06/17
     * @author longli
     */
    private function autoGenerateByOrder()
    {
        // 组装采购信息
        $user = session('lmterp');
        $userId = $user ? $user->id : 1;
        $in = $out = [];
        $where = [
            ["order_status", "in", [Orders::ORDER_LACK, Orders::ORDER_WAIT_PUR]]
        ];
        if($this->input->hasOption('ids'))
        {
            $in = explode(',', $this->input->getOption("ids"));
            $where[] = ["order_id", "in", $in];
        }
        Orders::field(['order_id', 'warehouse_id'])->where($where)->with(['detail'])
        ->chunk($this->defChunk, function($orders)use(&$out, $userId)
        {
            $orderData = [];
            foreach($orders as $order)
            {
                $out[] = $order->order_id;
                $order->order_status = Orders::ORDER_WAIT_PUR_ING;
                $order->save();

                // 组装订单采购信息
                foreach($order->detail as $detail)
                {
                    if(WarehouseStock::isEnough($order->warehouse_id, $detail->sku, $detail->qty)) continue;
                    if(isset($orderData[$order->warehouse_id][$detail->sku]))
                    {
                        $orderData[$order->warehouse_id][$detail->sku]['qty'] += $detail->qty;
                    }
                    else
                    {
                        $orderData[$order->warehouse_id][$detail->sku] = [
                            'sku' => $detail->sku,
                            'qty' => $detail->qty,
                        ];
                    }
                }
            }

            // @todo 未添加供应商单月最大采购金额限制
            // 获取供应商采购信息
            $logistics = $productPurchase = [];
            foreach($orderData as $w => $items)
            {
                foreach($items as $sku => $v)
                {
                    $store = ProductStore::getStoreBySku($sku);
                    if(!($pp = ProductPurchase::where(['product_id' => $store->product_id])->order("is_default desc, price")->find())) continue;
                    $logistics[$w][$pp->producer_id]['logistics_price'] = $pp->start_weight_price;
                    $productPurchase[$w][$pp->producer_id][] = [
                            'price'           => $pp->price,
                            'url'             => $pp->url,
                        ] + $v;
                }
            }

            $pService = PurchaseService::getInstance();
            foreach($productPurchase as $w => $items)
            {
                foreach($items as $producerId => $info)
                {
                    $tomorrow = Tools::addDate(1);
                    $purchase = [
                        'purchase' => [
                            'warehouse_id'    => $w,
                            'logistics_price' => isset($logistics[$w]) ? $logistics[$w][$producerId]['logistics_price'] : 0,
                            'producer_id'     => $producerId,
                            'pay_type'        => \app\common\model\FinancePayment::PAY_TYPE_BANK,
                            'estimate_date'   => $tomorrow,
                            'delivery_date'   => $tomorrow,
                            'buyer_user_id'   => $userId,
                        ],
                        'info' => $info
                    ];
                    $pService->addPurchase($purchase);
                }
            }
        });
        Log::info(($user ? "系统自动" : "用户【{$userId}】") . "生成采购单");
        $this->output->write(!empty($out) ? (Tools::equalsArray($out, $in) ? "全部生成" : "部分生成") : "");
    }
}
