<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2021/03/11
 * Time: 12:38
 * @link http://www.lmterp.cn
 */

namespace app\command;

use app\common\model\Orders;
use app\common\model\Warehouse;
use think\console\input\Option;
use think\facade\Log;

/**
 * 订单分仓
 * Class OrderWarehouse
 * @package app\command
 */
class OrderWarehouse extends BaseCommand
{
    protected function configure()
    {
        $this->setName('order_warehouse')
            ->addOption("start_date", null, Option::VALUE_REQUIRED, "开始导入时间，默认为今天")
            ->addOption("end_date", null, Option::VALUE_REQUIRED, "结束导入时间")
            ->addOption("ids", null, Option::VALUE_REQUIRED, "订单id，多个使用英文逗号分开");
    }

    /**
     * 执行订单分仓逻辑
     * @date 2021/03/11
     * @author longli
     */
    protected function handler()
    {
        $orders = Orders::with(['detail'])->where([
                ['warehouse_id', 'eq', 0],
                ['order_status', 'in', [Orders::ORDER_WAIT]]
            ]);
        if($this->input->hasOption('ids'))
        {
            $orders->whereIn("order_id", replaceStr($this->input->getOption('ids')));
        }
        else
        {
            // 处理时间范围
            $startDate = $this->input->hasOption('start_date')
                ? $this->input->getOption('start_date')
                : date('Y-m-d');
            $endDate = $this->input->hasOption('end_date')
                ? $this->input->getOption('end_date')
                : date('Y-m-d', strtotime("1day"));
            $orders->where([
                ['create_time', 'egt', $startDate],
                ['create_time', 'elt', $endDate]
            ]);
        }
        Log::info('订单开始分仓');
        $orders->chunk($this->defChunk, function($orders)
        {
            foreach($orders as $order)
            {
                if($wid = $this->getWarehouseByOrder($order))
                {
                    $order->warehouse_id = $wid;
                    $order->order_status = Orders::ORDER_WAREHOUSE;
                    $order->save();
                }
            }
        });
        Log::info('订单完成分仓');
    }

    /**
     * 通过订单获取仓库 id
     * @param Orders $order
     * @date 2021/03/11
     * @author longli
     */
    protected function getWarehouseByOrder(Orders $order)
    {
        // @todo 订单分仓未实现逻辑
        $w = Warehouse::getDefault();
        return $w ? $w->warehouse_id : 0;
    }
}
