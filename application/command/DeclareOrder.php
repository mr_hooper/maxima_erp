<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2021/01/28
 * Time: 15:20
 * @link http://www.lmterp.cn
 */
namespace app\command;

use app\common\model\Orders;
use app\common\service\logistics\ChannelService;
use think\console\input\Option;

/**
 * 申报订单
 * Class DeclareOrder
 * @package app\command
 */
class DeclareOrder extends BaseCommand
{
    protected function configure()
    {
        // 指令配置
        $this->setName('declare')
            ->addOption("ids", null, Option::VALUE_REQUIRED, "指定订单id，多个使用逗号分隔");
    }

    protected function handler()
    {
        $orderStatus = [Orders::ORDER_LOCK];
        if(\think\facade\Config::get('param.order_lack', 0))
            $orderStatus = array_merge($orderStatus, [Orders::ORDER_LACK, Orders::ORDER_WAIT_PUR, Orders::ORDER_WAIT_PUR_ING]);
        $where = [
            ["channel_id", "gt", 0],
            ["track_num", "eq", ""],
            ["order_status", "IN", $orderStatus],
            ["send_status", "IN", [Orders::SEND_WAIT, Orders::SEND_UPDATE, Orders::SEND_PART, Orders::SEND_WAIT_TIMEOUT]],
        ];
        if($this->input->hasOption('ids'))
        {
            $ids = replaceStr($this->input->getOption('ids'));
            $where[] = ['order_id', 'in', $ids];
        }
        $orders = Orders::with(["detail"])->where($where)->select();
        $service = ChannelService::getInstance();
        $ids = [];
        foreach($orders as $order)
        {
            $service->declareOrder($order);
            if(!empty($order->track_num)) $ids[] = $order->order_id;
            usleep(100);
        }
        $this->output->write(join(',', $ids));
    }
}