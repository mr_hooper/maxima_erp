<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2021/02/01
 * Time: 09:20
 * @link http://www.lmterp.cn
 */

namespace app\command;

use app\common\library\Tools;
use app\common\model\ChannelOrders;
use app\common\model\Orders;
use think\console\input\Argument;
use think\console\input\Option;
use think\facade\Log;

/**
 * 表之间数据同步
 * Class SyncData
 * @package app\command
 */
class SyncData extends BaseCommand
{
    /**
     * 表主键id
     * @var array
     */
    protected $ids = [];

    /**
     * 开始时间
     * @var string
     */
    protected $startDate;

    /**
     * 结束时间
     * @var string
     */
    protected $endDate;

    protected function configure()
    {
        $this->setName('syncdata')
            ->addArgument("action", Argument::REQUIRED, "操作指令")
            ->addOption("ids", null, Option::VALUE_REQUIRED, "表主键ID")
            ->addOption("start_date", null, Option::VALUE_REQUIRED, "开始时间")
            ->addOption("end_date", null, Option::VALUE_REQUIRED, "结束时间")
            ->setDescription("表之间数据同步");
    }

    /**
     * 初始化成员属性变量
     * @date 2021/02/01
     * @author longli
     */
    protected function initParams()
    {
        $options = ['ids', 'start_date', 'end_date'];
        $toArray = ['ids'];
        foreach($options as $option)
        {
            if($this->input->hasOption($option))
            {
                $name = Tools::toCamelCase($option);
                $value = $this->input->getOption($option);
                $this->$name = in_array($name, $toArray) ? explode(',', $value) : $value;
            }
        }
    }

    protected function handler()
    {
        $action = $this->input->getArgument("action");
        if(!method_exists($this, $action))
        {
            $msg = "the method【{$action}】 is not exists";
            $this->output->writeln($msg);
            Log::info($msg);
            return;
        }
        $this->initParams();
        $this->$action();
    }

    /**
     * 同步物流信息到订单
     * @date 2021/02/01
     * @author longli
     */
    protected function logistics()
    {
        // 过虑查询条件
        $where = [
            ["label_url", "neq", ""],
            ["track_num", "neq", ""],
        ];
        if(!empty($this->ids))
            $where[] = ["ch_id", "in", $this->ids];
        if(!empty($this->startDate))
            $where[] = ["create_time", "egt", $this->startDate];
        if(!empty($this->endDate))
            $where[] = ["create_time", "elt", $this->endDate];
        Log::info("开始同步物流信息到订单");
        ChannelOrders::where($where)->with(['order'])
        ->chunk($this->defChunk, function($chOrders)
        {
            foreach($chOrders as $chOrder)
            {
                $this->output->write(".");
                if(!($order = $chOrder->order) || $order->channel_id != $chOrder->channel_id) continue;
                $order->label_url = $chOrder->label_url;
                $order->track_num = $chOrder->track_num;
                $ts = $chOrder->getData('track_status');
                if(array_key_exists($ts, ChannelOrders::$MAPPING_ORDER_SEND)
                 && !in_array($order->getData('send_status'), [Orders::SEND_RETURN_ING, Orders::SEND_RETURN_ALL, Orders::SEND_RETURN_PART, Orders::SEND_NO_NEED])
                )
                {
                    $order->send_status = ChannelOrders::$MAPPING_ORDER_SEND[$ts];
                }
                $order->save();
            }
            $this->output->writeln("");
        });
        Log::info("完成同步物流信息到订单");
    }
}