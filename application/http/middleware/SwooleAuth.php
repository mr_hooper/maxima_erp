<?php

namespace app\http\middleware;

use app\common\library\Auth;
use app\common\library\Config;
use Closure;
use Exception;
use Firebase\JWT\JWT;
use think\Container;
use think\exception\HttpException;

class SwooleAuth
{
    public function handle($request, Closure $next)
    {
        Config::config();
        $settingsIp = config('param.swoole_allow_ip');
        $allowIp = array_merge(['127.0.0.1', 'localhost'], $settingsIp?:[]);
        if(!in_array(request()->ip(), $allowIp))throw new HttpException(401, '未授权访问');
        return $next($request);
    }
}
