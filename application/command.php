<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: yunwuxin <448901948@qq.com>
// +----------------------------------------------------------------------

return [
    app\command\Backup::class,
    app\command\GenerateModel::class,
    app\command\GetOrder::class,
    app\command\StatOrder::class,
    app\command\RemoveFile::class,
    app\command\RepairOrder::class,
    app\command\Logistics::class,
    app\command\DeclareOrder::class,
    app\command\SyncData::class,
    app\command\Test::class,
    app\command\LockStock::class,
    app\command\OrderChannel::class,
    app\command\OrderWarehouse::class,
    app\command\PurchaseCommand::class,
];
