<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2020/09/19
 * Time: 20:57
 * @link http://www.lmterp.cn
 */

namespace app\swoole\timer;

/**
 * 每小时执行入口
 * Class OneSecond
 * @package app\swoole\timer
 */
class OneHourTimer extends BaseTimer
{
    public function run()
    {
        echo "\t\t\t每小时执行  " . date('Y-m-d H:i:s') . "\n";
        // 拉取物流信息
        \think\Console::call('logistics');
        // 占用库存
        $this->lockStock(30);
    }

    /**
     * 占用库存
     * @param int $day 指定多少天内的订单，默认为今天
     * @date 2021/03/02
     * @author longli
     */
    private function lockStock($day = 0)
    {
        if($day < 0) $day = 0;
        $startDate = date('Y-m-d', strtotime("-{$day}day"));
        $endDate = date('Y-m-d H:i:s');
        $actions = ['order'];
        foreach($actions as $action)
        {
            \think\Console::call('lock_stock', [
                "--action={$action}",
                "--type=lock",
                "--start_date={$startDate}",
                "--end_date={$endDate}"
            ]);
        }
    }
}