<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2020/09/19
 * Time: 21:00
 * @link http://www.lmterp.cn
 */

namespace app\swoole\timer;

use think\Console;

/**
 * 每分钟执行入口
 * Class OneMinute
 * @package app\swoole\timer
 */
class OneMinuteTimer extends BaseTimer
{
    public function run()
    {
        echo "\t每分钟执行  " . date('Y-m-d H:i:s') . "\n";
        $this->declareOrder();
    }

    /**
     * 申报订单获取追踪号和面单
     * @date 2020/09/25
     * @author longli
     */
    private function declareOrder()
    {
        Console::call('declare');
    }
}