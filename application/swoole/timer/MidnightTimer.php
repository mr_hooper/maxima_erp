<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2020/09/19
 * Time: 21:03
 * @link http://www.lmterp.cn
 */

namespace app\swoole\timer;

use think\Console;

/**
 * 每天凌晨执行入口
 * Class MidnightTimer
 * @package app\swoole\timer
 */
class MidnightTimer extends BaseTimer
{
    public function run()
    {
        echo "\t\t\t\t凌晨12点执行  " . date('Y-m-d H:i:s') . "\n";
        $this->countOrder(45); // 统计订单
        $this->purchaseVerification(); // 核销采购单
    }

    /**
     * 统计每天的订单量,
     * @param int $day 指定需要更新前几天的统计量
     * @date 2020/12/13
     * @author longli
     */
    private function countOrder($day = 1)
    {
        if($day < 1) $day = 1;
        for($i = $day; $i > 0; $i--)
        {
            $diff = $i - 1;
            $oneDay = date('Y-m-d', strtotime("-{$i}day"));
            $twoDay = date('Y-m-d', strtotime("-{$diff}day"));
            Console::call('statorder', ["--start_date={$oneDay}", "--end_date={$twoDay}"]);
        }
    }

    /**
     * 核销采购单
     * @date 2021/04/09
     * @author longli
     */
    private function purchaseVerification()
    {
        Console::call('purchase', ["--action=check"]);
    }
}