<?php

return [
    // 财务
    'finance' => [
        \app\common\behavior\finance\Cash::class,
        \app\common\behavior\finance\Payment::class
    ],
    // 审批
    'check_status' => [
        \app\common\behavior\check\PurchaseCheck::class,
        \app\common\behavior\check\PurchaseExchangeCheck::class,
        \app\common\behavior\check\PurchaseReturnCheck::class,
        \app\common\behavior\check\WarehouseAllotCheck::class,
        \app\common\behavior\check\WarehouseBorrowCheck::class,
        \app\common\behavior\check\WarehouseCheckCheck::class,
        \app\common\behavior\check\FinancePaymentCheck::class,
        \app\common\behavior\check\FinanceCashCheck::class,
    ],
    // 订单
    'order' => [
        \app\common\behavior\order\LockStock::class,
        \app\common\behavior\order\ChoiceChannel::class,
    ]
];