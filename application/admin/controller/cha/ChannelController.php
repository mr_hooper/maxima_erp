<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2020/09/04
 * Time: 17:14
 * @link http://www.lmterp.cn
 */

namespace app\admin\controller\cha;

use app\admin\controller\BaseController;
use app\common\model\Channel;
use app\common\model\ChannelCarrier;
use app\common\service\logistics\ChannelService;
use think\facade\Validate;

/**
 * 渠道管理
 * Class IndexController
 * @package app\admin\controller\channel
 */
class ChannelController extends BaseController
{
    /**
     * 渠道列表
     * @date 2020/09/18
     * @author longli
     */
    public function index()
    {
        if(!$this->request->isAjax()) return $this->fetch('index');
        $this->assign('list', Channel::with(['carrier'])->order("sort desc,channel_name")->select());
        return $this->fetch('lists');
    }

    /**
     * 更新渠道
     * @date 2020/09/18
     * @author longli
     */
    public function save()
    {
        if($this->request->isPost())
        {
            $data = $this->request->post();
            // 普通验证
            $validate = Validate::make([
                'carrier_id'  => 'require',
                'channel_name'  => 'require',
                'channel_type'  => 'require',
                'day_max_shipping'  => 'egt:0',
                'default_weight'  => 'egt:0',
                'default_discount'  => 'egt:0',
                'label_width'  => 'egt:0',
                'label_height'  => 'egt:0',
            ],[
                'carrier_id.require' => '渠道必选',
                'channel_name.require' => '渠道名称必填',
                'channel_type.require' => '渠道类型必选',
                'day_max_shipping.egt' => '单日发货量有误',
                'default_weight.egt' => '起重有误',
                'default_discount.egt' => '拆扣有误',
                'label_width.egt' => '面单宽度有误',
                'label_height.egt' => '面单高度有误',
            ]);
            if(!$validate->batch()->check($data))$this->error(join(', ', $validate->getError()));
            if(empty($data['label_width'])) $data['label_width'] = 100;
            if(empty($data['label_height'])) $data['label_height'] = 100;
            if(!isset($data['status'])) $data['status'] = Channel::STATUS_N;
        }
        else
        {
            $validate = Validate::make([
                'channel_id'  => 'require',
            ],[
                'channel_id.require' => '非法请求',
            ]);
            $data = $this->request->only(['channel_id', 'status'], 'get');
            if(!$validate->batch()->check($data))$this->error(join(', ', $validate->getError()));
        }
        ChannelService::getInstance()->addChannel($data)
            ? $this->success('操作成功')
            : $this->error('操作失败');
    }

    /**
     * 录入渠道
     * @date 2020/09/18
     * @author longli
     */
    public function add()
    {
        $this->assign("channel_type", Channel::$CHANNEL_TYPE);
        $this->assign("carrier", ChannelCarrier::getAll());
        $this->assign("is_track", Channel::$IS_TRACK);
        $this->assign("track_type", Channel::$TRACK_TYPE);
        $this->assign("label_type", Channel::$LABEL_TYPE);
        $channelId = $this->request->request("channel_id");
        if(!empty($channelId)) $this->assign("channel", Channel::get($channelId));
        return $this->fetch("add");
    }

    /**
     * 删除渠道
     * @date 2020/09/18
     * @author longli
     */
    public function delete()
    {
        $channelId = $this->request->request("ids");
        if(empty($channelId)) $this->error('参数错误');
        Channel::destroy($channelId);
        $this->success('删除成功');
    }

    /**
     * 获取渠道列表
     * @date 2020/09/18
     * @author longli
     */
    public function channel_list()
    {
        $carrierId = $this->request->request("carrier_id");
        if(empty($carrierId)) return apiResponse(\app\common\status\BaseStatus::CODE_FAULT, [], '承运商必传');
        $data = Channel::getByCarrierId($carrierId);
        return apiResponse(\app\common\status\BaseStatus::CODE_NORMAL, $data, '获取成功');
    }
}