<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2021/03/04
 * Time: 15:20
 * @link http://www.lmterp.cn
 */

namespace app\admin\controller\cha;

use app\admin\controller\BaseController;
use app\common\model\ChannelExpress;
use app\common\service\logistics\ChannelService;
use think\facade\Validate;

/**
 * 国内快递管理
 * Class ExpressController
 * @package app\admin\controller\cha
 */
class ExpressController extends BaseController
{
    /**
     * 快递列表
     * @date 2021/03/04
     * @author longli
     */
    public function index()
    {
        if(!$this->request->isAjax()) return $this->fetch('index');
        $this->assign('list', ChannelExpress::order("sort desc")->select());
        return $this->fetch('lists');
    }

    /**
     * 更新快递
     * @date 2021/03/04
     * @author longli
     */
    public function save()
    {
        if($this->request->isPost())
        {
            $data = $this->request->post();
            // 普通验证
            $validate = Validate::make([
                'logistics_name'  => 'require',
            ],[
                'logistics_name.require' => '快递名称必填',
            ]);
            if(!$validate->batch()->check($data)) $this->error(join(', ', $validate->getError()));
            if(!isset($data['status'])) $data['status'] = ChannelExpress::IS_NO;
        }
        else
        {
            $validate = Validate::make([
                'express_id'  => 'require',
            ],[
                'express_id.require' => '非法请求',
            ]);
            $data = $this->request->only(['express_id', 'status'], 'get');
            if(!$validate->batch()->check($data))$this->error(join(', ', $validate->getError()));
        }
        ChannelService::getInstance()->addExpress($data)
            ? $this->success('操作成功')
            : $this->error('操作失败');
    }

    /**
     * 录入快递
     * @date 2021/03/04
     * @author longli
     */
    public function add()
    {
        $expressId = $this->request->request("express_id");
        if(!empty($expressId)) $this->assign("express", ChannelExpress::get($expressId));
        return $this->fetch("add");
    }

    /**
     * 删除快递
     * @date 2021/03/04
     * @author longli
     */
    public function delete()
    {
        $expressId = $this->request->request("ids");
        if(empty($expressId)) $this->error('参数错误');
        ChannelExpress::destroy($expressId);
        $this->success('删除成功');
    }
}