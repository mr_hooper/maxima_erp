<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2020/09/07
 * Time: 13:29
 * @link http://www.lmterp.cn
 */

namespace app\admin\controller\cra;

use app\admin\controller\BaseController;

class IndexController extends BaseController
{
    public function index()
    {
        return $this->fetch("index");
    }
}