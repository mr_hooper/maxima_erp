<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2021/02/18
 * Time: 10:29
 * @link http://www.lmterp.cn
 */

namespace app\admin\controller\wms;

use app\admin\controller\BaseController;
use app\common\model\Warehouse;
use app\common\model\WarehouseArea;
use app\common\model\WarehouseShelf;
use app\common\service\product\WarehouseService;

/**
 * 货架管理
 * Class WshelfController
 * @package app\admin\controller\wms
 */
class WshelfController extends BaseController
{
    /**
     * 货架列表
     * @date 2021/02/18
     * @author longli
     */
    public function index()
    {
        if($this->request->isAjax())
        {
            $shelf = WarehouseShelf::with(['warehouse', 'area'])
                ->where('warehouse_id', $this->warehouse->warehouse_id)
                ->order('warehouse_id, shelf_id desc');
            $this->searchModel($shelf, [
                'eq' => ['area_id', 'code', 'status'],
                'times' => ['create_time']
            ]);
            $limit = $this->getPageSize();
            $shelf = $shelf->paginate($limit);
            $this->assign("list", $shelf->getCollection());
            $this->assign("page", $shelf->render());
            return $this->fetch('lists');
        }
        $this->assign('status', WarehouseShelf::$STATUS);
        return $this->fetch('index');
    }

    /**
     * 添加货架
     * @date 2021/02/18
     * @author longli
     */
    public function add()
    {
        $shelfId = $this->request->get('shelf_id');
        if(!empty($shelfId) && ($shelf = WarehouseShelf::get($shelfId)))
        {
            $this->assign('shelf', $shelf);
        }
        $this->assign('area', WarehouseArea::getArea($this->warehouse->warehouse_id));
        $this->assign('status', WarehouseShelf::$STATUS);
        return $this->fetch('add');
    }

    /**
     * 保存货架
     * @date 2021/02/18
     * @author longli
     */
    public function save()
    {
        $data = [
            'status' => $this->request->request('status', WarehouseShelf::IS_NO),
            'warehouse_id' => $this->warehouse->warehouse_id,
        ];
        if($this->request->isPost())
        {
            $this->validateSave();
            $data += $this->request->post();
            if(empty($data['code'])) $data = ['code' => WarehouseShelf::generateCode()] + $data;
        }
        else
        {
            $shelfId = $this->request->get('shelf_id');
            if(empty($shelfId)) $this->error('非法请求');
            $data += ['shelf_id' => $shelfId];
        }
        WarehouseService::getInstance()->addShelf($data)
            ? $this->success('操作成功')
            : $this->error('操作失败');
    }

    /**
     * 验证保存货架
     * @date 2021/02/18
     * @author longli
     */
    private function validateSave()
    {
        $validate = \think\facade\Validate::make([
            'area_id'  => 'require',
            'level'  => 'egt:0',
        ],[
            'area_id.require' => '区域必选',
            'level.egt' => '货架层级不能小于0',
        ]);
        $data = $this->request->post();
        if(!$validate->batch()->check($data)) $this->error(join(', ', $validate->getError()));
        if(!isset($data['shelf_id']) && !empty($data['code']) && WarehouseShelf::hasCode($data['code'], $this->warehouse->warehouse_id))
        $this->error('货架编号已存在');
    }

    /**
     * 获取货架列表
     * @date 2021/02/20
     * @author longli
     */
    public function get()
    {
        $wid = $this->request->get('warehouse_id');
        if(empty($wid)) $this->error('仓库必传');
        $aid = $this->request->get('area_id');
        $shelf = WarehouseShelf::getShelf($wid, $aid);
        $this->success('请求成功', null, $shelf);
    }
}