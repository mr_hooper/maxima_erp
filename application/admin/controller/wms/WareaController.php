<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2021/02/18
 * Time: 10:29
 * @link http://www.lmterp.cn
 */

namespace app\admin\controller\wms;

use app\admin\controller\BaseController;
use app\common\model\Warehouse;
use app\common\model\WarehouseArea;
use app\common\service\product\WarehouseService;

/**
 * 区域管理
 * Class WlocationController
 * @package app\admin\controller\wms
 */
class WareaController extends BaseController
{
    /**
     * 区域列表
     * @date 2021/02/18
     * @author longli
     */
    public function index()
    {
        if($this->request->isAjax())
        {
            $area = WarehouseArea::with(['warehouse'])
                ->where('warehouse_id', $this->warehouse->warehouse_id)
                ->order('area_id desc');
            $this->searchModel($area, [
               'eq' => ['name', 'status'],
               'range' => ['shelf_qty']
            ]);
            $limit = $this->getPageSize();
            $area = $area->paginate($limit);
            $this->assign("list", $area->getCollection());
            $this->assign("page", $area->render());
            return $this->fetch('lists');
        }
        $this->assign("status", ["禁用", "可用"]);
        return $this->fetch('index');
    }

    /**
     * 添加区域
     * @date 2021/02/18
     * @author longli
     */
    public function add()
    {
        $areaId = $this->request->get('area_id');
        if(!empty($areaId) && ($area = WarehouseArea::get($areaId)))
            $this->assign('area', $area);
        return $this->fetch('add');
    }

    /**
     * 保存区域
     * @date 2021/02/18
     * @author longli
     */
    public function save()
    {
        $data = [
            'status' => $this->request->request('status', WarehouseArea::IS_NO),
            'warehouse_id' => $this->warehouse->warehouse_id,
        ];
        if($this->request->post())
        {
            $this->validateSave();
            $data += $this->request->post();
        }
        else
        {
            $areaId = $this->request->get('area_id');
            if(empty($areaId)) $this->error('非法请求');
            $data += [
                'area_id' => $areaId
            ];
        }
       WarehouseService::getInstance()->addArea($data)
           ? $this->success('操作成功')
           : $this->error('操作失败');
    }

    /**
     * 验证调拨单
     * @date 2021/02/18
     * @author longli
     */
    private function validateSave()
    {
        $validate = \think\facade\Validate::make([
            'name'  => 'require',
            'code'  => 'require',
            'area'  => 'egt:0',
        ],[
            'name.require' => '区域名称必填',
            'code.require' => '区域编号必填',
            'area.egt' => '区域面积有误',
        ]);
        $data = $this->request->post();
        if(!$validate->batch()->check($data)) $this->error(join(', ', $validate->getError()));
        if(!isset($data['area_id']) && WarehouseArea::hasName($this->warehouse->name, $this->warehouse->warehouse_id))
            $this->error('区域名称已存在');
    }

    /**
     * 删除区域
     * @date 2021/02/18
     * @author longli
     */
    public function delete()
    {
        $ids = $this->request->get('ids');
        if(empty($ids)) $this->error('非法请求');
        WarehouseArea::destroy($ids);
        $this->success('删除成功');
    }

    /**
     * 获取区域列表
     * @date 2021/02/20
     * @author longli
     */
    public function get()
    {
        $wid = $this->request->get('warehouse_id');
        if(empty($wid)) $this->error('仓库必传');
        $area = WarehouseArea::getArea($wid);
        $this->success('请求成功', null, $area);
    }
}