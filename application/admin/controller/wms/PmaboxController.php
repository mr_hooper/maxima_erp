<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2021/04/19
 * Time: 10:39
 * @link http://www.lmterp.cn
 */

namespace app\admin\controller\wms;

use app\admin\controller\BaseController;
use app\common\model\ProductAmazonBox;
use app\common\service\product\PlatformService;

/**
 * 亚马逊箱唛
 * Class Pmabox
 * @package app\admin\controller\wms
 */
class PmaboxController extends BaseController
{
    /**
     * 箱唛列表
     * @date 2021/04/19
     * @author longli
     */
    public function index()
    {
        if($this->request->isAjax())
        {
            $limit = $this->getPageSize();
            $boxCode = ProductAmazonBox::order("box_id desc")->with(['account' => function($query)
            {
                $query->field(['account_id', 'username', 'store_name']);
            }]);
            $this->searchModel($boxCode, [
                'eq' => ['account_id', 'status']
            ]);
            $boxCode = $boxCode->paginate($limit);
            $this->assign("list", $boxCode->getCollection());
            $this->assign("page", $boxCode->render());
            $this->assign('boxcode', $boxCode);
            return $this->fetch("lists");
        }
        $platform = \app\common\model\AccountPlatform::get(['code' => 'AMZ']);
        $this->assign('accounts', $platform->account);
        $this->assign("status", ["否", "是"]);
        return $this->fetch('index');
    }

    /**
     * 添加箱唛
     * @date 2021/04/19
     * @author longli
     */
    public function add()
    {
        $platform = \app\common\model\AccountPlatform::get(['code' => 'AMZ']);
        $this->assign('accounts', $platform->account);
        $codeId = $this->request->get('box_id');
        if(!empty($codeId)) $this->assign("boxcode", ProductAmazonBox::get($codeId));
        return $this->fetch('add');
    }

    /**
     * 保存箱唛
     * @date 2021/04/19
     * @author longli
     */
    public function save()
    {
        if($this->request->isPost())
        {
            $validate = \think\facade\Validate::make([
                'account_id'  => 'require',
                'path_pdf'  => 'require',
                'total_box'  => 'require|egt:0',
            ],[
                'account_id.require' => '账号必选',
                'path_pdf.require' => '平台PDF必传',
                'total_box.require' => '总箱数必填',
                'total_box.egt' => '总箱数必须大于0',
            ]);
            $data = $this->request->post();
            if(!$validate->batch()->check($data))$this->error(join(', ', $validate->getError()));
            if(!isset($data['status'])) $data['status'] = ProductAmazonBox::IS_YES;
        }
        else
        {
            $validate = \think\facade\Validate::make([
                'box_id'  => 'require',
            ],[
                'box_id.require' => '非法请求',
            ]);
            $data = $this->request->only(['box_id', 'status'], 'get');
            if(!$validate->batch()->check($data))$this->error(join(', ', $validate->getError()));
        }
        $boxCode = PlatformService::getInstance()->addAmazonBox($data);
        if($boxCode) $this->success('操作成功', '', ['box_id' => $boxCode->box_id]);
        $this->error('操作失败');
    }

    /**
     * 打印箱唛
     * @date 2021/04/19
     * @author longli
     */
    public function print()
    {
        $boxId = $this->request->get('box_id');
        $index = $this->request->get('index', 0);
        if(empty($boxId) || !($boxCode = ProductAmazonBox::get($boxId))) $this->error('非法操作');
        if($index < 0 || $index > $boxCode->total_box) $this->error("箱号【{$index}】不存在");
        $pdf = PlatformService::getRootPath($boxCode->path_pdf);
        if(!is_file($pdf)) $this->error('平台文件不存在');
        $absFile = config('temp_pdf') . "{$boxId}-{$boxCode->total_box}-{$index}.pdf";
        if(!is_file($absFile)) PlatformService::getInstance()->amazonBox($pdf, $boxCode->total_box, $index, $absFile);
        if(!is_file($absFile)) $this->error('打印打败，请与技术联系解决...');
        $byte = base64_encode(file_get_contents($absFile));
        $this->success('打印成功', '', ['file' => $byte]);
    }
}