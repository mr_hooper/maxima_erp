<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2021/03/18
 * Time: 13:35
 * @link http://www.lmterp.cn
 */

namespace app\admin\controller\wms;

use app\admin\controller\BaseController;
use app\common\model\WarehouseStock;

/**
 * 查看商品详情
 * Class WfindController
 * @package app\admin\controller\wms
 */
class WfindController extends BaseController
{
    /**
     * 查看商品
     * @date 2021/03/18
     * @author longli
     */
    public function index()
    {
        if($this->request->isAjax())
        {
            $sku = $this->request->request('sku', '', 'trim');
            if(empty($sku)) $this->error("非法操作", '', ["type" => "abnormal-operation"]);
            $sku = replaceStr($sku);
            $s = \app\common\model\Product::getSkusBySpu($sku);
            $stock = WarehouseStock::getWarehouseBySkus($this->warehouse, !empty($s) ? $s : $sku);
            if($stock->isEmpty()) $this->error('商品不存在', '', ["type" => "product-not-exist"]);
            $html = view('info', [
                'stock' => $stock
            ])->getContent();
            $this->success('查询成功', '', ["html" => $html, "type" => "operation-success"]);
        }
        return $this->fetch('index');
    }
}