<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2020/09/07
 * Time: 13:29
 * @link http://www.lmterp.cn
 */

namespace app\admin\controller\wms;

use app\admin\controller\BaseController;

/**
 * 相册管理
 * Class PhotoController
 * @package app\admin\controller\wms
 * @link https://eletree.hsianglee.cn/eleTree/demo-icon.html 插件手册
 */
class PhotoController extends BaseController
{
    /**
     * 相册首页
     * @return string
     * @date 2020/12/17
     * @author longli
     */
    public function index()
    {
        // @todo
        return $this->fetch('index');
    }

    /**
     * 相册列表
     * @return string
     * @date 2020/12/17
     * @author longli
     */
    public function list()
    {
        // @todo
    }

    /**
     * 相册移除
     * @return string
     * @date 2020/12/17
     * @author longli
     */
    public function remove()
    {
        // @todo
    }

    /**
     * 相册上传
     * @return string
     * @date 2020/12/17
     * @author longli
     */
    public function upload()
    {
        // @todo
    }
}