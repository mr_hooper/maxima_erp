<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2021/02/16
 * Time: 12:25
 * @link http://www.lmterp.cn
 */

namespace app\admin\controller\wms;

use app\admin\controller\BaseController;
use app\common\model\Orders;
use app\common\model\OrdersReturn;
use app\common\model\Purchase;
use app\common\model\WarehouseAllot;
use app\common\model\WarehouseBorrow;
use app\common\service\product\StockService;

/**
 * 商品上架
 * Class WupController
 * @package app\admin\controller\wms
 */
class WupController extends BaseController
{
    /**
     * 采购上架
     * @date 2021/02/16
     * @author longli
     */
    public function purchase()
    {
        if($this->request->isAjax())
        {
            $sn = $this->request->get('sn', '', 'trim');
            if(empty($sn)) $this->error('非法请求', '', ["type" => "abnormal-operation"]);
            $purchase = null;
            if($purchase = \app\common\model\PurchaseTrack::get(['track_num' => $sn])) $purchase = $purchase->purchase;
            if(!$purchase && !($purchase = Purchase::getBySn($sn)))
                $this->error('采购单不存在', '', ["type" => "order-not-exist"]);
            if($purchase->warehouse_id != $this->warehouse->warehouse_id)
                $this->error("采购单不属于【{$this->warehouse->name}】，不能上架", '', ["type" => "abnormal-operation"]);
            $html = view('purchase_info', [
                'list' => $purchase->info
            ])->getContent();
            $data = compact('html');
            if(($msg = StockService::purchaseStock($purchase)) !== true)
                $this->error($msg, '', $data + ["type" => "up-fail"]);
            $this->success("上架成功", '', $data + ["type" => "up-success"]);
            return $this->fetch('purchase_info');
        }
        $this->assign('msg', '采购单号/追踪号');
        return $this->fetch('wup');
    }

    /**
     * 调拨
     * @date 2021/02/16
     * @author longli
     */
    public function allot()
    {
        if($this->request->isAjax())
        {
            $sn = $this->request->get('sn', '', 'trim');
            if(empty($sn)) $this->error('非法请求', '', ["type" => "abnormal-operation"]);
            $allot = WarehouseAllot::get(['track_num' => $sn]);
            if(!$allot && !($allot = WarehouseAllot::getBySn($sn)))
                $this->error('调拨单不存在', '', ["type" => "order-not-exist"]);
            if($allot->in_id != $this->warehouse->warehouse_id)
                $this->error("调拨单不属于【{$this->warehouse->name}】，不能上架", '', ["type" => "abnormal-operation"]);
            $html = view('allot_info', [
                'list' => $allot->info
            ])->getContent();
            $data = compact('html');
            if(($msg = StockService::allotIn($allot->allot_sn)) !== true)
                $this->error($msg, '', $data + ["type" => "up-fail"]);
            $this->success("上架成功", '', $data + ["type" => "up-success"]);
            return $this->fetch('purchase_info');
        }
        $this->assign('msg', '调拨单号/追踪号');
        return $this->fetch('wup');
    }

    /**
     * 订单退货
     * @date 2021/02/16
     * @author longli
     */
    public function order()
    {
        if($this->request->isAjax())
        {
            $sn = $this->request->get('sn', '', 'trim');
            if(empty($sn)) $this->error('非法请求', '', ["type" => "abnormal-operation"]);
            $order = OrdersReturn::get(['track_num' => $sn]);
            if(!$order && !($order = Orders::getSnNo($sn)))
                $this->error('订单不存在', '', ["type" => "order-not-exist"]);
            if($order instanceof OrdersReturn) $order = $order->order;
            if($order->ren->in_id != $this->warehouse->warehouse_id)
                $this->error("退货订单不属于【{$this->warehouse->name}】，不能上架", '', ["type" => "abnormal-operation"]);
            $html = view('order_info', [
                'list' => $order->detail
            ])->getContent();
            $data = compact('html');
            if(($msg = StockService::orderReturn($order, $order->ren->in_id)) !== true)
                $this->error($msg, '', $data + ["type" => "up-fail"]);
            $this->success("上架成功", '', $data + ["type" => "up-success"]);
        }
        $this->assign('msg', '订单号/追踪号');
        return $this->fetch('wup');
    }

    /**
     * 借用商品
     * @date 2021/02/18
     * @author longli
     */
    public function borrow()
    {
        if($this->request->isAjax())
        {
            $sn = $this->request->get('sn', '', 'trim');
            if(empty($sn)) $this->error('非法请求', '', ["type" => "abnormal-operation"]);
            if(!($borrow = WarehouseBorrow::getBySn($sn)))
                $this->error('借用单不存在', '', ["type" => "order-not-exist"]);
            if($borrow->in_id != $this->warehouse->warehouse)
                $this->error("借用单不属于【{$this->warehouse->name}】，不能上架", '', ["type" => "abnormal-operation"]);
            $html = view('borrow_info', [
                'list' => $borrow->info
            ])->getContent();
            $data = compact('html');
            if(($msg = StockService::borrowReturn($borrow->borrow_sn, $borrow->in_id)) !== true)
                $this->error($msg, '', $data + ["type" => "up-fail"]);
            $this->success("上架成功", '', $data + ["type" => "up-success"]);
        }
        $this->assign('msg', '借用单号');
        return $this->fetch('wup');
    }
}