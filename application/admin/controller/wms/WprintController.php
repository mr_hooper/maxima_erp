<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2021/04/01
 * Time: 11:15
 * @link http://www.lmterp.cn
 */

namespace app\admin\controller\wms;

use app\admin\controller\BaseController;
use app\common\model\WarehouseStock;

/**
 * 打印条码
 * Class Wprint
 * @package app\admin\controller\wms
 */
class WprintController extends BaseController
{
    /**
     * 打印 SKU 条码
     * @date 2021/04/01
     * @author longli
     */
    public function index()
    {
        if($this->request->isAjax())
        {
            $sku = $this->request->post('sku', '', 'trim');
            if(empty($sku)) $this->error('非法请求', '', ["type" => "abnormal-operation"]);
            if(!($stock = WarehouseStock::getWarehouseBySku($this->warehouse, $sku)))
                $this->error('商品不存在', '', ["type" => "product-not-exist"]);
            $path = config('temp_pdf');
            if(!is_file($file = "$path/$sku.pdf"))
            {
                $pService = \app\common\service\product\ProductService::getInstance();
                $barcode = $pService->generateBarCode($sku, ['show_text' => true]);
                $html = "<body><div style='padding-top: 8mm;'><img src='$barcode'/></div></body>";
                $file = $pService->HTML2PDF($html, $path, $sku, [50, 30]);
            }
            if(!is_file($file)) $this->error('生成失败', '', ["type" => "operation-fail"]);
            $byte = base64_encode(file_get_contents($file));
            $html = view('wms/wfind/info', [
                'stock' => [$stock]
            ])->getContent();
            $this->success('打印成功', '', ['file' => $byte, 'html' => $html, "type" => "operation-success"]);
        }
        return $this->fetch('index');
    }
}