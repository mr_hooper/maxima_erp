<?php
/**
 * Created by PhpStorm.
 * User: longli
 * Date: 2021/07/17
 * Time: 10:07
 * @link http://www.lmterp.cn
 */

namespace app\admin\controller\order;

use app\admin\controller\BaseController;

class PickController extends BaseController
{
    /**
     * 拣货单列表
     * @date 2021/07/17
     * @author longli
     */
    public function index()
    {
        return $this->fetch('index');
    }
}