<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2020/11/25
 * Time: 21:56
 * @link http://www.lmterp.cn
 */

namespace app\admin\controller\pur;

use app\admin\controller\BaseController;
use app\common\model\FinancePayment;
use app\common\model\Producer;
use app\common\service\purchase\PurchaseService;
use think\facade\Validate;

class ProducerController extends BaseController
{
    /**
     * 供应商首页
     * @return string
     * @date 2020/11/25
     * @throws \think\exception\DbException
     * @author longli
     */
    public function index()
    {
        if($this->request->isAjax())
        {
            $producer = Producer::order("sort desc, quality desc, update_time desc");
            $this->searchModel($producer, [
                'eq' => ['code', 'address', 'type', 'platform',
                    'status', 'is_invoice', 'main_product', 'contacts', 'mobile', 'phone', 'wangwang', 'qq'],
                'like' => ['name'],
                'times' => ['create_time']
            ]);
            $limit = $this->getPageSize();
            $producer = $producer->paginate($limit);
            $this->assign("list", $producer->getCollection());
            $this->assign("page", $producer->render());
            return $this->fetch("lists");
        }
        return $this->fetch("index");
    }

    /**
     * 添加供应商
     * @date 2020/11/25
     * @author longli
     */
    public function add()
    {
        $producerId = $this->request->request("producer_id");
        if(!empty($producerId)) $this->assign("producer", Producer::get($producerId));
        $this->assign("pay_type", FinancePayment::$PAY_TYPE);
        $this->assign("line_type", Producer::$LINE_TYPE);
        $this->assign("status", Producer::$STATUS);
        return $this->fetch("add");
    }

    /**
     * 更新供应商信息
     * @date 2020/11/25
     * @author longli
     */
    public function save()
    {
        if($this->request->isPost())
        {
            $validate = Validate::make([
                'name'  => 'require',
                'type'  => 'require',
                'pay_type'  => 'require',
                'before_pay'  => 'egt:0',
                'credit_day'  => 'egt:0',
                'delivery_day'  => 'egt:0',
                'site' => 'url',
            ],[
                'name.require' => '供应商名称必填',
                'type.require' => '供应商类型必选',
                'pay_type.require' => '付款方式必选',
                'before_pay.egt' => '预付款百分比无效',
                'credit_day.egt' => '账期天数无效',
                'delivery_day.egt' => '交货天数无效',
                'site.url' => '网址无效',
            ]);
            $data = $this->request->post();
            if(!$validate->batch()->check($data))$this->error(join(', ', $validate->getError()));
            if(!isset($data['status'])) $data['status'] = Producer::IS_NO;
            if(!isset($data['is_invoice'])) $data['is_invoice'] = Producer::IS_NO;
            if(!isset($data['is_free_tax'])) $data['is_free_tax'] = Producer::IS_NO;
        }
        else
        {
            $validate = Validate::make([
                'producer_id'  => 'require',
            ],[
                'producer_id.require' => '非法请求',
            ]);
            $data = $this->request->only(['producer_id', 'status'], 'get');
            if(!$validate->batch()->check($data))$this->error(join(', ', $validate->getError()));
        }
        PurchaseService::getInstance()->addProducer($data)
            ? $this->success('操作成功')
            : $this->error('操作失败');
    }

    /**
     * 删除供应商
     * @date 2020/11/25
     * @author longli
     */
    public function delete()
    {
        $producerId = $this->request->request("ids");
        if(empty($producerId)) $this->error('参数错误');
        Producer::destroy($producerId);
        $this->success('删除成功');
    }

    /**
     * 获取供应商
     * @date 2020/12/20
     * @author longli
     */
    public function get()
    {
        $pid = $this->request->get("producer_id");
        if(empty($pid))
            return apiResponse(\app\common\status\BaseStatus::CODE_FAULT, [], '非法请求');;
        if(!($producer = Producer::get($pid)))
            return apiResponse(\app\common\status\BaseStatus::CODE_FAULT, [], '供应商不存在');
        return apiResponse(\app\common\status\BaseStatus::CODE_NORMAL, $producer->getData(), '获取成功');
    }
}