<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2020/12/20
 * Time: 15:10
 * @link http://www.lmterp.cn
 */

namespace app\admin\controller\pur;

use app\admin\controller\BaseController;
use app\common\library\Tools;
use app\common\model\ChannelExpress;
use app\common\model\FinanceBank;
use app\common\model\FinanceCash;
use app\common\model\JobFlowModule;
use app\common\model\Producer;
use app\common\model\Purchase;
use app\common\model\PurchaseReturn;
use app\common\model\PurchaseTrack;
use app\common\service\product\StockService;
use app\common\service\purchase\RExService;

/**
 * 采购退货单
 * Class ReturnController
 * @package app\admin\controller\pur
 */
class ReturnController extends BaseController
{
    protected $moduleName = 'purchase_return';

    /**
     * 退货单列表
     * @date 2021/01/23
     * @author longli
     */
    public function index()
    {
        if($this->request->isAjax())
        {
            $ren = PurchaseReturn::with(["purchase"])->order("status, ren_id desc");
            $this->search($ren);
            $limit = $this->getPageSize();
            $ren = $ren->paginate($limit);
            $this->assign("status_cancel", PurchaseReturn::STATUS_RETURN_CANCEL);
            $this->assign("status_out", PurchaseReturn::STATUS_RETURN_OUT);
            $this->assign("is_show_check", $this->user->hasPermissions('/pur/return/check'));
            $this->assign("list", $ren->getCollection());
            $this->assign("page", $ren->render());
            return $this->fetch('lists');
        }
        $this->assign("check_status", JobFlowModule::$CHECK_STATUS);
        $this->assign("producer", Producer::getAll());
        $this->assign("buyers", Purchase::getBuyers());
        $this->assign("status", PurchaseReturn::$STATUS_RETURN);
        return $this->fetch('index');
    }

    /**
     * 搜索退货单
     * @param $ren
     * @date 2021/01/23
     * @author longli
     */
    private function search($ren)
    {
        $this->searchModel($ren, [
            'eq' => ['producer_id', 'status', 'track_num', 'check_status'],
            'times' => ['create_time'],
        ]);
        // 处理子查询
        $subSel = ['buyer_user_id'];
        $pWhere = [];
        foreach($subSel as $f)
        {
            $text = $this->request->post($f, '', 'trim');
            if(empty($text) && !is_numeric($text)) continue;
            $pWhere[$f] = $text;
        }
        if($sn = $this->request->post('sn', '', 'trim'))
        {
            if($pTemp = Purchase::whereRaw("purchase_sn=? OR producer_sn=?", [$sn, $sn])->find())
            {
                $pWhere["purchase_id"] = $pTemp->purchase_id;
            }
            else
            {
                $ren->where('return_sn', $sn);
            }
        }
        if(!empty($pWhere))
        {
            $ren->where("purchase_id", "IN", function($query)use($pWhere)
            {
                $query->table(Purchase::getTable())->where($pWhere)->field("purchase_id");
            });
        }
    }

    /**
     * 退货单详情
     * @date 2021/01/23
     * @author longli
     */
    public function info()
    {
        $renId = $this->request->get("ren_id");
        if(empty($renId)) $this->error("非法请求");
        $ren = PurchaseReturn::get($renId);
        if(empty($ren)) $this->error("退货单不存在");
        if($ren->getData('check_status') != \app\common\model\JobFlowModule::CHECK_NO_NEED)
        {
            $this->assignCheck($renId);
        }
        $this->assign("ren_info", $this->renderInfo($ren));
        $this->assign("ren", $ren);
        return $this->fetch('info');
    }

    /**
     * 渲染自定义退货单详情模板
     * @param PurchaseReturn $ren 退货单数据模型
     * @return string|void
     * @date 2021/01/11
     * @author longli
     */
    private function renderInfo($ren)
    {
        return $this->renderTerp(PurchaseReturn::class, $ren->ren_id, "pur/purchase/info/purchase_return");
    }

    /**
     * 添加退货单
     * @date 2021/01/23
     * @author longli
     */
    public function add()
    {
        if($this->request->isPost())
        {
            $this->validateAdd();
            $data = [
                'purchase_id' => $this->request->post('purchase_id'),
                'express_id' => $this->request->post('express_id'),
                'account' => FinanceBank::get($this->request->post('bank_id'))->account,
                'type' => PurchaseReturn::RETURN_TYPE_PAM,
                'cash_type' => FinanceCash::CASH_TYPE_BANK,
                'pay_type' => $this->request->post('pay_type'), // 运费方式
                'cash_date' => $this->request->post('cash_date'),
                'logistics_price' => $this->request->post('logistics_price', 0),
                'track_num' => $this->request->post('track_num'),
                'remark' => $this->request->post('remark'),
                'is_check' => $this->request->post('is_check', 0),
            ];
            !empty($data['express_id']) && ($data['logistics_name'] = ChannelExpress::getNameById($data['express_id']));
            $info = [];
            $post = $this->request->post();
            foreach($post['sku'] as $k => $v)
            {
                $info[] = [
                    "sku" => $v,
                    "qty" => $post['qty'][$k],
                    "cost_desc" => isset($post['cost_desc'][$k]) ? $post['cost_desc'][$k] : '',
                    "remark" => isset($post['info_remark'][$k]) ? $post['info_remark'][$k] : '',
                ];
            }
            $msg = RExService::getInstance()->addReturn($data, $info);
            if(!is_object($msg)) $this->error($msg);
            $this->success("添加退货单成功");
        }
        // 展示退货页面
        $purId = $this->request->get("purchase_id");
        if(empty($purId)) $this->error("非法请求");
        if(!($purchase = Purchase::get($purId))) $this->error("采购单不存在");
        if(!in_array($purchase->getData('order_status'), [
            Purchase::ORDER_STATUS_RECV, Purchase::ORDER_STATUS_EXCEPTION,
            Purchase::ORDER_STATUS_CHE_INFO, Purchase::ORDER_STATUS_IN,
            Purchase::ORDER_STATUS_IN_EX, Purchase::ORDER_STATUS_WAIT_UP,
            Purchase::ORDER_STATUS_IN_PART, Purchase::ORDER_STATUS_IN_SUCC,
            Purchase::ORDER_STATUS_CHE_EX
        ])) $this->error("当前采购单状态为【{$purchase->order_status}】不能退货");
        $this->assign('pay_type', PurchaseTrack::$PAY_TYPE);
        $this->assign('express', ChannelExpress::getAll());
        $this->assign("purchase", $purchase);
        $this->assign("banks", FinanceBank::getAll());
        return $this->fetch('add');
    }

    /**
     * 验证添加收货单数据
     * @return bool|string
     * @date 2021/02/04
     * @author longli
     */
    private function validateAdd()
    {
        $data = $this->request->post();
        $validate = \think\facade\Validate::make([
            'purchase_id'  => 'require',
            'bank_id'  => 'require',
            'cash_date'  => 'require',
            'logistics_price'  => 'egt:0',
            'sku'  => 'array|require',
            'qty'  => 'array|require',
        ],[
            'purchase_id.require' => '采购ID必填',
            'bank_id.require' => '收款账号必选',
            'cash_date.require' => '收款日期必填',
            'logistics_price.egt' => '物流费用不能小于0',
            'sku.require' => 'SKU必填',
            'sku.array' => 'SKU必须是数组',
            'qty.require' => '数量必填',
            'qty.array' => '数量必须是数组',
        ]);
        if(!$validate->batch()->check($data))
            $this->error(join(", ", $validate->getError()));
        $error = [];
        foreach($data['qty'] as $k => $v)
        {
            if($v < 1) $error[] = "SKU【{$data['sku'][$k]}】数量不能小于1";
        }
        if(!empty($error)) $this->error(join(", ", $error));
        return true;
    }

    /**
     * 取消退货单
     * @date 2021/01/23
     * @author longli
     */
    public function cancel()
    {
        $ids = $this->request->get("ids");
        if(empty($ids)) $this->error("非法请求");
        $fail = [];
        foreach(PurchaseReturn::whereIn("ren_id", $ids)->select() as $ren)
        {
            if($ren->getData('status') > PurchaseReturn::STATUS_RETURN_WAIT_OUT)
            {
                $fail[] = $ren->ren_id;
                continue;
            }
            $ren->status = PurchaseReturn::STATUS_RETURN_CANCEL;
            $ren->save();
        }
        Tools::equalsArray($fail, explode(',', $ids))
            ? $this->error("取消失败")
            : $this->success("取消成功");
    }

    /**
     * 占用库存
     * @date 2021/07/02
     * @author longli
     */
    public function lock()
    {
        // @todo
        StockService::purchaseReturnUnlock();
    }
}