<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2021/04/01
 * Time: 14:29
 * @link http://www.lmterp.cn
 */

namespace app\admin\controller\report;

use app\admin\controller\BaseController;
use app\common\model\Account;
use app\common\model\Orders;
use app\common\model\SysCountries;
use think\Db;

/**
 * 数据报表
 * Class IndexController
 * @package app\admin\controller\report
 */
class DataController extends BaseController
{
    /**
     * 日期
     * @var string
     */
    protected $date;

    /**
     * 自定义时间
     * @var string
     */
    protected $customize;

    /**
     * 分页条数
     * @var int
     */
    protected $limit = 5;

    /**
     * 订单
     * @date 2021/03/25
     * @author longli
     */
    public function order()
    {
        if($this->request->isAjax())
        {
            $type = $this->request->request('type');
            $action = "order" . ucfirst($type);
            if(!method_exists($this, $action)) return "";
            if($customize = $this->request->request('customize', '', 'trim'))
            {
                $this->customize = $this->parseLayuiRangeDate('order_source_create_time', $customize);
            }
            else
            {
                $day = $this->request->request('day', 7, 'abs');
                $this->date = date('Y-m-d', strtotime("-{$day}day"));
            }
            return $this->$action();
        }
        $this->assign('account', Account::getAll());
        $this->assign('counties', SysCountries::getCodeNameCh());
        $this->assign('days', [3, 7, 10, 15, 30, 45, 60, 75, 90, 180, 360]);
        return $this->fetch('order');
    }

    /**
     * 订单国家占比
     * @date 2021/03/31
     * @author longli
     */
    private function orderCountry()
    {
        $where = [
            ["o.total_price", "gt", 0],
            ['o.order_status', 'not in', [
                    Orders::ORDER_CANCEL,
                    Orders::ORDER_REJECT,
                    ORders::ORDER_FALSE,
                    Orders::ORDER_RETURN
                ]
            ]
        ];
        $where[] = !empty($this->customize)
            ? $this->customize
            : ['order_source_create_time', 'egt', $this->date];

        if($code = $this->request->request('buyer_country_code', '', 'trim'))
            $where[] = ['o.buyer_country_code', 'eq', $code];
        if(($sku = $this->getSku()) && !empty($sku))
            $where[] = ['d.sku', 'in', $sku];
        $orders = Db::field(['o.buyer_country', 'd.image_url', 'sum(qty) qty', 'sku'])
            ->table('orders o')
            ->join('orders_detail d', 'o.order_id=d.order_id')
            ->where($where)
            ->group('o.buyer_country_code, d.sku')
            ->order('qty desc, sku')
            ->paginate($this->limit);
        $this->assign("list", $orders->getCollection());
        $this->assign("page", $orders->render());
        return $this->fetch('order_country');
    }

    /**
     * 订单账号销售
     * @date 2021/03/31
     * @author longli
     */
    private function orderAccount()
    {
        $where = [
            ["o.total_price", "gt", 0],
            ['o.order_status', 'not in', [
                    Orders::ORDER_CANCEL,
                    Orders::ORDER_REJECT,
                    ORders::ORDER_FALSE,
                    Orders::ORDER_RETURN
                ]
            ]
        ];
        $where[] = !empty($this->customize)
            ? $this->customize
            : ['order_source_create_time', 'egt', $this->date];
        $sa = false;
        if($accountId = $this->request->request('account_id', '', 'trim'))
        {
            $sa = true;
            $where[] = ['a.account_id', 'eq', $accountId];
        }
        if(($sku = $this->getSku()) && !empty($sku))
            $where[] = ['d.sku', 'in', $sku];
        $orders = Db::field(['a.username', 'd.image_url', 'sum(qty) qty', 'sku'])
            ->table('orders o')
            ->join('orders_detail d', 'o.order_id=d.order_id')
            ->join('account a', 'a.account_id=o.account_id')
            ->where($where)
            ->group('d.sku')
            ->order('qty desc, sku')
            ->paginate($this->limit);
        $this->assign('sa', $sa);
        $this->assign("list", $orders->getCollection());
        $this->assign("page", $orders->render());
        return $this->fetch('order_account');
    }

    /**
     * 通过 spu 获取 sku
     * @return array
     * @date 2021/04/12
     * @author longli
     */
    private function getSku()
    {
        $s = $this->request->request('sku', '', 'trim');
        if(empty($s)) return [];
        $sku = \app\common\model\Product::getSkusBySpu($s);
        return !empty($sku) ? $sku : $s;
    }
}