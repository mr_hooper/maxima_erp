<?php
/**
 * Created by PhpStorm.
 * User: longli
 * Date: 2021/06/11
 * Time: 15:07
 * @link http://www.lmterp.cn
 */

namespace app\admin\controller\report;

use app\admin\controller\BaseController;
use app\common\model\Account;
use app\common\model\ReportOrderDay;
use think\Db;

/**
 * 统计订单年月销售额
 * Class DataMonth
 * @package app\admin\controller\report
 */
class MonthController extends BaseController
{

    /**
     * 统计年月订单
     * @return mixed
     * @date 2021/06/11
     * @author longli
     */
    public function order()
    {
        if($this->request->isAjax())
        {
            $type = $this->request->request('type', 'month', 'trim');
            $action = "order" . ucfirst($type);
            if(!method_exists($this, $action)) return "";
            return $this->$action();
        }
        $this->assign('year', \app\common\library\Tools::getRecentYear(10));
        $this->assign('account', Account::getAll());
        return $this->fetch('order');
    }

    /**
     * 统计每个月销量情况
     * @return mixed|void
     * @date 2021/06/11
     * @author longli
     */
    private function orderMonth()
    {
        $where = [
            ['ref_key', 'eq', 'account_id'],
        ];
        if($year = $this->request->request('year', '', 'trim'))
        {
            $where[] = ['stat_date', 'gt', $year];
            $where[] = ['stat_date', 'lt', trim($year + 1)];
        }
        $sa = false;
        if($account = $this->request->request('account_id'))
        {
            $sa = true;
            $where[] = ['a.account_id', 'eq', $account];
        }
        $sql = ReportOrderDay::field(['a.username', 'SUM(total_price) total_price', 'SUM(qty) total_qty', "DATE_FORMAT(stat_date, '%Y-%m') stat_month"])
                ->alias("r")
                ->where($where)
                ->join('account a', 'a.account_id=r.ref_value')
                ->group("stat_month")
                ->order("stat_month desc")
                ->buildSql();
        $orders = Db::table($sql)->alias('temp')->paginate(12);
        $this->assign('sa', $sa);
        $this->assign("list", $orders->getCollection());
        $this->assign("page", $orders->render());
        return $this->fetch('order_month');
    }

    /**
     * 统计每年销量情况
     * @return mixed|void
     * @date 2021/06/11
     * @author longli
     */
    private function orderYear()
    {
        $where = [
            ['ref_key', 'eq', 'account_id'],
        ];
        $sa = false;
        if($account = $this->request->request('account_id'))
        {
            $sa = true;
            $where[] = ['a.account_id', 'eq', $account];
        }
        // TP5.1 bug 无法直接使用分组查询，需要转义一下
        $sql = ReportOrderDay::field(['a.username', 'SUM(total_price) total_price', 'SUM(qty) total_qty', "DATE_FORMAT(stat_date, '%Y') stat_year"])
            ->alias("r")
            ->where($where)
            ->join('account a', 'a.account_id=r.ref_value')
            ->group("stat_year")
            ->order("stat_year desc")
            ->buildSql();
        $orders = Db::table($sql)->alias("temp")->paginate(12);
        $this->assign('sa', $sa);
        $this->assign("list", $orders->getCollection());
        $this->assign("page", $orders->render());
        return $this->fetch('order_year');
    }
}