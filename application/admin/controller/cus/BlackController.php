<?php
/**
 * Created by Administrator
 * User: longli
 * VX: isa1589518286
 * Date: 2020/12/10
 * Time: 21:03
 * @link http://www.lmterp.cn
 */

namespace app\admin\controller\cus;

use app\admin\controller\BaseController;
use app\common\model\AccountPlatform;
use app\common\model\SysCountries;
use app\common\model\CsBlacklist;
use app\common\service\cus\CustomerService;
use think\facade\Validate;

class BlackController extends BaseController
{
    /**
     * 黑名单列表
     * @return string
     * @date 2020/12/10
     * @author longli
     */
    public function index()
    {
        if($this->request->isAjax())
        {
            if(!$this->request->isAjax()) return $this->fetch('index');
            $model = CsBlacklist::with(['country'])->order("status desc, black_id desc");
            // 处理搜索
            $this->searchModel($model, [
                'eq' => ['risk_num', 'platform_name', 'buyer_country_code'],
                'like' => ['consignee'],
                'times' => ['black_date']
            ]);
            $limit = $this->getPageSize();
            $ret = $model->paginate($limit);
            $this->assign('list', $ret->getCollection());
            $this->assign('page', $ret->render());
            return $this->fetch('lists');
        }
        $this->loadCommon();
        return $this->fetch("index");
    }

    /**
     * 加载共有变量信息
     * @date 2020/12/09
     * @author longli
     */
    protected function loadCommon()
    {
        $this->assign("country", SysCountries::getCodeNameCh());
        $this->assign('platform', AccountPlatform::getAll());
        $this->assign("risk_num", range(10, 1));
    }

    /**
     * 通过订单添加黑名单
     * @date 2020/12/10
     * @author longli
     */
    public function push()
    {
        $ids = $this->request->post("ids");
        if(empty($ids)) $this->error("非法请求");
        $reason = $this->request->post('reason', '', 'trim');
        CustomerService::getInstance()->addBlackByOrder($ids, $reason);
        $this->success("已加入黑名单");
    }

    /**
     * 更新黑名单信息
     * @date 2020/12/10
     * @author longli
     */
    public function save()
    {
        if($this->request->isPost())
        {
            $validate = Validate::make([
                'buyer_country_code' => 'require',
                'risk_num' => 'between:1,10'
            ],[
                'buyer_country_code.require' => '国家必选',
                'risk_num.between' => '风险等级只能在1-10之间',
            ]);
            $data = $this->request->post();
            if(!$validate->batch()->check($data))$this->error(join(', ', $validate->getError()));
            // 必填 5 项以上
            $count = 0;
            foreach($data as $v)
            {
                if(!empty($v)) $count++;
                if($count > 4) break;
            }
            if($count < 5) $this->error('必须填5项，或以上');
            if(!isset($data['status'])) $data['status'] = CsBlacklist::IS_YES;
            if(empty($data['risk_num'])) $data['risk_num'] = 3;
            if(!empty($data['order_no'])) $data['order_no'] = replaceStr($data['order_no']);
        }
        else
        {
            $validate = Validate::make([
                'black_id'  => 'require',
            ],[
                'black_id.require' => '非法请求',
            ]);
            $data = $this->request->only(['black_id', 'status'], 'get');
            if(!$validate->batch()->check($data))$this->error(join(', ', $validate->getError()));
        }
        CustomerService::getInstance()->addBlack($data)
            ? $this->success('操作成功')
            : $this->error('操作失败');
    }

    /**
     * 添加黑名单
     * @date 2020/12/10
     * @author longli
     */
    public function add()
    {
        $this->loadCommon();
        $blackId = $this->request->get("black_id");
        if(!empty($blackId)) $this->assign("black", CsBlacklist::get($blackId));
        return $this->fetch("add");
    }

    /**
     * 删除黑名单
     * @date 2020/12/10
     * @author longli
     */
    public function delete()
    {
        $blackId = $this->request->request("ids");
        if(empty($blackId)) $this->error('参数错误');
        CsBlacklist::destroy($blackId);
        $this->success('删除成功');
    }
}