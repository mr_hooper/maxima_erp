<?php
/**
 * Created by Administrator
 * User: longli
 * VX: isa1589518286
 * Date: 2020/12/08
 * Time: 21:03
 * @link http://www.lmterp.cn
 */

namespace app\admin\controller\cus;

use app\admin\controller\BaseController;
use app\common\library\Tools;
use app\common\model\CsEmailCategory;
use app\common\service\cus\EmailService;
use think\facade\Validate;

class EmailcateController extends BaseController
{
    /**
     * 分类列表
     * @return string|string[]
     * @date 2020/12/08
     * @author longli
     */
    public function index()
    {
        if($this->request->isAjax())
        {
            $cates = CsEmailCategory::order("sort desc")->select()->toArray();
            $list = Tools::generateTree($cates, 'cate_id', 'parent_id');
            $list = Tools::levelArray($list);
            $this->assign("list", $list);
            return $this->fetch('lists');
        }
        return $this->fetch("index");
    }

    /**
     * 更新分类信息
     * @date 2020/12/08
     * @author longli
     */
    public function save()
    {
        if($this->request->isPost())
        {
            $validate = Validate::make([
                'parent_id'  => 'require',
                'name' => 'require'
            ],[
                'parent_id.require' => '父类必选',
                'name.require' => '名称必填',
            ]);
            $data = $this->request->post();
            if(!$validate->batch()->check($data))$this->error(join(', ', $validate->getError()));
            if(!isset($data['status'])) $data['status'] = CsEmailCategory::IS_NO;
            if(!empty($data['keyword'])) $data['keyword'] = replaceStr($data['keyword']);
            if(empty($data['sort'])) $data['sort'] = 0;
        }
        else
        {
            $validate = Validate::make([
                'cate_id'  => 'require',
            ],[
                'cate_id.require' => '非法请求',
            ]);
            $data = $this->request->only(['cate_id', 'status'], 'get');
            if(!$validate->batch()->check($data))$this->error(join(', ', $validate->getError()));
        }
        EmailService::getInstance()->addCategory($data)
            ? $this->success('操作成功')
            : $this->error('操作失败');
    }

    /**
     * 添加分类
     * @date 2020/12/08
     * @author longli
     */
    public function add()
    {
        $this->assign("category", CsEmailCategory::getAllByTree());
        $cateId = $this->request->get("cate_id");
        if(!empty($cateId)) $this->assign("cate", CsEmailCategory::get($cateId));
        return $this->fetch("add");
    }

    /**
     * 删除分类
     * @date 2020/12/08
     * @author longli
     */
    public function delete()
    {
        $cateId = $this->request->request("ids");
        if(empty($cateId)) $this->error('参数错误');
        CsEmailCategory::destroy($cateId);
        $this->success('删除成功');
    }
}