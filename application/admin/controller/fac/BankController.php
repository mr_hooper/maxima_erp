<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2021/01/09
 * Time: 15:19
 * @link http://www.lmterp.cn
 */

namespace app\admin\controller\fac;

use app\admin\controller\BaseController;
use app\common\model\FinanceBank;
use app\common\service\finance\FinanceService;
use think\facade\Validate;

/**
 * 银行管理
 * Class BankController
 * @package app\admin\controller\fac
 */
class BankController extends BaseController
{
    /**
     * 银行列表
     * @return string
     * @date 2021/01/09
     * @author longli
     */
    public function index()
    {
        if(!$this->request->isAjax()) return $this->fetch('index');
        $bank = FinanceBank::order("sort desc, bank_id desc");
        $this->searchModel($bank, [
            'like' => ['user']
        ]);
        $limit = $this->getPageSize();
        $ret = $bank->paginate($limit);
        $this->assign("list", $ret->getCollection());
        $this->assign("page", $ret->render());
        return $this->fetch("lists");
    }

    /**
     * 添加银行
     * @return string
     * @date 2021/01/09
     * @author longli
     */
    public function add()
    {
        $bankId = $this->request->get("bank_id");
        if(!empty($bankId)) $this->assign("bank", FinanceBank::get($bankId));
        return $this->fetch('add');

    }

    /**
     * 保存银行
     * @date 2021/01/09
     * @author longli
     */
    public function save()
    {
        if($this->request->isPost())
        {
            if(($msg = $this->validateBank()) !== true) $this->error($msg);
            $data = $this->request->post();
            if(!isset($data['status'])) $data['status'] = FinanceBank::IS_NO;
            if(!isset($data['is_default'])) $data['is_default'] = FinanceBank::IS_NO;
        }
        else
        {
            $validate = Validate::make([
                'bank_id'  => 'require',
            ],[
                'bank_id.require' => '非法请求',
            ]);
            $data = $this->request->only(['bank_id', 'status', 'is_default'], 'get');
            if(!$validate->batch()->check($data))$this->error(join(', ', $validate->getError()));
        }
        FinanceService::getInstance()->addBank($data)
            ? $this->success("操作成功")
            : $this->error("操作失败");
    }

    /**
     * 删除银行
     * @date 2021/01/09
     * @author longli
     */
    public function delete()
    {
        $bankId = $this->request->get("ids");
        if(empty($bankId)) $this->error('非法操作');
        FinanceBank::destroy($bankId)
            ? $this->success("删除成功")
            : $this->error("删除失败");
    }

    private function validateBank()
    {
        $data = $this->request->post();
        // 普通验证
        $validate = Validate::make([
            'name'  => 'require',
            'account'  => 'require',
            'user'  => 'require',

        ],[
            'name.require' => '银行名称必填',
            'account.require' => '银行账号必填',
            'user.require' => '开户名必填',
        ]);
        if(!$validate->batch()->check($data)) return join(', ', $validate->getError());
        return true;
    }
}