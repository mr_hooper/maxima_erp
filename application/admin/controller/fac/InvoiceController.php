<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2021/01/15
 * Time: 15:17
 * @link http://www.lmterp.cn
 */

namespace app\admin\controller\fac;

use app\admin\controller\BaseController;
use app\common\model\FinanceBank;
use app\common\model\FinanceInvoiceSetting;
use app\common\service\finance\FinanceService;
use think\facade\Validate;

/**
 * 发票配置管理
 * Class CashController
 * @package app\admin\controller\fac
 */
class InvoiceController extends BaseController
{
    /**
     * 发票配置列表
     * @date 2021/01/09
     * @author longli
     */
    public function index()
    {
        if(!$this->request->isAjax())
        {
            $this->assign("invoice_type", FinanceInvoiceSetting::$INVOICE_TYPE);
            return $this->fetch('index');
        }
        $invoice = FinanceInvoiceSetting::order("sort desc");
        $this->searchModel($invoice, [
            'eq' => ['type', 'title', 'tax_code', 'email']
        ]);
        $limit = $this->getPageSize();
        $ret = $invoice->paginate($limit);
        $this->assign("list", $ret->getCollection());
        $this->assign("page", $ret->render());
        return $this->fetch("lists");
    }
    
    /**
     * 添加发票配置
     * @return string
     * @date 2021/01/09
     * @author longli
     */
    public function add()
    {
        $invoiceId = $this->request->get("invoice_id");
        if(!empty($invoiceId)) $this->assign("invoice", FinanceInvoiceSetting::get($invoiceId));
        $this->assign("banks", FinanceBank::getAll());
        $this->assign("invoice_type", FinanceInvoiceSetting::$INVOICE_TYPE);
        return $this->fetch('add');

    }

    /**
     * 保存发票配置
     * @date 2021/01/09
     * @author longli
     */
    public function save()
    {
        if($this->request->isPost())
        {
            if(($msg = $this->validateBank()) !== true) $this->error($msg);
            $data = $this->request->post();
            if(!isset($data['status'])) $data['status'] = FinanceInvoiceSetting::IS_NO;
            if(!isset($data['is_default'])) $data['is_default'] = FinanceInvoiceSetting::IS_NO;
        }
        else
        {
            $validate = Validate::make([
                'invoice_id'  => 'require',
            ],[
                'invoice_id.require' => '非法请求',
            ]);
            $data = $this->request->only(['invoice_id', 'status', 'is_default'], 'get');
            if(!$validate->batch()->check($data))$this->error(join(', ', $validate->getError()));
        }
        FinanceService::getInstance()->addInvoiceSetting($data)
            ? $this->success("操作成功")
            : $this->error("操作失败");
    }

    /**
     * 删除发票配置
     * @date 2021/01/09
     * @author longli
     */
    public function delete()
    {
        $invoiceId = $this->request->get("ids");
        if(empty($invoiceId)) $this->error('非法操作');
        FinanceInvoiceSetting::destroy($invoiceId)
            ? $this->success("删除成功")
            : $this->error("删除失败");
    }

    private function validateBank()
    {
        $data = $this->request->post();
        // 普通验证
        $validate = Validate::make([
            'type'  => 'require',
            'title'  => 'require',
            'tax_code'  => 'require',

        ],[
            'type.require' => '发票类型必选',
            'title.require' => '发票抬头必填',
            'tax_code.require' => '纳税人识别号必填',
        ]);
        if(!$validate->batch()->check($data)) return join(', ', $validate->getError());
        return true;
    }
}