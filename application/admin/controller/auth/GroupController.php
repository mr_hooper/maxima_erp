<?php

namespace app\admin\controller\auth;

use app\admin\controller\BaseController;
use app\common\model\AuthRule;
use think\Db;

/**
 * 用户组管理
 * Class GroupController
 * @package app\admin\controller\auth
 */
class GroupController extends BaseController
{

    /**
     * 主页面
     * @return mixed
     */
    public function index()
    {
        if(!$this->request->isAjax()) return view();
        $list = Db::name('auth_group')->select();
        $this->assign('list', $list);
        return view('lists');
    }

    /**
     * 添加页面和添加操作
     * @return mixed
     */
    public function add()
    {
        if($this->request->isPost())
        {
            $post = $this->request->post();
            if(!isset($post['title']))
            {
                $this->error('名称不能为空');
            }
            if(Db::name('auth_group')->insert($post) !== false)
            {
                $this->success('操作成功');
            }

            $this->error('添加失败');
        }
        else
        {
            return $this->fetch('edit');
        }
    }

    /**
     * 修改页面和修改操作
     * @return mixed
     */
    public function edit()
    {
        if($this->request->isPost())
        {
            $post = $this->request->post();
            if(!isset($post['name']))
            {
                $this->error('名称不能为空');
            }
            if(Db::name('auth_group')->update($post) !== false)
            {
                $this->success('操作成功');
            }

            $this->error('更新失败');
        }
        else
        {
            $id = $this->request->get('id', 0, 'intval');
            if(!$id)
            {
                exit('参数错误');
            }
            $info = Db::name('auth_group')->where('id', $id)->find();
            $this->assign('info', $info);

            return view();
        }
    }

    /**
     * 删除操作
     * @return json
     */
    public function delete()
    {
        $id = $this->request->get('id', 0, 'intval');
        if($id <= 0)
        {
            $this->error('参数错误');
        }
        if(Db::name('auth_group')->delete($id) !== false)
        {
            Db::name('auth_group_access')->where('group_id', $id)->delete();
            $this->success('删除成功');
        }
        $this->error('删除失败');
    }

    public function assigned()
    {
        if($this->request->isPost())
        {
            $ids = $this->request->post('id');
            if(empty($ids)) $this->error('参数错误');
            $groupId = $this->request->post('group_id');
            // 保存有问题，未更新权限表 rules 字段
            $t = Db::name('auth_group')->where("id", $groupId)->setField('rules', $ids);
            $this->success('设置成功');
        }
        $id = $this->request->get('id', 0, 'intval');
        if(empty($id)) $this->error('参数错误');
        $group = Db::name('auth_group')->where('id', $id)->find();
        if(empty($group)) $this->error('非法请求');
        $haveRules = !empty($group['rules']) ? explode(',', $group['rules']) : [];
        $rules = AuthRule::where('status', AuthRule::IS_YES)
        ->order('sort desc')
        ->select()
        ->toArray();
        foreach($rules as & $item)
        {
            if(in_array($item['id'], $haveRules)) $item['checked'] = true;
            if($item['pid'] == 0) $item['spread'] = true;
            $item['title'] = "【{$item['title']}】";
        }
        $tree = \app\common\library\Tools::generateTree($rules);
        $tree = \app\common\library\Tools::replaceArrayKey(['son' => 'children'], $tree);
        $this->assign('group', $group);
        $this->assign('tree', $tree);
        return $this->fetch('assigned');
    }
}