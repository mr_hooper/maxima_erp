<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2021/04/02
 * Time: 22:23
 * @link http://www.lmterp.cn
 */


namespace app\admin\controller\system;

use app\admin\controller\BaseController;
use app\common\model\JobFlowModule;

/**
 * 审批模块配置
 * Class JobmoduleController
 * @package app\admin\controller\system
 */
class JobfController extends BaseController
{
    /**
     * 首页
     * @date 2021/04/02
     * @author longli
     */
    public function index()
    {
        if($this->request->isAjax())
        {
            $flows = JobFlowModule::order('sort desc, flow_id desc')->select();
            $this->assign('list', $flows);
            return $this->fetch('lists');
        }
        $admin = \app\common\model\Admin::getAll()->toArray();
        $this->assign('admin', \app\common\library\Tools::visibleArray(['id', 'nickname'], $admin));
        return $this->fetch('index');
    }

    /**
     * 审批模块详情
     * @date 2021/04/03
     * @author longli
     */
    public function info()
    {
        if(!($flowId = $this->request->get('flow_id'))) $this->error("非法操作");
        $this->assign('flow', JobFlowModule::get($flowId));
        return $this->fetch('info');
    }

    /**
     * 添加审批模块
     * @date 2021/04/02
     * @author longli
     */
    public function add()
    {
        if($flowId = $this->request->get('flow_id'))
        {
            $this->assign('flow', JobFlowModule::get($flowId));
        }
        $this->assign('tables', JobFlowModule::getTables());
        return $this->fetch('add');
    }

    /**
     * 保存审批流
     * @date 2021/04/02
     * @author longli
     */
    public function save()
    {
        $validate = \think\facade\Validate::make([
            'module'  => 'require|max:50|unique:job_flow_module,module',
            'name'  => 'require|max:128',
            'id_type'  => 'require|max:64',
        ],[
            'module.require' => '模块标识必填',
            'module.unique' => '模块标识必须是唯一',
            'name.require' => '模块名称必填',
            'id_type.require' => '模块对应表必选',
        ]);
        $post = $this->request->post();
        if(!$validate->batch()->check($post))$this->error(join(', ', $validate->getError()));
        \app\common\service\system\JobFlowService::getInstance()->addFlow($post)
            ? $this->success('保存成功')
            : $this->error('保存失败');
    }

    /**
     * 删除审批模块
     * @date 2021/04/02
     * @author longli
     */
    public function delete()
    {
        $flowId = $this->request->get("flow_id");
        if(empty($flowId)) $this->error('非法操作');
        JobFlowModule::destroy($flowId)
            ? $this->success('删除成功')
            : $this->error('删除失败');
    }
}