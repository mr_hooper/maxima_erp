<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2020/6/25
 * Time: 13:33
 * @link http://www.lmterp.cn
 */
namespace app\admin\controller;

use app\common\library\Tools;
use app\common\model\Orders;
use app\common\model\ReportOrderDay;
use think\response\View;

class IndexController extends BaseController
{
    /**
     * 首页
     * @return View
     * @date 2020/04/14
     * @author longli
     */
    public function index()
    {
        $monthTotal = ReportOrderDay::field(["SUM(total_price) `price`", "SUM(qty) `order`"])
            ->where([
                ["stat_date", "gt", date('Y-m')],
                ["ref_key" , 'eq', 'account_id']
            ])->find();
        $this->assign("monthTotal", $monthTotal);
        $this->assign("exOrder", Orders::where("order_status", Orders::ORDER_EXCEPTION)->count());
        $this->assign("outTime", Orders::where([
            ["order_status", "lt", Orders::ORDER_PACKAGE],
            ["latest_delivery_time", "<=", date("Y-m-d H:i")]
        ])->whereRaw("latest_delivery_time IS NOT NULL")->count());
        $appAuth = Tools::curlGet(base64_decode('aHR0cDovL3d3dy5sbXRlcnAuY24vYXBpL2F1dGgvc3lzdGVtLmh0bWw/Y29kZT0=') . \Env::get('APP_AUTH'));
        $this->assign('tips',$appAuth['status'] && Tools::isJson($appAuth['data'], $data) && $data['code'] == 0 ? $data['msg'] : '');
        $this->assign("graph_30_day", $this->getSumMonthOrder());
        $this->assign("table_ymd", $this->getSumYMWD());
        $this->assign("home_index", true);
        return $this->fetch('index');
    }

    /**
     * 统计一个月内每天订单销售情况
     * @return array
     * @date 2020/09/29
     * @author longli
     */
    private function getSumMonthOrder()
    {
        $day = date('t');
        $beforeDay = date('j');
        $reportOrder = ReportOrderDay::field(["SUM(total_price) `price`", "SUM(qty) `order`", "stat_date"])
            ->where([
                ["stat_date", "gt", date('Y-m-d', strtotime("-{$beforeDay}day"))],
                ["ref_key" , 'eq', 'account_id']
            ])
            ->order("stat_date")
            ->group("stat_date")
            ->select();
        $ret = array_map(function($item){return 0;}, array_flip(range(1, $day)));
        foreach($reportOrder as $item)
        {
            if(empty($item['order'])) continue;
            $j = date("j", strtotime($item->stat_date));
            if(isset($ret[$j])) $ret[$j] = $item->order;
        }
        return $ret;
    }

    /**
     * 按年月周日统计
     * @date 2020/09/29
     * @author longli
     */
    private function getSumYMWD()
    {
        $tableName = ReportOrderDay::getTable();
        return ReportOrderDay::field(["SUM(total_price) `price`", "SUM(qty) `order`", "'昨天' s"])
            ->where([
                ["stat_date", "egt", date("Y-m-d", strtotime("-1day"))],
                ["ref_key", "eq", 'account_id'],
            ]) // 昨天
            ->union(function($query)use($tableName)
            {
                $day = date('w');
                $query->table($tableName)
                    ->field(["SUM(total_price) `price`", "SUM(qty) `order`", "'本周' s"])
                    ->where([
                        ["stat_date", "egt", date("Y-m-d", strtotime("-{$day}day"))],
                        ["ref_key" , 'eq', 'account_id']
                    ]); // 本周
            })
            ->union(function($query)use($tableName)
            {
                $query->table($tableName)
                    ->field(["SUM(total_price) `price`", "SUM(qty) `order`", "'本月' s"])
                    ->where([
                        ["stat_date", "egt", date("Y-m")],
                        ["ref_key" , 'eq', 'account_id']
                    ]); // 本月
            })
            ->union(function($query)use($tableName)
            {
                $query->table($tableName)
                    ->field(["SUM(total_price) `price`", "SUM(qty) `order`", "'本年' s"])
                    ->where([
                        ["stat_date", "egt", date("Y")],
                        ["ref_key" , 'eq', 'account_id']
                    ]); // 本年
            })->select();
    }

    /**
     * 更新侧边栏状态
     * @date 2020/04/14
     * @author longli
     */
    public function flexible()
    {
        $menu = $this->request->get('menu', 'open', 'trim');
        session('menu_status', $menu);
    }
}
