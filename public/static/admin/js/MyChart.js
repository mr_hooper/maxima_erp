;var MyChart = {
    // 柱状图
    bar: function(el, data)
    {
        if(!data.data.rich) data.data.rich = {};
        var series = data.data.series;
        if($.type(series) == 'array')
        {
            for(let i in series)
            {
               series[i] = $.extend({
                    type: 'bar',
                    label: {
                        show: true,
                        position: 'top'
                    }
                }, series[i]);
            }
        }
        else
        {
            series = $.extend({
                type: 'bar',
                label: {
                    show: true,
                    position: 'top'
                }
            }, series);
        }
        var chart = echarts.init(el instanceof $ ? el.get(0) : el)
            ,option = {
            notMerge: true,
            title: {
                text: data.title || '柱状图',
                show: true,
                textStyle: {
                    color: 'rgba(173, 36, 36, 1)',
                    fontWeight: '700',
                    fontFamily: 'Arial',
                    fontSize: 20,
                }
            },
            tooltip: {
                show: true,
                trigger: 'axis',
                //提示文字
                formatter: data.formatterTip || null
            },
            legend: data.legend || {},
            grid: {
                left: 20,
                right: 45,
                bottom: 75,
                containLabel: true
            },
            toolbox: {
                feature: {
                    magicType: {
                        type: ["line", "bar"]
                    },
                    saveAsImage: {},
                    restore: {},
                }
            },
            xAxis: {
                type: 'category',
                boundaryGap: data.data.boundaryGap || true,
                data: data.data.xAxis,
                axisLabel: {
                    formatter: function(value)
                    {
                        for(let i in data.data.xAxis)
                        {
                            if(data.data.xAxis[i] == value)
                            {
                                return '{' + i + '| }\n' + '{textValue|' + value + '}';
                            }
                        }
                    },
                    interval: 0,
                    rich: {
                        textValue: {
                            paddingTop: 20,
                        },
                        ...data.data.rich
                    }
                }
            },
            yAxis: {
                type: 'value',
            },
            series: series
        };
        chart.clear();
        chart.setOption(option);
    }
    // 饼图
    ,pie: function(el, data)
    {
        var chart = echarts.init(el instanceof $ ? el.get(0) : el)
            ,option = {
            title: {
                text: data.title || '饼图',
                left: 'center'
            },
            tooltip: {
                trigger: 'item',
                formatter: '{b} : {c} ({d}%)'
            },
            legend: {
                bottom: 10,
                left: 'center',
            },
            series: [
                {
                    type: 'pie',
                    radius: '65%',
                    center: ['50%', '50%'],
                    selectedMode: 'single',
                    data: data.data.series,
                    emphasis: {
                        itemStyle: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: 'rgba(0, 0, 0, 0.5)'
                        }
                    }
                }
            ]
        };
        chart.setOption(option);
    }
};