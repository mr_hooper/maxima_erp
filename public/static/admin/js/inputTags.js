/**
 * @Author: longli
 * @Date:   2020-08-31 11:40:42
 */
layui.define(['jquery','layer'],function(exports){
  "use strict";
  var $ = layui.jquery,layer = layui.layer


      //外部接口
      ,inputTags = {
        config: {}

        // 当前输入框
        , input: null

        //设置全局项
        ,set: function(options){
          var that = this;
          that.config = $.extend({}, that.config, options);
          return that;
        }

        // 事件监听
        ,on: function(events, callback){
          return layui.onevent.call(this, MOD_NAME, events, callback)
        }

      }

      //操作当前实例
      ,thisinputTags = function(){
        var that = this
            ,options = that.config;
        return {
          config: options
        }
      }

      //字符常量
      ,MOD_NAME = 'inputTags'


      // 构造器
      ,Class = function(options){
        var that = this;
        that.config = $.extend({}, that.config, inputTags.config, options);
        that.render();
      };

  //默认配置
  Class.prototype.config = {
    close: false  //默认:不开启关闭按钮
    ,theme: ''   //背景:颜色
    ,content: [] //默认标签
    ,delimiter: ',' //默认配置
    ,name: null // 表单提交
  };

  // 初始化
  Class.prototype.init = function(){
    var that = this
        ,spans = ''
        ,options = that.config;
    $.each(options.content,function(index,item){
      spans +='<span><em>'+item+'</em><button type="button" class="close">×</button></span>';
    })
    options.elem.before(spans)
    that.events()
    that.putHidden();
  }

  Class.prototype.render = function(){
    var that = this
        ,options = that.config;
    if(!(options.elem instanceof $))
    {
      var container = $(options.elem).addClass("layui-input-tags")
          .append("<input type='text' class='layui-input'/>");
      options.elem = container.find("input:last")
          .attr("placeholder", container.attr("msg") || "按回车生成标签");
    }
    that.enter();
    that.remove();
  };

  Class.prototype.getVal = function()
  {
    return this.config.content.join(this.config.delimiter);
  }

  Class.prototype.putHidden = function()
  {
    if(this.config.name)
    {
      var sclass = "input-hidden-tags-" + this.config.name;
      this.config.elem.parent().find("input[type=hidden]").remove();
      this.config.elem.after("<input class='" + sclass + "' type='hidden' name='" + this.config.name + "' value='" + this.getVal() + "'>");
    }
  }

  Class.prototype.remove = function()
  {
    var that = this
        , options = this.config;
    options.elem.keydown(function (event)
    {
      if(event.keyCode == 8 &&  options.elem.val() == '')
      {
        var $prev = options.elem.prev();
        if($prev.is("span"))
        {
          $prev.remove();
          options.content.pop();
          options.elem.val(' ');
          options.done && typeof options.done === 'function' && options.done(options.content);
          that.putHidden();
        }
      }
    });
  }

  // 回车生成标签
  Class.prototype.enter = function(){
    var that = this
        ,spans = ''
        ,options = that.config;
    options.elem.focus();
    options.elem.keyup(function(event){
      var keynum = (event.keyCode ? event.keyCode : event.which);
      if(keynum == '13'){
        var $val = options.elem.val().trim();
        if(!$val) return false;
        if(options.content.indexOf($val) == -1){
          options.content.push($val)
          that.render()
          spans ='<span><em>'+$val+'</em><button type="button" class="close">×</button></span>';
          options.elem.before(spans)
        }
        options.done && typeof options.done === 'function' && options.done(options.content);
        options.elem.val('');
        that.putHidden();
      }
      return false;
    })
  };

  //事件处理
  Class.prototype.events = function(){
    var that = this
        ,options = that.config;
    $(".layui-input-tags").on('click','.close',function(){
      var Thisremov = $(this).parent('span').remove(),
          ThisText = $(Thisremov).find('em').text();
      options.content.splice($.inArray(ThisText,options.content),1)
    })
  };

  //核心入口
  inputTags.render = function(options){
    var inst = new Class(options);
    inst.init();
    this.input = inst;
    return thisinputTags.call(inst);
  };
  exports('inputTags',inputTags);
}).link('/static/admin/css/inputTags.css');