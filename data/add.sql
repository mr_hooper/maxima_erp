-- 2021-08-16 添加
-- 添加字段
ALTER TABLE `purchase_exchange` ADD COLUMN `pay_type` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '运费方式' AFTER `logistics_name`;
ALTER TABLE `purchase_return` ADD COLUMN `pay_type` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '运费方式' AFTER `logistics_name`;
ALTER TABLE `purchase_return` MODIFY COLUMN `logistics_price` decimal(15, 4) UNSIGNED NOT NULL DEFAULT 0.0000 COMMENT '运费' AFTER `pay_type`;
ALTER TABLE `purchase_track` MODIFY COLUMN `pay_type` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '0包邮，1自付，2垫付' AFTER `track_info`;
-- 更新数据
UPDATE `auth_group` SET `name` = 'admin', `title` = '管理员', `rules` = '1,90,104,112,151,117,120,121,123,124,125,126,127,208,152,153,154,155,156,157,118,128,119,122,129,130,131,142,139,140,141,146,143,144,145,150,147,148,149,398,403,404,405,406,407,402,399,400,401,132,133,134,135,136,207,348,322,323,324,325,334,335,336,331,332,333,317,318,319,320,321,359,326,327,328,329,330,314,274,275,276,315,345,316,346,338,347,309,310,312,313,337,296,300,297,298,299,301,302,303,304,305,306,307,308,389,339,340,344,341,342,343,362,363,365,382,265,266,267,268,269,381,383,384,385,91,92,103,356,388,390,93,94,95,96,97,354,353,355,102,378,379,210,227,247,391,392,393,394,395,396,397,408,107,108,98,349,350,351,352,105,187,289,290,291,188,211,216,179,180,181,182,175,99,176,177,178,183,184,185,186,106,113,357,380,386,206,209,217,218,219,221,222,223,224,225,228,249,270,271,272,358,360,361,279,280,281,282,288,283,284,285,286,287,226,277,278,212,213,214,215,248,109,114,231,232,234,235,236,233,237,238,239,241,240,242,243,244,245,246,111,115,366,409,410,367,368,387,110,116,250,251,292,293,294,295,256,257,258,259,260,261,262,263,264,252,253,254,255,2,5,6,7,9,10,16,4,83,85,158,159,160,161,162,169,167,164,165,168,170,173,171,172,174,202,369,373,370,371,372,377,374,375,376,192,196,193,194,195,198,199,200,201,3,11,13,14,15,17,19,20,21,22,23,24,26,27,28,29,30,189,190,191,86,87,88,89', `status` = 1, `sort` = 0, `remark` = '最高权限管理员' WHERE `id` = 2;
INSERT INTO `auth_rule`(`id`, `pid`, `name`, `title`, `icon`, `condition`, `remark`, `is_menu`, `sort`, `status`, `create_time`, `update_time`) VALUES (409, 115, '/report/trend', '销量走势图', '', '', '', 1, 0, 1, '2021-06-11 16:04:29', '2021-06-11 16:04:29');
INSERT INTO `auth_rule`(`id`, `pid`, `name`, `title`, `icon`, `condition`, `remark`, `is_menu`, `sort`, `status`, `create_time`, `update_time`) VALUES (410, 409, '/report/trend/index', '走势图详情', '', '', '', 0, 0, 1, '2021-06-11 16:04:29', '2021-06-11 16:04:29');
UPDATE `auth_rule` SET `pid` = 365, `name` = '/wms/wprint/index', `title` = '商品条码', `icon` = '', `condition` = '', `remark` = '', `is_menu` = 1, `sort` = 40, `status` = 1, `create_time` = '2021-04-19 14:10:56', `update_time` = '2021-07-28 16:26:38' WHERE `id` = 382;
UPDATE `auth_rule` SET `pid` = 391, `name` = '/order/pick/reset', `title` = '重置打印状态', `icon` = '', `condition` = '', `remark` = '', `is_menu` = 0, `sort` = 0, `status` = 1, `create_time` = '2021-01-11 10:16:01', `update_time` = '2021-07-17 11:42:44' WHERE `id` = 408;
INSERT INTO `sys_config`(`id`, `type`, `title`, `name`, `value`, `group`, `extra`, `create_time`, `update_time`, `lock`, `sort`, `remark`) VALUES (30, 1, '缺货自动申报订单', 'order_lack', '1', 6, '', '2021-08-10 15:01:13', '2021-08-10 15:01:13', 0, 0, '订单缺货是否自动申报订单');
UPDATE `sys_config` SET `type` = 0, `title` = '采购税', `name` = 'purchase_tax', `value` = '0.17', `group` = 1, `extra` = '', `create_time` = '2021-01-11 10:16:02', `update_time` = '2021-01-11 10:16:02', `lock` = 0, `sort` = 0, `remark` = '国内采购税' WHERE `id` = 18;
UPDATE `sys_config` SET `type` = 0, `title` = '订单重量偏移', `name` = 'weight_offset', `value` = '100', `group` = 1, `extra` = '', `create_time` = '2021-02-22 13:15:56', `update_time` = '2021-02-22 13:15:56', `lock` = 0, `sort` = 0, `remark` = '订单出库可偏移重量g' WHERE `id` = 23;
UPDATE `sys_config` SET `type` = 1, `title` = '拣货单默认数', `name` = 'pick_default_qty', `value` = '100', `group` = 6, `extra` = '', `create_time` = '2021-07-19 15:45:05', `update_time` = '2021-07-19 15:45:05', `lock` = 0, `sort` = 0, `remark` = '每个拣货单上有多少个订单' WHERE `id` = 29;





