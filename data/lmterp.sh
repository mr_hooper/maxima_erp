#!/bin/bash
rootPath=/project/root/path
cd "$rootPath"
# 每天23.59备份数据库
hm=$(date +%H%M)
if [ "$hm" -eq "2359" ]; then
  mysqldump -uroot -p'123456' lmterp|gzip -c > /path/to/$(date +%Y%m%d).gz
fi



# 重启 swoole
if [ "$hm" -eq "0520" ]; then
  sudo -u www php "$rootPath/think" swoole stop > /dev/null 2>&1 &
  sleep 10
  sudo -u www php "$rootPath/think" swoole start > /dev/null 2>&1 &
  now=$(date +%Y-%m-%d:%H:%M:%S)
  echo "$now restart swoole" >> "$rootPath/runtime/swoole.log"
fi