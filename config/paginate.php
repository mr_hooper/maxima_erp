<?php
/**
 * Created by PhpStorm.
 * User: longli
 * VX: isa1589518286
 * Date: 2020/2/7
 * Time: 13:08
 * @link http://www.lmterp.cn
 */
//分页配置
return [
    'type'      => 'app\\common\library\\LayPage',
    'var_page'  => 'page',
    'list_rows' => 30,
    'show_page' => true,
];