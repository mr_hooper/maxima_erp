# 龙玛拓ERP

#### 介绍
- 龙玛拓ERP 支持跨境电商，国内电商，传统企业管理
- 使用 tp5.1 + swoole 改变了原有的系统架构，采用MSVC架构模式，提高系统性能及运行速度，代码易扩展
- 安装教程在 **data** 目录下

#### 参与贡献
- 龙里
- 微信号:`isa1589518286`
- 新版体验地址：[demo-erp.lmterp.cn](http://demo-erp.lmterp.cn)
- 账号：`manager`
- 密码：`123456`
- 官方地址：[www.lmterp.cn](http://www.lmterp.cn)
- 帮助手册：[帮助手册](http://www.lmterp.cn/static/attachment/lmterp-help.pdf?v=1.0)

#### 独立商城
- 国内：[demo1.lmterp.cn](http://demo1.lmterp.cn)
- 跨境：[demo2.lmterp.cn](http://demo2.lmterp.cn)

